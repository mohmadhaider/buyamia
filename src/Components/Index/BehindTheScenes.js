import { Row, Col, Button, Container } from "reactstrap";
import s from "./BehindTheSeen.module.css";

// Display Image with Conent On HomePage
const BehindTheSeen = () => {
  return (
    <div style={{ padding: "0px 0px 0px" }}>
      <Row style={{ padding: "0", margin: "0" }}>
        <Col lg="12" md="12" sm="12" xs="12" style={{}} className={s.Container}>
          <Row style={{ padding: "30px 0px 50px" }}>
            <Col
              lg="3"
              md="5"
              sm="12"
              xs="12"
              style={{ padding: "0", margin: "0" }}
            >
              <div className={s.InfoContainer}>
                <span className={s.image1Title}>
                  Behind&nbsp;the&nbsp;Scenes
                </span>
                <br />
                <h5 className={s.image1Desc}>
                  A success story about what makes us great, the makers and
                  their products, backgrounds and how they are there for you.
                </h5>
                <h5 className={s.image1Desc}>
                  Meet The Bali Curator and their Susksema project working with
                  Lepra hit communities in Samba.
                </h5>
                <Button>Learn More</Button>
              </div>
            </Col>
            <Col
              lg="9"
              md="7"
              sm="12"
              xs="12"
              style={{ padding: "0", margin: "0", width: "74%" }}
            >
              <Container>
                <Row
                  style={{ padding: "0", margin: "0", position: "relative" }}
                >
                  <Row
                    style={{
                      padding: "0",
                      margin: "0",
                      position: "absolute",
                      justifyContent: "center",
                      top: "10%",
                      zIndex: "2",
                    }}
                  >
                    <Col
                      lg="5"
                      md="5"
                      sm="5"
                      xs="5"
                      style={{ padding: "0", margin: "0" }}
                    >
                      <div style={{ position: "relative" }}>
                        <img style={{ width: "100%" }} src={"/Images/6.png"} />
                      </div>
                    </Col>
                  </Row>
                  <Col
                    lg="5"
                    md="5"
                    sm="5"
                    xs="5"
                    style={{ padding: "0", margin: "0" }}
                  >
                    <div style={{ marginTop: "30%" }}>
                      <img style={{ width: "100%" }} src={"/Images/5.png"} />
                    </div>
                  </Col>
                  <Col
                    lg="2"
                    md="2"
                    sm="2"
                    xs="2"
                    style={{ padding: "0", margin: "0" }}
                  ></Col>
                  <Col
                    lg="5"
                    md="5"
                    sm="5"
                    xs="5"
                    style={{ padding: "0", margin: "0" }}
                  >
                    <div style={{ marginTop: "30%" }}>
                      <img style={{ width: "100%" }} src={"/Images/7.png"} />
                    </div>
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default BehindTheSeen;
