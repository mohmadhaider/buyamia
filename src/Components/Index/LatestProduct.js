import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import s from "./LatestProduct.module.css";

function Item(props) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: "white",
        p: 1,
        m: 1,
        borderRadius: 1,
        textAlign: "center",
        fontSize: "1rem",
        marginRight: 10,
        fontWeight: "700",
        ...sx,
      }}
      {...other}
    />
  );
}

Item.propTypes = {
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool])
    ),
    PropTypes.func,
    PropTypes.object,
  ]),
};

// Display List of Latest product
export default function LatestProduct() {
  return (
    <div style={{ width: "100%" }}>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
          alignItems: "center",
          justifyContent: "center",
          top: 1350,
        }}
      >
        <Item>
          <img
            layout="fill"
            src={"/Images/LatestProduct/LatestProduct1.png"}
            className={s.mainImage}
          />
        </Item>
        <Item>
          <img
            layout="fill"
            src={"/Images/LatestProduct/LatestProduct1.png"}
            className={s.sideImage}
          />
        </Item>
        <Item>
          <img layout="fill" src={"/Images/LatestProduct/LatestProduct1.png"} />
        </Item>
      </Box>
    </div>
  );
}
