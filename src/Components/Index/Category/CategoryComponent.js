import React from "react";
import s from "../MainContent.module.css";

// Display Category List On HomePage
function CategoryComponent({ name, icon, color = "" }) {
  return (
    <div>
      {!color ? (
        <div
          className={s.category}
          onClick={() => {}}
          style={{ cursor: "pointer" }}
          color={"primary"}
          id="panel2a-header"
        >
          {icon}
          <span style={{ textAlign: "center" }}>{name}</span>
        </div>
      ) : (
        <div
          className={s.category}
          onClick={() => {}}
          style={{ cursor: "pointer", color: color }}
          color={"primary"}
          id="panel2a-header"
        >
          {icon}
          <span style={{ textAlign: "center", color: color }}>{name}</span>
        </div>
      )}
    </div>
  );
}

export default CategoryComponent;
