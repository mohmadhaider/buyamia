import * as React from "react";
//Use CardMedia to display
import CardMedia from "@mui/material/CardMedia";
import s from "./CategoryContainer.module.css";
// Container For Categoey
export default function CategoryContainer({ categoryImage, title, colorCode }) {
  return (
    <div
      style={{
        minWidth: 275,
        zIndex: 10,
      }}
    >
      <div
        style={{
          backgroundColor: colorCode,
        }}
      >
        <CardMedia
          component="img"
          height={270}
          width={187}
          sx={{
            objectFit: "contain",
            padding: "15px",
            cursor: "pointer",
            transition: "all 0.3s ease-in-out",
            zIndex: "-1",
            overflow: "hidden",
            "&:hover": {
              "-ms-transform": "scale(1.1)",
              "-webkit-transform": "scale(1.1)",
              transform: "scale(1.1)",
            },
          }}
          src={categoryImage}
          alt="Category Image Not Found"
        />
      </div>
      <div className={s.title}>
        <lable gutterBottom variant="h5" component="div">
          <center>{title}</center>
        </lable>{" "}
      </div>
    </div>
  );
}
