import * as React from "react";
import PropTypes from "prop-types";
//use matrial ui box  to specify design of single component
import Box from "@mui/material/Box";
import { Row, Col, Container } from "reactstrap";
//Display Specific items
import DetailsComponent from "./DetailsComponent";

function Item(props) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: "white",
        p: 1,
        m: 1,
        borderRadius: 1,
        textAlign: "center",
        fontSize: "1rem",
        fontWeight: "700",
        ...sx,
      }}
      {...other}
    />
  );
}

Item.propTypes = {
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool])
    ),
    PropTypes.func,
    PropTypes.object,
  ]),
};

// Display Details For Website Item on HomePage 
export default function Details() {
  return (
    <div style={{ padding: "60px 0px" }}>
      <Container>
        <Row style={{ margin: "0", padding: "0" }}>
          <Col lg="3" md="3" sm="12" xs="12">
            <DetailsComponent
              icon={"/Images/Details/wecommerce.svg"}
              title="WE-COMMERCE"
              summery="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh."
            />
          </Col>
          <Col lg="3" md="3" sm="12" xs="12">
            <DetailsComponent
              icon={"/Images/Details/quality.svg"}
              title="CURATED QUALITY"
              summery="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet "
            />
          </Col>
          <Col lg="3" md="3" sm="12" xs="12">
            <DetailsComponent
              icon={"/Images/Details/shopping.svg"}
              title="SHIPPING AND QC"
              summery="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam "
            />
          </Col>
          <Col lg="3" md="3" sm="12" xs="12">
            <DetailsComponent
              icon={"/Images/Details/support.svg"}
              title="24H SUPPORT"
              summery="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet "
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
