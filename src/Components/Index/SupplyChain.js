import React from "react";
import { Container } from "reactstrap";
import s from "./SupplyChain.module.css";

//Display Image with content on homepage
const SupplyChain = () => {
  return (
    <div className={s.supplyChain}>
      <Container className="text-center">
        <img src={"/Images/Surity.png"} style={{ width: "100px" }} />
        <br />
        <span className={s.supplyChainHeader}>
          Creating a 100% conscious supply chain
        </span>
        <br />
        <br />
        <span className={s.supplyChainInfo}>
          BUYAMIA is aiming to provide the many small and micro manufacturers in
          Indonesia a platform to sell to the rest of the world. From Rural to
          Main Street. With a WE-Commerce approach we highlight both the
          products you love and enjoy as well as the artisans behind them.
        </span>
        <br />
        <br />
        <span className={s.supplyChainInfo}>
          We differ from the traditional E-Commerce platforms as we offer extra
          services and dedicate ourselves to curated quality products and full
          service to take your headaches away by streamlining entire process.
        </span>
      </Container>
    </div>
  );
};

export default SupplyChain;
