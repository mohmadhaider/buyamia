import React from "react";
import s from "./imageshome.module.css";
//Diplay Image on Homepage
const Imageshome = () => {
  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <div
        className={s.mobileWrap}
        style={{
          display: "flex",
          justifyContent: "space-evenly",
          width: "100%",
          marginTop: 47,
        }}
      >
        <div style={{ position: "relative", width: "100%" }}>
          <img
            style={{ width: "100%", height: "100%" }}
            src="https://buyamiaimages.s3.ap-south-1.amazonaws.com/image+37.png"
          />
          <div className={s.category}>
            <label className={s.uppertext}>THE LATEST PRODUCTS IN</label>
            <label style={{ lineHeight: 0.8 }} className={s.categorytitle}>
              HOME & LIVING
            </label>
            <label
              className={s.smallfont + " mt-3"}
              style={{ color: "#7a7070" }}
            >
              A success story about what makes us great, the makers and their
              products, backgrounds and how they are there for you.
            </label>
            <button className={s.shopnowbtn}>SHOP NOW</button>
          </div>
        </div>
        <div style={{ width: "80px", height: "100%" }}></div>
        <div
          className={s.imageWidth}
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "space-between",
            flexDirection: "column",
            // width: "30%"
          }}
        >
          <div
            className={s.img2Container}
            style={{
              position: "relative",
            }}
          >
            <img src={"/Images/Asset3.png"} />
            <label
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                padding: "7px 14px",
                letterSpacing: 1,
                color: "#fff",
                fontSize: 12,
                backgroundColor: "#546122",
              }}
            >
              DESIGNER
            </label>

            <div
              style={{
                display: "flex",
                position: "absolute",
                bottom: 0,
                left: 30,
                flexDirection: "column",
                justifyContent: "flex-end",
                // padding: "30px 0px 30px 65px",
              }}
            >
              <label
                style={{ bottom: 38, position: "absolute" }}
                className={s.img2Title}
              >
                EXCLUSIVE
              </label>
              <p className={s.img2Title}> DESIGNER LABELS</p>
            </div>
          </div>
          <div
            className={s.img2Container}
            style={{ backgroundColor: "#efefef" }}
          >
            <img src="https://buyamiaimages.s3.ap-south-1.amazonaws.com/2+5.png" />
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                padding: "30px 0px 30px 10px",
                overflow: "auto",
              }}
            >
              <label style={{ lineHeight: 1 }} className={s.img3Title}>
                New Product 2021
              </label>
              <label className={s.img2Desc}>
                Lorem Ipsum is simply dummy text of the printing and types sate
                industry Lorem Ipsum has been the industry.
              </label>
              <button className={s.shopnowbtn2}>Buy Now</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
//2800

export default Imageshome;
