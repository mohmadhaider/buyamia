import * as React from "react";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import s from "./MainContent.module.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
//Category COntainer Icon ,Name
import CategoryComponent from "./Category/CategoryComponent";
// Display Image in Slider
import MainSlider from "./MainSlider";
// After Header List of Category and Slider of Main Image
export default function MainContent() {
  return (
    <div className={s.containter}>
      <div className={s.child1 + " d-none d-xl-block"}>
        <div>
          <CategoryComponent
            name="CATEGORIES"
            color="#062A30"
            icon={
              <FormatListBulletedIcon
                style={{
                  margin: "0px 15px 0px 7px",
                  height: 30,
                  color: "#062A30",
                }}
              />
            }
            subCategory={[
              "Bathroom",
              "Bedroom",
              "Home Decor",
              "Tabletop",
              "Kitchen & Dining",
              "Lighting",
              "Textiles & Rugs",
              "Furniture",
              "Office",
            ]}
          />
          <CategoryComponent
            name="Home & Lifestyle"
            icon={
              <img
                src="/Images/CategoryIcon/home.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "Bathroom",
              "Bedroom",
              "Home Decor",
              "Tabletop",
              "Kitchen & Dining",
              "Lighting",
              "Textiles & Rugs",
              "Furniture",
              "Office",
            ]}
          />
          <CategoryComponent
            name="Beauty"
            icon={
              <img
                src="/Images/CategoryIcon/beauty.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "Bath & Body	",
              "Fragrences & Oils",
              "Hair",
              "Maek-up",
              "Shaving & Grooming",
              "Skincare",
              "Spa & Spiritual	",
              "Nail Care",
            ]}
          />
          <CategoryComponent
            name="Fashion"
            icon={
              <img
                src="/Images/CategoryIcon/fashion.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "ActiveWear",
              "Swimwear",
              "Coats & Jackets",
              "Dresses",
              "Knitwear & Sweaters",
              "Pants & Shorts",
              "Skirts",
              "Sleepwear",
              "Socks",
              "Tops",
              "T-shirts",
              "Underwear",
              "Footwear",
              "Bags & Purses",
            ]}
          />
          <CategoryComponent
            name="Food & Drinks"
            icon={
              <img
                src="/Images/CategoryIcon/food.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "Drinks & Syrups",
              "Snacks",
              "Sweet",
              "Cooking & Baking",
              "Cereals & Grains",
              "Jams",
              "Sauces",
              "Pure Indonesian",
              "Spices & Seasoning",
            ]}
          />
          <CategoryComponent
            name="Jewelry"
            icon={
              <img
                src="/Images/CategoryIcon/jewelry.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "Rings",
              "Necklaces",
              "Earrings",
              "Bracelets",
              "Body Jewelry",
            ]}
          />
          <CategoryComponent
            name="Kids"
            icon={
              <img
                src="/Images/CategoryIcon/kids.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={["Apparel", "Footwear", "Nursery", "Toys & Learning"]}
          />
          <CategoryComponent
            name="Stationery"
            icon={
              <img
                src="/Images/CategoryIcon/stationery.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "Books",
              "Cards",
              "Office Supplies",
              "Storage/Filing",
            ]}
          />
          <CategoryComponent
            name="Art & Antique"
            icon={
              <img
                src="/Images/CategoryIcon/art.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={[
              "Drawing/Painting",
              "Sculpture",
              "Functional Art",
              "Indonesian",
              "Prints",
              "Photography",
            ]}
          />
          <CategoryComponent
            name="Raw Materials"
            icon={
              <img
                src="/Images/CategoryIcon/raw.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={["Sub-Category Not Available"]}
          />
          <CategoryComponent
            name="Designer Labels"
            color="#cfa684"
            icon={
              <img
                src="/Images/CategoryIcon/star.svg"
                height={30}
                style={{ margin: "0px 15px 0px 0px" }}
              />
            }
            subCategory={["Sub-Category Not Available"]}
          />
        </div>
      </div>
      <div className={s.child2}>
        <MainSlider />
      </div>
    </div>
  );
}
