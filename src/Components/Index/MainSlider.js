import React, { useState } from "react";
import s from "./MainSlider.module.css";

// Display main Image in Slider After Header
const MainSlider = () => {
  const images = [
    "/Images/MainContent/homepagemain.png",
    "/Images/MainContent/homepagemain.png",
    "/Images/MainContent/homepagemain.png",
    "/Images/MainContent/homepagemain.png",
  ];

  const [currentImageIndex, setCurrentImage] = useState(0);

  function plusSlides(incrIndex) {
    let nextIndex = currentImageIndex + incrIndex;

    if (nextIndex < 0) {
      setCurrentImage(images.length - 1);
    } else if (nextIndex > images.length - 1) {
      setCurrentImage(0);
    } else {
      setCurrentImage(nextIndex);
    }
  }

  return (
    <>
      <div className={s.slideshow_container}>
        <div className={(s.mySlides, s.fade)}>
          <div className={s.InfoContainer}>
            <span className={s.InfoTitle}>MEET THE MAKERS</span>
            <br />
            <span className={s.InfoHeading}>
              FROM RURAL
              <br /> TO MAIN STREET
            </span>
            <br />
            <span className={s.InfoDescription}>
              A success story about what makes us great, the makers and their
              products, backgrounds and how they are there for you
            </span>
          </div>
          <img
            src={images[currentImageIndex]}
            style={{
              objectFit: "scale-down",
              height: "80%",
              width: "auto",
              margin: "0 30px 0px 0px",
            }}
          />
        </div>

        <span className={s.prev} onClick={(e) => plusSlides(-1)}>
          &#10094;
        </span>
        <span className={s.next} onClick={(e) => plusSlides(1)}>
          &#10095;
        </span>
      </div>
      <div
        style={{
          textAlign: "center",
          position: "absolute",
          zIndex: "100",
          bottom: "20px",
          right: "46%",
        }}
      >
        <img src="/Images/Youtube.svg" style={{ cursor: "pointer" }} />
        <br />
        {images &&
          images.map((item, index) => (
            <span
              key={index}
              className={s.dot}
              style={{ opacity: index === currentImageIndex ? "0.5" : "1" }}
              onClick={(e) => setCurrentImage(index)}
            ></span>
          ))}
      </div>
    </>
  );
};

export default MainSlider;
