import React from "react";
import s from "./Meet.module.css";
import { Col, Row } from "reactstrap";

// Display Image With Content on Homepage
const MeetTheMakers = () => {
  return (
    <Row style={{ padding: "0", margin: "0" }}>
      <Col lg="7" md="7" sm="12" xs="12" style={{ padding: "0" }}>
        <img src={"/Images/4.png"} style={{ width: "100%" }} />
      </Col>
      <Col
        lg="5"
        md="5"
        sm="12"
        xs="12"
        style={{
          backgroundColor: "#FEF6EC",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div className={s.InfoContainer}>
          <span className={s.image1Heading}>THE BALI CURATOR</span>
          <br />
          <span className={s.image1Title}>Meet The Makers</span>
          <br />
          <br />
          <div className={s.image1Desc}>
            A success story about what makes us great, the makers and their
            products, backgrounds and how they are there for you.
          </div>
          <div className={s.image1Desc}>
            A success story about what makes us great, the makers and their
            products, backgrounds and how they are there for you.
          </div>
          <div className={s.image1Desc}>
            A success story about what makes us great, the makers and their
            products, backgrounds and how they are there for you.
          </div>

          <button className={s.shopnowbtn}>Learn More</button>
        </div>
      </Col>
    </Row>
  );
};

export default MeetTheMakers;
