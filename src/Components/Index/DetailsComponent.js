import React from "react";
import s from "./DetailsComponent.module.css";

// Display Image and Category name 
export default function DetailsComponent({ icon, title, summery }) {
  return (
    <div className={s.container + " text-center"}>
      <div>
        <img src={icon} className={s.iconWrap} />
      </div>
      <div className={s.titleWrap}>{title}</div>
      <div className={s.summeryWrap}>{summery}</div>
    </div>
  );
}
