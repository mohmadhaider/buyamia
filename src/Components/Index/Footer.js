import React from "react";
import { Container, Row, Col, Input, FormGroup } from "reactstrap";

import s from "./Footer.module.css";
// Icon From Material UI
import {
  BsFacebook,
  BsInstagram,
  BsLinkedin,
  BsPinterest,
  BsTwitter,
  BsWhatsapp,
  BsYoutube,
} from "react-icons/bs";
import { FaTiktok } from "react-icons/fa";

const Footer = () => {
  return (
    <div className={s.footer}>
      <Container>
        <Row className={s.linkContainer}>
          <Col lg="7" md="7" sm="12" xs="12">
            <Row>
              <Col
                lg={{ size: "8", offset: "2" }}
                md={{ size: "8", offset: "2" }}
                sm="12"
                xs="12"
              >
                <Row>
                  <Col lg="4" md="4" sm="4" xs="4">
                    <span className={s.linkHeader}>About</span>
                    <br />
                    <br />
                    <span className={s.linkItem}>About Us</span>
                    <br />
                    <span className={s.linkItem}>Timeline</span>
                    <br />
                    <span className={s.linkItem}>Careers</span>
                    <br />
                    <span className={s.linkItem}>Become a Seller</span>
                    <br />
                    <span className={s.linkItem}>Buying from us</span>
                  </Col>
                  <Col lg="4" md="4" sm="4" xs="4">
                    <span className={s.linkHeader}>Support</span>
                    <br />
                    <br />
                    <span className={s.linkItem}>Contact US</span>
                    <br />
                    <span className={s.linkItem}>Shipping</span>
                    <br />
                    <span className={s.linkItem}>Return/Warranty</span>
                    <br />
                    <span className={s.linkItem}>Support</span>
                  </Col>
                  <Col lg="4" md="4" sm="4" xs="4">
                    <span className={s.linkHeader}>Legal</span>
                    <br />
                    <br />
                    <span className={s.linkItem}>Privacy</span>
                    <br />
                    <span className={s.linkItem}>Terms</span>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col lg="4" md="4" sm="12" xs="12">
            <Container>
              <div className={s.infoNewsLetter}>
                <img
                  src="https://img.icons8.com/color/48/000000/indonesia-circular.png"
                  className={s.newsletterFlag}
                />
                &nbsp;&nbsp;&nbsp;
                <span className={s.newsLetterInfoHeader}>Region</span>
                &nbsp;&nbsp;
                <span className={s.newsLetterInfoItem}>Indonesia</span>
              </div>
              <div className={s.infoNewsLetter}>
                <span className={s.newsLetterInfoHeader}>
                  Wan’t be receive discounts, latest and greatest, sign up for
                  our newsletter:
                </span>
              </div>
              <FormGroup className={s.inputNewsLetter}>
                <Input
                  placeholder="Sign Up"
                  className={s.inputNewsLetterInput}
                ></Input>
              </FormGroup>
              <div className={s.divIconContainer}>
                <div className={s.divIconCircle}>
                  <BsInstagram />
                </div>
                <div className={s.divIconCircle}>
                  <BsLinkedin />
                </div>
                <div className={s.divIconCircle}>
                  <BsPinterest />
                </div>
                <div className={s.divIconCircle}>
                  <BsFacebook />
                </div>
                <div className={s.divIconCircle}>
                  <FaTiktok />
                </div>
                <div className={s.divIconCircle}>
                  <BsWhatsapp />
                </div>
                <div className={s.divIconCircle}>
                  <BsTwitter />
                </div>
                <div className={s.divIconCircle}>
                  <BsYoutube />
                </div>
              </div>
            </Container>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Footer;
