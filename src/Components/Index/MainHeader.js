import React from "react";
import s from "./MainHeader.module.css";
// Diplay On TOp
const MainHeader = () => {
  return (
    <>
      <div className={s.container}>
        <div className={s.wrapper}>
          <div className={s.wrapperlinks}>How We Work</div>
          <div className={s.wrapperlinks}>Meet Our Makers</div>
          <div className={s.wrapperlinks}>About Us</div>
        </div>
      </div>
    </>
  );
};

export default MainHeader;
