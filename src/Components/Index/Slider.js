import React from "react";
import s from "./Slider.module.css";

const Slider = () => {
  return (
    <div className={s.container}>
      <div className={s.gridRow}>
        <div className={s.gridColumn1}>
          <img layout="fill" src={"/Images/Designer2.png"} />
          <div className={s.ImageOverlay}>
            <span className={s.image1Heading}>THE LATEST PRODUCTS IN</span>
            <br />
            <span className={s.image1Title}>HOME & LIVING</span>
            <br />
            <h5 className={s.image1Desc}>
              A success story about what makes us great, the makers and their
              products, backgrounds and how they are there for you.
            </h5>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Slider;
