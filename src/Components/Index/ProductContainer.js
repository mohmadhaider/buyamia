import * as React from "react";
//Use Material Ui Cardmedia component For Display Product and Rating COmponent
import CardMedia from "@mui/material/CardMedia";
import Rating from "@mui/material/Rating";
import s from "./ProductContainer.module.css";

// Display Product with Title ,Image, Disc and Rating
export default function ProductContainer({
  productid,
  productImage = "",
  title = "",
  imageHeight = "250px",
  price = "",
  rating = "",
  minWidth = "",
  minHeight = "",
  hotseller,
  ratingColor = "",
}) {
  //   console.log(JSON.stringify(productImage) + "PRODUCT IAMEF");
  return (
    <div
      onClick={() => {
        window.location.href = "/ProductDetail/" + productid;
      }}
      style={{
        minWidth: minWidth,
        minHeight: minHeight,
        backgroundColor: "#fff",
        maxWidth: 315,
        maxHeight: 440,
        cursor: "pointer",
        position: "relative",
        boxShadow: "5px 0px 10px  rgba(32, 33, 36, 0.28)",
        borderRadius: "0px",
      }}
    >
      <CardMedia
        component="img"
        sx={{
          minHeight: imageHeight,
        }}
        src={productImage?.split(",")[0]}
        alt="Product Image Not Found"
      />
      {hotseller && (
        <div
          style={{
            position: "absolute",
            top: "0",
            left: "0",
            height: "42px",
            width: "55px",
            backgroundColor: "#e88f48",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            padding: "10px",
            fontSize: "12px",
            color: "#ffffff",
            lineHeight: 1,
            fontWeight: "400",
          }}
        >
          HOT SELLER
        </div>
      )}
      <div className={s.cardContainer}>
        {title?.length > 21 ? (
          <span className={s.title}> {title}</span>
        ) : (
          <lable className={s.title}>{title}</lable>
        )}{" "}
        <b className={s.price}>
          From IDR {price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
        </b>
        <label color="text.secondary" className={s.summery}>
          50 Pieces (Min. Order)
        </label>
        <label color="text.secondary" className={s.vendor}>
          The Bali Curator
        </label>
        {ratingColor === "" ? (
          <Rating
            name="half-rating"
            contentEditable={false}
            defaultValue={2.5}
            precision={rating}
            style={{
              color: "#c8a27b",
              bottom: 10,
              backgroundColor: "white",
              position: "absolute",
            }}
          />
        ) : (
          <Rating
            name="half-rating"
            contentEditable={false}
            defaultValue={2.5}
            precision={rating}
            style={{
              color: "#e88f48",
              bottom: 10,
              backgroundColor: "white",
              position: "absolute",
            }}
          />
        )}
      </div>
    </div>
  );
}
