import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import ProductContainer from "./ProductContainer";
import s from "./CategoryWiseProduct1.module.css";
function Item(props) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: "white",
        p: 1,
        m: 1,
        borderRadius: 1,
        textAlign: "center",
        fontSize: "1rem",
        fontWeight: "700",
        ...sx,
      }}
      {...other}
    />
  );
}

Item.propTypes = {
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool])
    ),
    PropTypes.func,
    PropTypes.object,
  ]),
};
export default function CategoryWiseProduct1({
  ProductCategoryData2 = [],
  ProductCategoryData3 = [],
  CategoryName2 = "Art and Antique",
  // CategoryName3 =  "Antique",
}) {
  return (
    <div style={{ width: "100%", marginTop: 10 }}>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          bgcolor: "background.paper",
          justifyContent: "center",
        }}
      >
        <div className={s.productContainer}>
          <div className={s.box}>
            <div className={s.title}>{CategoryName2} </div>
            <div className={s.iconWrap}>
              <img
                src={"/Images/DropshipHotlist/Rectangle 115.png"}
                style={{ width: 100 }}
                height={8}
              />
            </div>
            <Box
              sx={{
                display: "flex",
                //flexWrap: "wrap",
                overflow: "hidden",
                p: 1,
                m: 1,
                bgcolor: "background.paper",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {ProductCategoryData2.slice(0, 2)?.map((product, index) => (
                <Item key={index}>
                  <ProductContainer
                    productid={product.productid}
                    productImage={product.mainimagepath}
                    title={product.productname}
                    price={product.rate}
                    summery={product.productdesc}
                    rating={4}
                    minWidth={255}
                  />
                </Item>
              ))}
            </Box>
          </div>
        </div>
        <div className={s.productContainer}>
          <div className={s.box}>
            <div className={s.title}>{CategoryName3} </div>

            <div className={s.iconWrap}>
              <img
                src={"/Images/DropshipHotlist/Rectangle 115.png"}
                style={{ width: 110, backgroundColor: "#e88f48" }}
                height={6}
              />
            </div>
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                p: 1,
                m: 1,
                width: "100%",
                bgcolor: "background.paper",
                //   maxWidth: 300,
                justifyContent: "center",
              }}
            >
              {ProductCategoryData3.slice(0, 2)?.map((product, index) => (
                <Item key={index}>
                  <ProductContainer
                    productImage={product.mainimagepath}
                    title={product.productname}
                    productid={product.productid}
                    price={product.rate}
                    summery={product.productdesc}
                    rating={4}
                    minWidth={255}
                  />
                </Item>
              ))}
            </Box>
          </div>
        </div>
      </Box>
    </div>
  );
}
