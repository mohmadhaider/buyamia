import * as React from "react";
import PropTypes from "prop-types";
import s from "./Category.module.css";

//Category Container
import CategoryContainer from "./CategoryContainer";

import { Box } from "@material-ui/core";
function Item(props) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: "#666666",
        border: "none",
        fontSize: "1rem",
        width: 300,
        fontWeight: "700",
        ...sx,
      }}
      {...other}
    />
  );
}

Item.propTypes = {
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool])
    ),
    PropTypes.func,
    PropTypes.object,
  ]),
};
// Display List of Multiple Category with Image and Name
export default function Category() {
  let DivRef = React.useRef(null);
  const [currentScrollPosition, setCurrentScrollPosition] = React.useState(0);
  return (
    <div className={s.box}>
      <div className={s.title}>Categories</div>
      <div className={s.iconWrap}>
        <div style={{ width: 110, height: 6, backgroundColor: "#e88f48" }} />
      </div>
      <div className={s.productContainer}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "relative",
            alignItems: "center",
            justifyItems: "center",
            minHeight: "max-content",
          }}
        >
          {/* logic For Scrolling */}
          <span
            className={s.prev}
            onClick={() => {
              if (currentScrollPosition - 300 <= 0) {
                setCurrentScrollPosition(0);
              } else {
                setCurrentScrollPosition(currentScrollPosition - 300);
              }
              DivRef.current.scrollTo(
                currentScrollPosition,
                currentScrollPosition - 300
              );
            }}
          >
            &#10094;
          </span>
          <div
            ref={DivRef}
            style={{
              display: "flex",
              scrollBehavior: "smooth",
              alignItems: "center",
              flexWrap: "nowrap",
              padding: "20px 0px 5px",
              scrollbarWidth: "none",
              scrollbarColor: "#fff",
              overflow: "hidden",
            }}
          >
            <Item>
              <CategoryContainer
                categoryImage={"/Images/Category/Home.png"}
                title="Home & Lifestyle"
                colorCode="#e2e6e3"
              />
            </Item>
            <Item>
              <CategoryContainer
                categoryImage={"/Images/Category/Asset 1.png"}
                title="Beauty"
                heightSize={162}
                colorCode="#fef6ec"
                widthSize={236}
              />
            </Item>
            <Item>
              <CategoryContainer
                categoryImage={"/Images/Category/Asset 2.png"}
                title="Fashion"
                heightSize={270}
                widthSize={187}
                colorCode="#e2e6e3"
              />
            </Item>
            <Item>
              <CategoryContainer
                categoryImage={"/Images/Category/Food.png"}
                title="Food & Drinks"
                heightSize={162}
                widthSize={236}
                colorCode="#f4f2f0"
              />
            </Item>{" "}
            <Item>
              <CategoryContainer
                categoryImage={"/Images/Category/Raw.png"}
                title="Raw Materials"
                heightSize={270}
                widthSize={187}
                colorCode="#e2e6e3"
              />
            </Item>
            <Item>
              <CategoryContainer
                categoryImage={"/Images/Category/Art.png"}
                title="Art & Antique"
                heightSize={162}
                widthSize={236}
                colorCode="#f4f2f0"
              />
            </Item>{" "}
          </div>
          {/* logic For Scrolling */}

          <span
            className={s.next}
            onClick={() => {
              DivRef.current.scrollTo({ left: currentScrollPosition });
              if (
                DivRef.current.scrollWidth -
                  DivRef.current.offsetWidth -
                  (currentScrollPosition + 300) >
                300
              ) {
                setCurrentScrollPosition(currentScrollPosition + 300);
              } else {
                setCurrentScrollPosition(
                  DivRef.current.scrollWidth - DivRef.current.offsetWidth
                );
              }
            }}
          >
            &#10095;
          </span>
        </div>
      </div>
    </div>
  );
}
