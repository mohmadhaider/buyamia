import * as React from "react";
import s from "./CategoryWiseProduct.module.css";
s;
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
// Container For Product
import ProductContainer from "./ProductContainer";
// For Slider
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useEffect } from "react";

//Specify Design for diffrent size of device
const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
};

function Item(props) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: "white",
        p: 1,
        m: 1,
        margin: "0px 10px",
        borderRadius: 1,
        textAlign: "center",
        fontSize: "1rem",
        width: 254,
        fontWeight: "700",
        ...sx,
      }}
      {...other}
    />
  );
}

Item.propTypes = {
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool])
    ),
    PropTypes.func,
    PropTypes.object,
  ]),
};

// Displat List of Product base on Catgory
export default function CategoryWiseProduct({
  ProductCategoryData = [],
  CategoryName = "Home & Living",
  Size = 8,
}) {
  const [width, setWidth] = React.useState();
  useEffect(() => {
    setWidth(window.innerWidth);
    console.log(window.innerWidth);

    window.addEventListener("resize", () => {
      setWidth(window.innerWidth);
      console.log(window.innerWidth);
    });
  }, []);

  return (
    <div style={{ width: "100%" }}>
      <div className={s.box}>
        <div className={s.title}>{CategoryName.replace("_", " & ")}</div>
        <div className={s.iconWrap}>
          <div style={{ width: 110, height: 6, backgroundColor: "#C8A27B" }} />
        </div>
        <div
          style={{
            padding: "20px 20px 0px",
          }}
        >
          <Carousel
            responsive={responsive}
            containerClass={`${
              width >= "1420" ? " justify-content-center" : ""
            }`}
            removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
            itemClass={s.carouselitem}
          >
            {ProductCategoryData.slice(0, 5)?.map((product, index) => (
              <Item key={index}>
                <ProductContainer
                  productImage={product.mainimagepath}
                  title={product.productname}
                  price={product.rate}
                  productid={product.productid}
                  summery={product.productdesc}
                  rating={4}
                  minWidth={250}
                  minHeight={420}
                />
              </Item>
            ))}
          </Carousel>
        </div>
      </div>
    </div>
  );
}
