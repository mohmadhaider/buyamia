import * as React from "react";
import PropTypes from "prop-types";

import s from "./DropshipHotlist.module.css";
//Display Specific Single Product
import ProductContainer from "./ProductContainer";
//use matrial ui box  to specify design of single component
import Box from "@mui/material/Box";

function Item(props) {
  const { sx, ...other } = props;
  return (
    <Box
      sx={{
        color: "#666666",
        p: 1,
        m: 1,
        borderRadius: 1,
        fontSize: "1rem",
        fontWeight: "700",
        ...sx,
      }}
      {...other}
    />
  );
}

Item.propTypes = {
  sx: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool])
    ),
    PropTypes.func,
    PropTypes.object,
  ]),
};
// Display All Product in Slider
export default function DropshipHotlist({ products = [] }) {
  let DivRef = React.useRef(null);
  const [currentScrollPosition, setCurrentScrollPosition] = React.useState(0);
  return (
    <div className={s.box}>
      <div className={s.title}>Best Sellers</div>
      <div className={s.iconWrap}>
        <div style={{ width: 110, height: 6, backgroundColor: "#e88f48" }} />
      </div>
      <div className={s.productContainer}>
        <div
          style={{
            display: "flex",
            maxWidth: "98%",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            justifyItems: "center",
            position: "relative",
          }}
        >
          {/* Previous Arrow Login */}
          <span
            className={s.prev}
            onClick={() => {
              if (currentScrollPosition - 350 <= 0) {
                setCurrentScrollPosition(0);
              } else {
                setCurrentScrollPosition(currentScrollPosition - 350);
              }
              DivRef.current.scrollTo(
                currentScrollPosition,
                currentScrollPosition - 350
              );
            }}
          >
            &#10094;
          </span>

          <div
            ref={DivRef}
            style={{
              display: "flex",
              scrollBehavior: "smooth",
              justifyContent: "space-between",
              flexWrap: "nowrap",
              padding: "5px 0px",
              scrollbarWidth: "none",
              scrollbarColor: "#fff",
              overflow: "hidden",
            }}
          >
            {products.slice(0, 10)?.map((product, index) => (
              <Item key={index}>
                <ProductContainer
                  productid={product.productid}
                  productImage={product.mainimagepath}
                  title={product.productname}
                  price={product.rate}
                  summery={product.productdesc}
                  rating={4}
                  ratingColor="true"
                  imageHeight={230}
                  hotseller={index === 2 ? true : false}
                  minWidth={232}
                  minHeight={400}
                />
              </Item>
            ))}
          </div>
          {/* Next Arrow Login */}
          <span
            className={s.next}
            onClick={() => {
              DivRef.current.scrollTo({ left: currentScrollPosition });
              if (
                DivRef.current.scrollWidth -
                  DivRef.current.offsetWidth -
                  (currentScrollPosition + 350) >
                350
              ) {
                setCurrentScrollPosition(currentScrollPosition + 350);
              } else {
                setCurrentScrollPosition(
                  DivRef.current.scrollWidth - DivRef.current.offsetWidth
                );
              }
            }}
          >
            &#10095;
          </span>
        </div>
      </div>
    </div>
  );
}
