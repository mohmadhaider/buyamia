import React, { useState } from "react";
import { FaDollarSign } from "react-icons/fa";
import { Slider, Stack } from "@mui/material";
import { Row } from "reactstrap";

const WholesalePrice = () => {
  const [value, setValue] = useState(50);

  const handleChange = (e, newval) => {
    setValue(newval);
    // console.log(value);
  };

  function valuetext(value) {
    const val = value*100;
    return `${val}$`;
  }

  return (
    <div style={{ padding: "20px" }}>
      <h5>Minimum Order</h5>
      <br />
      <Row>
        <Stack spacing={2} direction="row" sx={{ mb: 1 }} alignItems="center">
          <FaDollarSign />
          <h6>100$</h6>
          <Slider
            aria-label="Volume"
            getAriaValueText={valuetext}
            value={value}
            valueLabelDisplay="auto"
            onChange={handleChange}
            style={{ color: "black" }}
          />
          <h6>1000$</h6>
        </Stack>
      </Row>
    </div>
  );
};

export default WholesalePrice;
