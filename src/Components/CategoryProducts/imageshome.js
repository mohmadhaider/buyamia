import React from "react";
import s from "./imageshome.module.css";

const Imageshome = () => {
  return (
    <div>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-evenly",
          marginTop: 40,
          width: "100%",
          backgroundColor: "rgb(245 243 241)",
        }}
      >
        <div style={{ position: "relative" }}>
          <img
            style={{ width: "100%", height: "100%" }}
            src="https://buyamiaimages.s3.ap-south-1.amazonaws.com/image+37.png"
          />
          <div className={s.category}>
            <label className={s.uppertext}>THE LATEST PRODUCTS IN</label>
            <label className={s.categorytitle}>HOME & LIVING</label>
            <label className={s.smallfont}>
              A success story about what makes us great, the makers and their
              products, backgrounds and how they are there for you.
            </label>
            <button className={s.shopnowbtn}>SHOP NOW</button>
          </div>
        </div>
      </div>
    </div>
  );
};
//2800

export default Imageshome;
