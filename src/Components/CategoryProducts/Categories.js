import React from "react";

const Categories = () => {
  return (
    <div style={{ padding: "20px" }}>
      <h4>All Categories</h4>
      <br/>
      <ul style={{ listStyleType: "none" }}>
        <li>
          <h5>Apparel</h5>
        </li>
        <li>
          <h5>Beauty</h5>
        </li>
        <li>
          <h5>Food & Drinks</h5>
        </li>
        <li>
          <h5>Home & Lifestyle</h5>
          <ul style={{ listStyleType: "none" }}>
            <li>
              <h6>Bathroom</h6>
            </li>
            <li>
              <h6>Bedroom</h6>
            </li>
            <li>
              <h6>Canldes & Holders</h6>
            </li>
            <li>
              <h6>Cleaning & Storage</h6>
            </li>
            <li>
              <h6>Garden & Outdoor</h6>
            </li>
            <li>
              <h6>Home Decor</h6>
            </li>
            <li>
              <h6>Home Fragrance</h6>
            </li>
            <li>
              <h6>Kitchen & Dining</h6>
            </li>
            <li>
              <h6>Crafts & Hobbies</h6>
            </li>
          </ul>
        </li>
        <li>
          <h5>Jewelry</h5>
        </li>
        <li>
          <h5>Kids</h5>
        </li>
        <li>
          <h5>Stationery</h5>
        </li>
        <li>
          <h5>Art & Vintage</h5>
        </li>
      </ul>
    </div>
  );
};

export default Categories;
