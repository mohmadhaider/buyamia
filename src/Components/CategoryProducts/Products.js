import React from "react";
import { Row, Col, Container, Button } from "reactstrap";
import Categories from "./Categories";
import MinOrder from "./MinOrder";
import WholesalePrice from "./WholesalePrice";
import CategoryWiseProduct from "../Index/CategoryWiseProduct";
import ProductPagination from "./ProductPagination";

const Products = ({ ProductData, ProductCategoryData, CategoryName }) => {
  return (
    <>
      <div style={{ margin: "30px 0" , padding:"0"}}>
        <Row style={{margin:"0", padding:"0"}}>
          <Col lg="3" md="3" sm="12" xs="12">
            <Container>
              <Categories />
              <hr style={{ margin: "20px", color: "black" }} />
              <MinOrder />
              <hr style={{ margin: "20px", color: "black" }} />
              <WholesalePrice />
              <Button color="dark" block>
                Apply Now
              </Button>
            </Container>
          </Col>
          <Col lg="9" md="9" sm="12" xs="12">
            <CategoryWiseProduct
              CategoryName={CategoryName}
              ProductCategoryData={ProductCategoryData?.input[0]}
              Size={15}
            />
            <Row>
              <Col
                lg={{ size: "4", offset: "4" }}
                md={{ size: "6", offset: "3" }}
                sm="12"
                xs="12"
              >
                <ProductPagination />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Products;
