import React from "react";
import {
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
const MinOrder = () => {
  return (
    <div style={{ padding: "20px" }}>
      <h5>Minimum Order</h5>
      <br />
      <ul>
        <FormControl component="fieldset">
          <RadioGroup
            aria-label="gender"
            defaultValue="female"
            name="radio-buttons-group"
          >
            <FormControlLabel
              value="all"
              control={<Radio color="dark" />}
              label="Show All"
            />
            <FormControlLabel
              value="nm"
              control={<Radio color="dark" />}
              label="No Minimum"
            />
            <FormControlLabel
              value="100"
              control={<Radio color="dark" />}
              label="Up to $100"
            />
            <FormControlLabel
              value="250"
              control={<Radio color="dark" />}
              label="Up to $250"
            />
            <FormControlLabel
              value="500"
              control={<Radio color="dark" />}
              label="Up to $500"
            />
          </RadioGroup>
        </FormControl>
      </ul>
    </div>
  );
};

export default MinOrder;
