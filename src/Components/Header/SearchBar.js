import * as React from "react";
import s from "./SearchBar.module.css";

// Use Material UI ICON
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import { makeStyles } from "@material-ui/core/styles";

export default function SearchBar(props) {
  return (
    <div
      component="form"
      style={{
        display: "flex",
        borderRadius: "40px",
        alignItems: "center",
        justifySelf: "center",
        width: props.highwidth ? "600px" : "auto",
        maxWidth: 600,

        margin: "auto",
      }}
      className={s.centerInner}
    >
      <div
        style={{ padding: "0px 7px", fontWeight: 100 }}
        className={s.centerInnerselect}
      >
        <select name="search" id="search" className={s.centerInnerselect}>
          <option value="product" style={{ border: 0 }}>
            &nbsp;Products
          </option>
          <option value="seller" style={{ border: 0 }}>
            &nbsp;Sellers
          </option>
        </select>
      </div>
      <InputBase
        className={s.searchText + " text-truncate"}
        sx={{ mx: 2, flex: 1 }}
        placeholder="Search for products & find verified sellers near you"
        inputProps={{
          "aria-label": "Search for products & find verified sellers near you",
        }}
      />
      <IconButton
        type="submit"
        sx={{ p: "10px 10px 10px 0px" }}
        aria-label="search"
      >
        <SearchIcon />
      </IconButton>
    </div>
  );
}
