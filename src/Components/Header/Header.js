// Use Material UI ICON Drawer[For Popup Box] and box
import { AppsRounded } from "@material-ui/icons";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import HomeIcon from "@mui/icons-material/Home";
import AutoFixHighIcon from "@mui/icons-material/AutoFixHigh";
import ToysIcon from "@mui/icons-material/Toys";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import ArticleIcon from "@mui/icons-material/Article";
import BrunchDiningIcon from "@mui/icons-material/BrunchDining";
import FormatColorFillIcon from "@mui/icons-material/FormatColorFill";
import FlareIcon from "@mui/icons-material/Flare";
import WomanIcon from "@mui/icons-material/Woman";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import CancelIcon from "@mui/icons-material/Cancel";

//import search bar component and Category Component
import * as React from "react";
import s from "../Index/MainContent.module.css";
import s2 from "./Header.module.css";
import SearchBar from "./SearchBar";
import CategoryComponent from "../Index/Category/CategoryComponent";
import Link from "next/link";
const Header = () => {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <div
          className={s.category}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <FormatListBulletedIcon style={{ margin: "0px 20px 0px 0px" }} />
          <Typography style={{ fontWeight: 750, fontFamily: "Roboto" }}>
            CATEGORIES
          </Typography>
          <CancelIcon
            style={{ fontSize: 30, cursor: "pointer" }}
            onClick={toggleDrawer(anchor, false)}
          />
        </div>

        <CategoryComponent
          name="Home & Living"
          icon={<HomeIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={[
            "Bathroom",
            "Bedroom",
            "Home Decor",
            "Tabletop",
            "Kitchen & Dining",
            "Lighting",
            "Textiles & Rugs",
            "Furniture",
            "Office",
          ]}
        />
        <CategoryComponent
          name="BEAUTY"
          icon={<AutoFixHighIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={[
            "Bath & Body	",
            "Fragrences & Oils",
            "Hair",
            "Maek-up",
            "Shaving & Grooming",
            "Skincare",
            "Spa & Spiritual	",
            "Nail Care",
          ]}
        />
        <CategoryComponent
          name="FASHION"
          icon={<WomanIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={[
            "ActiveWear",
            "Swimwear",
            "Coats & Jackets",
            "Dresses",
            "Knitwear & Sweaters",
            "Pants & Shorts",
            "Skirts",
            "Sleepwear",
            "Socks",
            "Tops",
            "T-shirts",
            "Underwear",
            "Footwear",
            "Bags & Purses",
          ]}
        />
        <CategoryComponent
          name="FOOD & DRINKS"
          icon={<BrunchDiningIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={[
            "Drinks & Syrups",
            "Snacks",
            "Sweet",
            "Cooking & Baking",
            "Cereals & Grains",
            "Jams",
            "Sauces",
            "Pure Indonesian",
            "Spices & Seasoning",
          ]}
        />
        <CategoryComponent
          name="JEWELRY"
          icon={<FlareIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={[
            "Rings",
            "Necklaces",
            "Earrings",
            "Bracelets",
            "Body Jewelry",
          ]}
        />
        <CategoryComponent
          name="KIDS"
          icon={<ToysIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={["Apparel", "Footwear", "Nursery", "Toys & Learning"]}
        />
        <CategoryComponent
          name="STATIONERY"
          icon={<MenuBookIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={["Books", "Cards", "Office Supplies", "Storage/Filing"]}
        />
        <CategoryComponent
          name="ART & ANTIQUE"
          icon={<ArticleIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={[
            "Drawing/Painting",
            "Sculpture",
            "Functional Art",
            "Indonesian",
            "Prints",
            "Photography",
          ]}
        />
        <CategoryComponent
          name="RAW MATERIALS"
          icon={<FormatColorFillIcon style={{ margin: "0px 20px 0px 0px" }} />}
          subCategory={["Sub-Category Not Available"]}
        />
      </List>
    </Box>
  );

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          padding: "10px 10px",
        }}
      >
        <Link href="/">
          <img
            style={{
              maxWidth: 170,
              cursor: " pointer",
            }}
            src={"/Images/logocropped.svg"}
          />
        </Link>
        {
          <div
            style={{ margin: "20px 10px" }}
            className="d-none d-lg-block d-xl-block"
          >
            <SearchBar highwidth />
          </div>
        }
        <div style={{ display: "flex" }}>
          <img
            src="/Images/HeaderIcon/bucket.svg"
            height={35}
            className={s2.mobileicon}
          />
          <img
            src="/Images/HeaderIcon/notification.svg"
            height={35}
            className={s2.mobileicon}
          />
          <img
            src="/Images/HeaderIcon/user.svg"
            height={35}
            className={s2.mobileicon}
          />

          <AppsRounded
            className="d-lg-none d-xl-none"
            style={{ fontSize: "35px", color: "#C8A27B", cursor: "pointer" }}
            onClick={toggleDrawer("bottom", true)}
          />
          <div>
            <Drawer anchor="bottom" open={state["bottom"]}>
              {list("bottom")}
            </Drawer>
          </div>
        </div>
      </div>
      {
        <div style={{ margin: "20px 20px" }} className="d-lg-none d-xl-none">
          <SearchBar />
        </div>
      }
    </div>
  );
};

export default Header;
