import React from 'react'
//Inquiry Form
function InquiryForm() {
    return (
        
        <div style={{ display: "flex", flexDirection: "column", flexWrap: "wrap", padding: "50px 0px", justifyContent: "center", alignItems: "center" }}>
            <b style={{  fontSize: 30 }}>Send Your Message To Supplier</b>
            <div style={{ display: "flex", flexDirection: "column", flexWrap: "wrap", padding: "50px 0px", justifyContent: "center" }}>
                <label>To: Seller</label>
                <div style={{ display: "flex", marginTop: "10px" }}><label>*Message:&nbsp;&nbsp;</label><textarea rows={8} style={{ width: "70%" }} placeholder='Enter your inquiry details such as product name, color, size, moq etx'></textarea>
                </div><label style={{ marginLeft: "60px" }}>Your message must be between 20-8000 characters</label>
                <div style={{ display: "flex" }}><label>Quantity: </label><input type="number"  style={{marginLeft: 20}} placeholder='Enter quantity' />
                    <select style={{marginLeft: 20}}>
                        <option>Acre/Acres</option>
                        <option>Acre/Acres</option>
                    </select>
                </div> <div style={{ display: "flex" }}><input type="checkbox" />&nbsp;&nbsp;<label>Recommend matching suppliers if this supplier doesnt contact me on Message Center within 24 hours  <b>Request for quotation</b></label>
                </div> <div style={{ display: "flex" }}><input type="checkbox" />&nbsp;&nbsp;<label>I agree to share my <b>Business Card</b> to the supplier</label>
                </div>
                <div><button style={{marginTop: 20, padding: "10px 20px", background: "orange", border: 0, borderRadius: 6, color: "#fff"}}>Send</button></div>
            </div>
        </div>
    )
}

export default InquiryForm
