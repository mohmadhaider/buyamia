import React, { useState, useEffect } from "react";
import s from "./SliderGalary.module.css";
import Link from "next/link";
import { Button, Container } from "reactstrap";

// Display Multiple Images for Product on slider
const SliderGalary = () => {
  const MaxSize = 6;
  const [slideIndex, setSlideIndex] = useState(1);

  function plusSlides(n) {
    debugger;
    if (slideIndex + n > MaxSize) {
      setSlideIndex(1);
    } else if (slideIndex + n < 1) {
      setSlideIndex(MaxSize);
    } else {
      setSlideIndex((slideIndex += n));
    }
  }

  function currentSlide(n) {
    setSlideIndex((slideIndex = n));
  }

  return (
    <>
      <Container>
        <div className={s.container}>
          {slideIndex == 1 && (
            <div className={(s.mySlides, s.active)}>
              <div className={s.numbertext}>1 / 6</div>
              <img
                src="Images/ProductDetails/3.png"
                style={{ width: "100%" }}
              />
            </div>
          )}
          {slideIndex == 2 && (
            <div className={(s.mySlides, s.active)}>
              <div className={s.numbertext}>2 / 6</div>
              <img
                src="Images/ProductDetails/2.png"
                style={{ width: "100%" }}
              />
            </div>
          )}
          {slideIndex == 3 && (
            <div className={(s.mySlides, s.active)}>
              <div className={s.numbertext}>3 / 6</div>
              <img
                src="Images/ProductDetails/1.png"
                style={{ width: "100%" }}
              />
            </div>
          )}
          {slideIndex == 4 && (
            <div className={(s.mySlides, s.active)}>
              <div className={s.numbertext}>4 / 6</div>
              <img
                src="Images/ProductDetails/4.png"
                style={{ width: "100%" }}
              />
            </div>
          )}
          {slideIndex == 5 && (
            <div className={(s.mySlides, s.active)}>
              <div className={s.numbertext}>5 / 6</div>
              <img
                src="Images/ProductDetails/5.png"
                style={{ width: "100%" }}
              />
            </div>
          )}
          {slideIndex == 6 && (
            <div className={(s.mySlides, s.active)}>
              <div className={s.numbertext}>6 / 6</div>
              <img
                src="Images/ProductDetails/6.png"
                style={{ width: "100%" }}
              />
            </div>
          )}
          <Button
            className={s.prev}
            onClick={(e) => {
              plusSlides(-1);
            }}
          >
            ❮
          </Button>
          <Button
            className={s.next}
            onClick={(e) => {
              plusSlides(1);
            }}
          >
            ❯
          </Button>

          <div className={s.caption_container}>
            <p id="caption"></p>
          </div>

          <div className={s.row}>
            <div className={s.column}>
              <img
                className={(s.demo, s.cursor)}
                src="Images/ProductDetails/3.png"
                style={{ width: "100%" }}
                onClick={(e) => currentSlide(1)}
                alt="The Woods"
              />
            </div>
            <div className={s.column}>
              <img
                className={(s.demo, s.cursor)}
                src="Images/ProductDetails/2.png"
                style={{ width: "100%" }}
                onClick={(e) => currentSlide(2)}
                alt="Cinque Terre"
              />
            </div>
            <div className={s.column}>
              <img
                className={(s.demo, s.cursor)}
                src="Images/ProductDetails/1.png"
                style={{ width: "100%" }}
                onClick={(e) => currentSlide(3)}
                alt="Mountains and fjords"
              />
            </div>
            <div className={s.column}>
              <img
                className={(s.demo, s.cursor)}
                src="Images/ProductDetails/4.png"
                style={{ width: "100%" }}
                onClick={(e) => currentSlide(4)}
                alt="Northern Lights"
              />
            </div>
            <div className={s.column}>
              <img
                className={(s.demo, s.cursor)}
                src="Images/ProductDetails/5.png"
                style={{ width: "100%" }}
                onClick={(e) => currentSlide(5)}
                alt="Nature and sunrise"
              />
            </div>
            <div className={s.column}>
              <img
                className={(s.demo, s.cursor)}
                src="Images/ProductDetails/6.png"
                style={{ width: "100%" }}
                onClick={(e) => currentSlide(6)}
                alt="Snowy Mountains"
              />
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default SliderGalary;
