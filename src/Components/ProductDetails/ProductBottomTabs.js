import React from "react";
import Link from "next/link";

import {
  Container,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Media,
  Modal,
  ModalBody,
} from "reactstrap";

import ReviewForm from "./ReviewForm";
import Stars from "./ReviewForm";
import { BsCheckCircle } from "react-icons/bs";
import { FaTimes } from "react-icons/fa";

const ProductBottomTabs = ({ product, ProductData }) => {
  const [activeTab, setActiveTab] = React.useState(1);
  const [refundModal, setRefundModal] = React.useState(false);
  const toggleTab = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  const groupByN = (n, data) => {
    let result = [];
    for (let i = 0; i < data.length; i += n) result.push(data.slice(i, i + n));
    return result;
  };

  // Details of Product
  const groupedAdditionalInfo = groupByN(4, product.additionalinfo);
  return (
    <>
      <section className="mt-5">
        <Container>
          <Nav tabs className="flex-column flex-sm-row">
            <NavItem>
              <NavLink
                className={`detail-nav-link ${activeTab === 1 ? "active" : ""}`}
                onClick={() => toggleTab(1)}
                style={{ cursor: "pointer" }}
              >
                Specifications
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`detail-nav-link ${activeTab === 2 ? "active" : ""}`}
                onClick={() => toggleTab(2)}
                style={{ cursor: "pointer" }}
              >
                Company Profile
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`detail-nav-link ${activeTab === 3 ? "active" : ""}`}
                onClick={() => toggleTab(3)}
                style={{ cursor: "pointer" }}
              >
                Reviews
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`detail-nav-link ${activeTab === 4 ? "active" : ""}`}
                onClick={() => toggleTab(4)}
                style={{ cursor: "pointer" }}
              >
                FAQs
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent className="py-4" activeTab={activeTab}>
            <TabPane tabId={1}>
              <Row>
                <Col md="12"></Col>

                <Row style={{ marginTop: "30px", padding: "10px 20px" }}>
                  <Col md="12" style={{ fontSize: "15px" }}>
                    <table>
                      <tbody>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Brand:
                          </th>
                          <td className="text-muted border-0 p-1">DAOQI</td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Model:
                          </th>
                          <td className="text-muted border-0 p-1">
                            XH124-Classic
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Features:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Adjustable, Rounded, Reversible
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Application:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Cooking, Home
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Colors:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Light Brown, Dark Brown, Black
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Material:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Teak Wood, Steel
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Style & Design:
                          </th>
                          <td className="text-muted border-0 p-1">Moroccan</td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Buyer types:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Restaurant, Kitchen, Food-Shop
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Occasion:
                          </th>
                          <td className="text-muted border-0 p-1">Winter</td>
                        </tr>

                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Country of Origin:
                          </th>
                          <td className="text-muted border-0 p-1">Indonesia</td>
                        </tr>
                        <br />
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Operating instruction:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Use in cool environment and non humid area.
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Handling instruction:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Handle gently, when move use packing box, unfold
                            with care and place parts in their sockets as
                            instructed in handling manual.
                          </td>
                        </tr>
                        <br />
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Packing:
                          </th>
                          <td className="text-muted border-0 p-1">
                            23cm x 24cm x 2.5cm, 50cm x 48cm x 4cm
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Shipping:
                          </th>
                          <td className="text-muted border-0 p-1">
                            Surabya, DHL, Aramex
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Refund:
                          </th>
                          <td className="text-muted border-0 p-1">
                            7 Days replacement
                            <br />
                            <Link href="#">
                              <span
                                style={{
                                  color: "darkgreen",
                                  fontWeight: "450",
                                  cursor: "pointer",
                                }}
                                onClick={(e) => setRefundModal(!refundModal)}
                              >
                                Refund Policy <BsCheckCircle />
                              </span>
                            </Link>
                          </td>
                        </tr>
                        <tr>
                          <th className="font-weight-normal border-0 p-1">
                            Certification:
                          </th>
                          <td className="text-muted border-0 p-1">
                            CE / EU, CIQ, Eec, LFGB, Sgs
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </Col>
                </Row>
                <Row style={{ marginTop: "20px", padding: "10px 20px" }}>
                  <Col md="6" style={{ fontSize: "14px" }}>
                    <table className="table table-bordered">
                      <tbody>
                        <tr>
                          <th>Qty</th>
                          <td>12-48</td>
                          <td>49-120</td>
                          <td>120+</td>
                        </tr>
                        <tr>
                          <th>Price</th>
                          <td> IDR 2,000.00/ Piece</td>
                          <td> IDR 1,950.00/ Piece</td>
                          <td> IDR 1,900.00/ Piece</td>
                        </tr>
                        <tr>
                          <th>Est. Lead Time</th>
                          <td>4-7 days</td>
                          <td>8-12 days</td>
                          <td>12-15 days</td>
                        </tr>
                      </tbody>
                    </table>
                  </Col>
                </Row>
                {/* {groupedAdditionalInfo.map((infoBlock, index) => (
                <Col key={index} md="6">
                  <table className="table text-sm">
                    <tbody>
                      {infoBlock.map((info, index) => (
                        <tr key={index}>
                          <th className="font-weight-normal border-0">
                            {info.name}
                          </th>
                          <td className="text-muted border-0">{info.text}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </Col>
              ))} */}
              </Row>
            </TabPane>
            <TabPane tabId={3}>
              <ReviewForm />
            </TabPane>
          </TabContent>
        </Container>
      </section>

      <Modal
        isOpen={refundModal}
        toggle={() => setRefundModal(!refundModal)}
        keyboard={false}
        size="sm"
      >
        <div style={{ position: "relative" }}>
          <FaTimes
            style={{
              position: "absolute",
              right: "0",
              margin: "10px",
              cursor: "pointer",
            }}
            className="text-danger"
            onClick={(e) => setRefundModal(!refundModal)}
          />
        </div>
        <br />
        <br />
        <ModalBody>
          <div className="table-responsive">
            <span
              style={{ fontWeight: "600", color: "#000", marginTop: "20px" }}
            >
              * If either dispatch date or product quality differs from what are
              specified in the online order, you can claim for a full or partial
              refund. 2-hour cancellation: For orders ≤ $500, you can get refund
              within 2 hours after you make a payment.
            </span>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default ProductBottomTabs;
