import React, { useState } from "react";
import s from "./SliderGalary.module.css";
import { Container, Row, Col } from "reactstrap";
import { FaHeart, FaSearch } from "react-icons/fa";
import { IoIosArrowUp, IoIosArrowDown } from "react-icons/io";

const SliderGalary = ({ ProductData }) => {
  const [currentImage, setCurrentImage] = useState(0);
  const [spliceIndex, setSpliceIndex] = useState(0);
  return (
    <>
      {/*  */}
      <Container className="p-0">
        <div className={s.container}>
          <Row style={{ margin: "0", padding: "0" }}>
            <Col
              lg="2"
              md="2"
              sm="2"
              xs="2"
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
                padding: "0",
              }}
              className={s.ProductImgContainer}
            >
              <div
                style={{ backgroundColor: "lightgray", opacity: "0.5" }}
                className="text-center mb-1"
              >
                <IoIosArrowUp style={{ fontSize: "25px" }} />
              </div>
              {ProductData?.split(",")
                .splice(spliceIndex, spliceIndex + 4)
                .map((image, index) => (
                  <div className="mb-1" key={index}>
                    <img
                      src={image}
                      key={index}
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                      onClick={(e) => setCurrentImage(index)}
                      onMouseOver={(e) => setCurrentImage(index)}
                      className={currentImage == index && "opacity-50"}
                    />
                  </div>
                ))}
              <div
                style={{ backgroundColor: "lightgray", opacity: "0.5" }}
                className="text-center"
              >
                <IoIosArrowDown style={{ fontSize: "25px" }} />
              </div>
              {ProductData?.split(",").length == 1 && (
                <>
                  <div className="mb-1">
                    <img
                      src="/Images/Transparent.png"
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                    />
                  </div>
                  <div className="mb-1">
                    <img
                      src="/Images/Transparent.png"
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                    />
                  </div>
                  <div className="mb-1">
                    <img
                      src="/Images/Transparent.png"
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                    />
                  </div>
                </>
              )}
              {ProductData?.split(",").length == 2 && (
                <>
                  <div className="mb-1">
                    <img
                      src="/Images/Transparent.png"
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                    />
                  </div>
                  <div className="mb-1">
                    <img
                      src="/Images/Transparent.png"
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                    />
                  </div>
                </>
              )}
              {ProductData?.split(",").length == 3 && (
                <>
                  <div className="mb-1">
                    <img
                      src="/Images/Transparent.png"
                      style={{
                        width: "100%",
                        height: "auto",
                        cursor: "pointer",
                        transition: "transform .2s",
                      }}
                    />
                  </div>
                </>
              )}
            </Col>
            <Col lg="10" md="10" sm="10" xs="10">
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  position: "relative",
                }}
                className={s.ProductImage}
              >
                <img
                  src={ProductData?.split(",")[currentImage]}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
                <div
                  style={{
                    position: "absolute",
                    height: "70px",
                    width: "70px",
                    top: "0",
                    right: "0",
                    fontSize: "30px",
                    backgroundColor: "#54622B",
                    alignItems: "center",
                    justifyContent: "center",
                    display: "flex",
                  }}
                >
                  <FaHeart style={{ color: "#ffffff" }} />
                </div>
                <div
                  style={{
                    position: "absolute",
                    height: "70px",
                    width: "70px",
                    bottom: "0",
                    right: "0",
                    fontSize: "30px",
                    backgroundColor: "lightgray",
                    opacity: "0.7",
                    alignItems: "center",
                    justifyContent: "center",
                    display: "flex",
                  }}
                >
                  <FaSearch style={{ color: "#ffffff" }} />
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  );
};

export default SliderGalary;
