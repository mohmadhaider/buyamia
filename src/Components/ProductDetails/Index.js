import React, { useEffect, useState, useRef } from "react";
import s from "./Index.module.css";
import Link from "next/link";
// Json Data of Product
import dummyProduct from "../../data/dummyproduct.json";
// Button Tab
import ProductBottomTabs from "./ProductBottomTabs";
import SliderGalary from "./SliderGalary3";
import CategoryWiseProduct from "../Index/CategoryWiseProduct";
import MeetTheMakers from "../ProductDetails/MeetTheMakers";
import { Container, Row, Col, Form, Modal, ModalBody } from "reactstrap";
// Icon From Maerial Ui 
import { FaTimes } from "react-icons/fa";
// Rating Component fROM Matrial UI
import Rating from "@mui/material/Rating";

const Index = ({ ProductData, ProductCategoryData }) => {
  ProductData = ProductData[0];
  const [open, setOpen] = useState(false);
  const [showMore, setShowMore] = useState(false);
  const DvScrollbar = useRef(null);
  useEffect(() => {
    if (showMore == true) {
      DvScrollbar.current.scrollTo(0, 0);
    }
  }, [showMore]);
  return (
    <>
      <section>
        <Container className="pt-4">
          <Row>
            <Col lg="6" className="order-1 order-lg-1">
              <SliderGalary ProductData={ProductData.mainimagepath} />
            </Col>
            <Col lg="6" className={"pl-lg-4 order-2 order-lg-2 "}>
              <div clsssName={s.Header}>
                <div
                  className="d-flex"
                  style={{ justifyContent: "space-between" }}
                >
                  <span style={{ color: "#666666", fontSize: "15px" }}>
                    {ProductData.creator
                      ? ProductData.creator
                      : "The Bali Curator"}
                  </span>
                </div>
                <span
                  className="mb-0"
                  style={{
                    textTransform: "uppercase",
                    fontWeight: "600",
                    fontSize: "20px",
                  }}
                >
                  {ProductData.productname}
                </span>
                <div className="d-flex mb-3">
                  <Rating
                    name="half-rating"
                    contentEditable={false}
                    defaultValue={2.5}
                    precision={4}
                    style={{ color: "#e88f48" }}
                  />
                  &nbsp;&nbsp;(25)
                </div>
                <div className="d-flex flex-row flex-sm-row align-items-sm-center justify-content-sm-between">
                  <ul className="list-inline mb-2 mb-sm-0">
                    <li
                      className="list-inline-item h4 font-weight-light mb-0"
                      style={{ color: "#000" }}
                    >
                      <span
                        style={{
                          color: "#000",
                          fontSize: "18px",
                          fontWeight: "400",
                        }}
                      >
                        IDR{" "}
                        {ProductData.rate
                          .toFixed(2)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                        <Link href="#">
                          <span
                            onClick={(e) => setOpen(true)}
                            style={{
                              color: "#000",
                              fontSize: "18px",
                              fontWeight: "400",
                              cursor: "pointer",
                              textDecoration: "underline",
                            }}
                          >
                            (Min.Order {ProductData.moq})
                          </span>
                        </Link>
                      </span>
                    </li>
                    <li
                      className="list-inline-item h4 font-weight-light mb-0"
                      style={{ color: "#000" }}
                    ></li>
                  </ul>
                </div>
                {ProductData.msrp && (
                  <div style={{ marginBottom: "40px" }}>
                    <Row>
                      <span
                        style={{
                          color: "#000",
                          fontSize: "14px",
                          fontWeight: "400",
                          opacity: "0.8",
                        }}
                      >
                        Retail Price : IDR{" "}
                        {ProductData.msrp
                          .toFixed(2)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </span>
                    </Row>
                  </div>
                )}
              </div>

              <div
                className={s.Content + " " + (showMore && s.ScrollBar)}
                ref={DvScrollbar}
              >
                <Form>
                  <Row>
                    <Col sm="3" lg="3" className="detail-option mb-2">
                      <span
                        className="detail-option-heading"
                        style={{
                          color: "#000",
                          fontSize: "15px",
                          fontWeight: "200",
                        }}
                      >
                        Order Qty
                      </span>
                      <div
                        style={{
                          display: "flex",
                          maxHeight: "min-content",
                          border: "1px solid #000",
                          alignItems: "center",
                          height: "33px",
                        }}
                      >
                        <input
                          type="number"
                          placeholder="5"
                          style={{
                            width: "100%",
                            border: "none !important",
                            textDecoration: "none",
                            outline: "none !important",
                            padding: "2px 5px",
                            color: "#000",
                            height: "100%",
                          }}
                        />
                      </div>
                    </Col>
                    <Col sm="6" lg="5" className="detail-option mb-2">
                      <span
                        className="detail-option-heading"
                        style={{
                          color: "#000",
                          fontSize: "15px",
                          fontWeight: "200",
                        }}
                      >
                        Style
                      </span>
                      <div
                        style={{
                          display: "flex",
                          maxHeight: "min-content",
                          border: "1px solid #000",
                          alignItems: "center",
                          height: "33px",
                        }}
                      >
                        <select
                          style={{
                            width: "100%",
                            border: "none",
                            textDecoration: "none",
                            outline: "none",
                            padding: "2px 5px",
                            color: "#666666",
                            height: "100%",
                          }}
                        >
                          <option>Moroccan</option>
                          {/* {dummyProduct.types.map((item, index) => (
                            <option key={index}>{item.label}</option>
                          ))} */}
                        </select>
                      </div>
                    </Col>
                    <Col sm="6" lg="3" className="detail-option mb-2">
                      <span
                        className="detail-option-heading"
                        style={{
                          color: "#000",
                          fontSize: "15px",
                          fontWeight: "200",
                        }}
                      >
                        Material
                      </span>
                      <div
                        style={{
                          display: "flex",
                          maxHeight: "min-content",
                          border: "1px solid #000",
                          alignItems: "center",
                          height: "33px",
                        }}
                      >
                        <select
                          style={{
                            width: "100%",
                            border: "none",
                            textDecoration: "none",
                            outline: "none",
                            padding: "2px 5px",
                            color: "#666666",
                            height: "100%",
                          }}
                        >
                          <option>Wooden</option>
                          <option>Metalic</option>
                        </select>
                        {/* <SelectBox options={dummyProduct.types} style={{width:"100%"}} /> */}
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg="12" sm="12">
                      <span style={{ fontSize: "15px" }}>
                        <b>Samples: </b>
                        IDR 40,000.00/Piece | 1 Piece (Min Order){" "}
                      </span>
                    </Col>
                  </Row>
                  <br />
                  <Row>
                    <Col
                      sm="12"
                      lg="8"
                      className="d-flex mb-2"
                      style={{ justifyContent: "space-between" }}
                    >
                      <div
                        style={{
                          backgroundColor: "#c8a27c",
                          borderRadius: "10px",
                          color: "#ffffff",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          height: "35px",
                          cursor: "pointer",
                        }}
                      >
                        <span
                          style={{ fontSize: "14px", fontWeight: "500" }}
                          className="px-5"
                        >
                          Add to Cart
                        </span>
                      </div>
                      <div
                        style={{
                          backgroundColor: "#000",
                          borderRadius: "10px",
                          color: "#ffffff",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          height: "35px",
                          cursor: "pointer",
                        }}
                      >
                        <span
                          style={{ fontSize: "14px", fontWeight: "500" }}
                          className="px-4"
                        >
                          Get Custom Quote
                        </span>
                      </div>
                    </Col>
                  </Row>

                  <Row>
                    <Col sm="12" lg="10">
                      {!showMore ? (
                        <>
                          <p
                            className={"mb-4 " + s.ParagraphEffect}
                            style={{ fontWeight: "450", fontSize: "15px" }}
                          >
                            {ProductData.productdesc}
                          </p>
                          <span>
                            <span
                              style={{
                                cursor: "pointer",
                                color: "rgb(83 83 83)",
                                fontWeight: "550",
                              }}
                              onClick={(e) => setShowMore(!showMore)}
                            >
                              Show More
                            </span>
                          </span>
                        </>
                      ) : (
                        <>
                          <p
                            className={"mb-4"}
                            style={{
                              color: "#000",
                              fontWeight: "450",
                              fontSize: "15px",
                            }}
                          >
                            {ProductData.productdesc}
                          </p>

                          <br />
                        </>
                      )}
                    </Col>
                  </Row>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      {showMore && (
        <section>
          <Container>
            <ProductBottomTabs
              product={dummyProduct}
              ProductData={ProductData}
            />
            <div className="d-flex" style={{ justifyContent: "space-between" }}>
              <div>&nbsp;</div>
              <span>
                <span
                  style={{
                    cursor: "pointer",
                    color: "rgb(83 83 83)",
                    fontWeight: "550",
                  }}
                  onClick={(e) => setShowMore(!showMore)}
                >
                  Show Less
                </span>
              </span>
            </div>
          </Container>
        </section>
      )}
      <div className="mt-5">
        <MeetTheMakers />
      </div>

      <CategoryWiseProduct
        CategoryName="More from this maker"
        Size={15}
        ProductCategoryData={ProductCategoryData}
      />

      <Modal
        isOpen={open}
        toggle={() => setOpen(false)}
        // backdrop="static"
        keyboard={false}
        size="md"
      >
        <div style={{ position: "relative" }}>
          <FaTimes
            style={{
              position: "absolute",
              right: "0",
              margin: "10px",
              cursor: "pointer",
              color: "#e88f48",
              fontSize: "20px",
            }}
            onClick={(e) => setOpen(false)}
          />
        </div>
        <br />
        <ModalBody>
          <div
            className="table-responsive"
            style={{ fontSize: "16px", Color: "#666666", opacity: "0.8" }}
          >
            <table className="table">
              <tbody>
                <tr>
                  <th style={{ border: "none" }}>Qty</th>
                  <th style={{ border: "none" }}>Price p.p</th>
                  <th style={{ border: "none" }}>Est. Lead Time</th>
                </tr>
                <tr>
                  <td>12-48</td>
                  <td> IDR 1,950.00</td>
                  <td>4-7 days</td>
                </tr>
                <tr>
                  <td>49-120</td>
                  <td> IDR 2,000.00</td>
                  <td>8-12 days</td>
                </tr>
                <tr>
                  <td>120+</td>
                  <td> IDR 1,900.00</td>
                  <td>12-15 days</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default Index;
