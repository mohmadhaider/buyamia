import React from "react";
import { FaStar } from "react-icons/fa";

const Stars = (props) => {
  const starsArray = [];
  for (let i = 1; i <= 5; i++) {
    i <= props.stars
      ? starsArray.push(
          <FaStar
            key={i}
            style={{ color: "#c8a27c" }}
            className={`fa fa-star ${props.starClass ? props.starClass : ""}`}
          />
        )
      : starsArray.push(
          <FaStar
            key={i}
          />
        );
  }
  return (
    <div className={props.className + " d-flex align-items-center"}>
      {starsArray}&nbsp;&nbsp;
      <div className="text-sm">
        <span className="text-muted text-uppercase">(25)</span>
      </div>
    </div>
  );
};

export default Stars;
