import React from "react";
import s from "./Meet.module.css";
import { Col, Row } from "reactstrap";

//Display Content
const MeetTheMakers = () => {
  return (
    <Row style={{ padding: "0", margin: "0" }}>
      <Col lg="7" md="7" sm="12" xs="12" style={{ padding: "0" }}>
        <img src={"/Images/4.png"} style={{ width: "100%" }} />
      </Col>
      <Col
        lg="5"
        md="5"
        sm="12"
        xs="12"
        style={{
          backgroundColor: "rgb(200 162 124);",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div className={s.InfoContainer}>
          <span className={s.image1Heading}>MEET THE MAKERS</span>
          <br />
          <span className={s.image1Title}>THE BALI CURATOR</span>
          <br />
          <span className={s.image1Desc}>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
            volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
            ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
            consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
            velit esse molestie consequat, vel illum dolore eu feugiat nulla
            facilisis at vero eros et accumsan et iusto odio dignissim qui
            blandit praesent luptatum zzril delenit augue duis dolore te feugait
            nulla facilisi. Lorem ipsum dolor sit amet, cons ectetuer adipiscing
            elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
            magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
            nostrud exerci tation ullamcorper suscipit lobortis nisl ut PLAY
            aliquip ex ea commodo consequat.
          </span>
        </div>
      </Col>
    </Row>
  );
};

export default MeetTheMakers;
