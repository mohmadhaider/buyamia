import s from "./SliderGalary.module.css";
import { Container } from "reactstrap";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
//SLider
import { Carousel } from "react-responsive-carousel";

// List of Images in slider
const SliderGalary = ({ ProductData }) => {
  return (
    <>
      <Container>
        <div className={s.container}>
          <Carousel showArrows={true} autoPlay>
            {ProductData?.split(",").map((image, index) => (
              <div key={index}>
                <img src={image} />
              </div>
            ))}
          </Carousel>
        </div>
      </Container>
    </>
  );
};

export default SliderGalary;
