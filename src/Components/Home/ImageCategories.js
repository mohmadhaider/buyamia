import React from 'react'
import s from './ImageCategories.module.css'

const ImageCategories = () => {
    return (
      <div className={s.gallery}>
        <div className={s.gallery__item1}></div>
        <div className={s.gallery__item2}></div>
        <div className={s.gallery__item3}></div>
      </div>
    );
}

export default ImageCategories
