import React from "react";
import s from "./ImageCategories.module.css";
import { Button } from "reactstrap";

const ImageCategories = () => {
  return (
    <div className={s.gridRow}>
      <div className={s.gridColumn1}>
        <img layout="fill" src={"/Images/Designer2.png"} />
        <div className={s.ImageOverlay}>
          <span className={s.image1Heading}>THE LATEST PRODUCTS IN</span>
          <br />
          <span className={s.image1Title}>HOME & LIVING</span>
          <br />
          <span className={s.image1Desc}>
            A success story about what makes us great, the makers and their
            products, backgrounds and how they are there for you.
          </span>
          <br />
          <Button>Shop Now</Button>
        </div>
      </div>
      <div className={s.gridColumn2}>
        <img layout="fill" src={"/Images/Designer.png"} />
        <div className={s.Image3Container}></div>
      </div>
    </div>
  );
};

export default ImageCategories;
