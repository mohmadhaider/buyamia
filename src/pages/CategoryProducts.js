import MainHeader from "../Components/Index/MainHeader"
import Header from "../Components/Header/Header";
import SupplyChain from "../Components/Index/SupplyChain";
import Footer from "../Components/Index/Footer";
import Product from '../Components/CategoryProducts/Products'
import Imageshome from "../Components/CategoryProducts/imageshome";


export default function CategoryHome({ ProductData, ProductCategoryData, CategoryName }) {
  return (
    <>
      <MainHeader />
      <Header />
      <Imageshome/>
      <Product
        ProductData={ProductData}
        ProductCategoryData={ProductCategoryData}
        CategoryName={CategoryName}
      />
      <SupplyChain />
      <Footer />
    </>
  );
}

export async function getServerSideProps({ req, res, params }) {
  let CategoryName = "HOME_LIVING";

  const url = "https://hzlvonfjd2.execute-api.ap-south-1.amazonaws.com/dev/";
  const productAllData = await fetch(url + "getproduct");
  const ProductData = await productAllData.json();

  const productCategoryData = await fetch(url + "getproductbycategory", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ category: CategoryName }),
  });
  const ProductCategoryData = await productCategoryData.json();

 
  return {
    props: {
      ProductData,
      ProductCategoryData,
      CategoryName,
    },
  };
}

