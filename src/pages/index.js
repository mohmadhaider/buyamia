import MainHeader from "../Components/Index/MainHeader";
import MeetTheMakers from "../Components/Index/MeetTheMakers";
import Details from "../Components/Index/Details";
import DropshipHotlist from "../Components/Index/DropshipHotlist";
import Category from "../Components/Index/Category";
import CategoryWiseProduct from "../Components/Index/CategoryWiseProduct";
import BehindTheSeen from "../Components/Index/BehindTheScenes";
import Imageshome from "../Components/Index/imageshome";
import Header from "../Components/Header/Header";
import Footer from "../Components/Index/Footer";
import MainContent from "../Components/Index/MainContent";
//Home Page
export default function Home({
  ProductData,
  ProductCategoryData,
  ProductCategoryData2,
  ProductCategoryData3,
}) {
  return (
    <div>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <MainHeader />
      <Header />
      <div
        style={{
          maxWidth: "1520px",
          margin: "0 auto",
        }}
      >
        <MainContent />
        <Imageshome />
        <Details />
        <DropshipHotlist products={ProductData?.data} />
        <Category />
        <MeetTheMakers />
        <CategoryWiseProduct
          CategoryName="Home And Living"
          ProductCategoryData={ProductCategoryData?.data}
        />
        <CategoryWiseProduct
          CategoryName="Art And Antique"
          Size={5}
          ProductCategoryData={ProductCategoryData2?.data.concat(
            ProductCategoryData3?.data
          )}
        />
        <BehindTheSeen />
      </div>

      <Footer />
    </div>
  );
}

export async function getServerSideProps({ req, res, params }) {
  let CategoryId = 7;

  // Get All ProductData
  const url = "https://axshlo8do5.execute-api.ap-south-1.amazonaws.com/dev/";
  const productAllData = await fetch(url + "getproduct");
  const ProductData = await productAllData.json();
  // Get Product Data by Category
  const productCategoryData = await fetch(url + "getproductbycategory", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ categoryid: CategoryId }),
  });
  const ProductCategoryData = await productCategoryData.json();
  // Get Product Data by Category
  let CategoryId2 = 1;
  const productCategoryData2 = await fetch(url + "getproductbycategory", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ categoryid: CategoryId2 }),
  });
  const ProductCategoryData2 = await productCategoryData2.json();
  // Get Product Data by Category

  let CategoryId3 = 3;
  const productCategoryData3 = await fetch(url + "getproductbycategory", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ categoryid: CategoryId3 }),
  });
  const ProductCategoryData3 = await productCategoryData3.json();
  return {
    props: {
      ProductData,
      ProductCategoryData,
      ProductCategoryData2,
      ProductCategoryData3,
      CategoryName: "Home And Living",
      CategoryName2: "Art And Antique",
      CategoryName3: "Beauty",
    },
  };
}
