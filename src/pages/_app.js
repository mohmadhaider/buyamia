import Head from "next/head";
import "./global.css";
import "bootstrap/dist/css/bootstrap.min.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Buyamia</title>
        <meta name="description" content="Generated by create next app" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1"
        ></meta>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="preload"
          href="/Fonts/Roboto-Regular.ttf"
          as="font"
          crossOrigin="true"
        />
        <link
          rel="preload"
          href="/Fonts/Roboto-Medium.ttf"
          as="font"
          crossOrigin="true"
        />
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
