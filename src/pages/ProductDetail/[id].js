import React from "react";
import ProductDetail from "../../Components/ProductDetails/Index";
import MainHeader from "../../Components/Index/MainHeader";
import Header from "../../Components/Header/Header";
import CategoryWiseProduct from "../../Components/Index/CategoryWiseProduct";
import BehindTheSeen from "../../Components/Index/BehindTheScenes";
import Footer from "../../Components/Index/Footer";

// Display Product Details
const ProductDetails = ({
  ProductData,
  ProductAllData,
  ProductCategoryData,
}) => {
  return (
    <>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <MainHeader />
      <Header />
      <ProductDetail
        ProductData={ProductData.data}
        ProductAllData={ProductAllData}
        ProductCategoryData={ProductCategoryData?.data}
      />
      <CategoryWiseProduct ProductCategoryData={ProductCategoryData?.data} />
      <BehindTheSeen />
      <Footer />
    </>
  );
};

export async function getServerSideProps({ req, res, params }) {
  let productid = params.id;
  const url = "https://axshlo8do5.execute-api.ap-south-1.amazonaws.com/dev/";
  //Get Single Product Details by Id
  const productData = await fetch(url + "getsingleproduct", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ productid: productid }),
  });
  const ProductData = await productData.json();
  //Get All Product Detials
  const productAllRes = await fetch(url + "getproduct");
  const ProductAllData = await productAllRes.json();

  // Get Product Cattegory Details
  let CategoryName = 7;
  const productCategoryData = await fetch(url + "getproductbycategory", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ categoryid: CategoryName }),
  });
  const ProductCategoryData = await productCategoryData.json();

  return {
    props: {
      ProductData,
      ProductAllData,
      ProductCategoryData,
    },
  };
}

export default ProductDetails;
