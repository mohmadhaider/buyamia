(() => {
var exports = {};
exports.id = 126;
exports.ids = [126];
exports.modules = {

/***/ 59630:
/***/ ((module) => {

// Exports
module.exports = {
	"shopnowbtn": "imageshome_shopnowbtn__XRpdL",
	"shopnowbtn2": "imageshome_shopnowbtn2__53lyF",
	"category": "imageshome_category__IpVor",
	"uppertext": "imageshome_uppertext__PQVPY",
	"categorytitle": "imageshome_categorytitle___bw2B",
	"smallfont": "imageshome_smallfont__ptpre",
	"img2Title": "imageshome_img2Title__ONWND",
	"img2Desc": "imageshome_img2Desc__z4i8Q",
	"img2Container": "imageshome_img2Container__yUi2E"
};


/***/ }),

/***/ 698489:
/***/ ((module) => {

// Exports
module.exports = {
	"supplyChain": "SupplyChain_supplyChain__tTqqa",
	"supplyChainHeader": "SupplyChain_supplyChainHeader__skBnC",
	"supplyChainInfo": "SupplyChain_supplyChainInfo__uqVMP"
};


/***/ }),

/***/ 390819:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps),
/* harmony export */   "getStaticPaths": () => (/* binding */ getStaticPaths),
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "unstable_getStaticParams": () => (/* binding */ unstable_getStaticParams),
/* harmony export */   "unstable_getStaticProps": () => (/* binding */ unstable_getStaticProps),
/* harmony export */   "unstable_getStaticPaths": () => (/* binding */ unstable_getStaticPaths),
/* harmony export */   "unstable_getServerProps": () => (/* binding */ unstable_getServerProps),
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "_app": () => (/* binding */ _app),
/* harmony export */   "renderReqToHTML": () => (/* binding */ renderReqToHTML),
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(170607);
/* harmony import */ var next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(659450);
/* harmony import */ var private_dot_next_build_manifest_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(297020);
/* harmony import */ var private_dot_next_react_loadable_manifest_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(973978);
/* harmony import */ var next_dist_build_webpack_loaders_next_serverless_loader_page_handler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(999436);

      
      
      
      

      
      const { processEnv } = __webpack_require__(972333)
      processEnv([])
    
      
      const runtimeConfig = {}
      ;

      const documentModule = __webpack_require__(423105)

      const appMod = __webpack_require__(952654)
      let App = appMod.default || appMod.then && appMod.then(mod => mod.default);

      const compMod = __webpack_require__(568384)

      const Component = compMod.default || compMod.then && compMod.then(mod => mod.default)
      /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Component);
      const getStaticProps = compMod['getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['getStaticProp' + 's'])
      const getStaticPaths = compMod['getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['getStaticPath' + 's'])
      const getServerSideProps = compMod['getServerSideProp' + 's'] || compMod.then && compMod.then(mod => mod['getServerSideProp' + 's'])

      // kept for detecting legacy exports
      const unstable_getStaticParams = compMod['unstable_getStaticParam' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticParam' + 's'])
      const unstable_getStaticProps = compMod['unstable_getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticProp' + 's'])
      const unstable_getStaticPaths = compMod['unstable_getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticPath' + 's'])
      const unstable_getServerProps = compMod['unstable_getServerProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getServerProp' + 's'])

      let config = compMod['confi' + 'g'] || (compMod.then && compMod.then(mod => mod['confi' + 'g'])) || {}
      const _app = App

      const combinedRewrites = Array.isArray(private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg)
        ? private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg
        : []

      if (!Array.isArray(private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg)) {
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.beforeFiles */ .Dg.beforeFiles)
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.afterFiles */ .Dg.afterFiles)
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.fallback */ .Dg.fallback)
      }

      const { renderReqToHTML, render } = (0,next_dist_build_webpack_loaders_next_serverless_loader_page_handler__WEBPACK_IMPORTED_MODULE_4__/* .getPageHandler */ .u)({
        pageModule: compMod,
        pageComponent: Component,
        pageConfig: config,
        appModule: App,
        documentModule: documentModule,
        errorModule: __webpack_require__(89185),
        notFoundModule: undefined,
        pageGetStaticProps: getStaticProps,
        pageGetStaticPaths: getStaticPaths,
        pageGetServerSideProps: getServerSideProps,

        assetPrefix: "",
        canonicalBase: "",
        generateEtags: true,
        poweredByHeader: true,

        runtimeConfig,
        buildManifest: private_dot_next_build_manifest_json__WEBPACK_IMPORTED_MODULE_2__,
        reactLoadableManifest: private_dot_next_react_loadable_manifest_json__WEBPACK_IMPORTED_MODULE_3__,

        rewrites: combinedRewrites,
        i18n: undefined,
        page: "/CategoryProducts",
        buildId: "-8BASkNRMpFB4EzFQPodJ",
        escapedBuildId: "\-8BASkNRMpFB4EzFQPodJ",
        basePath: "",
        pageIsDynamic: false,
        encodedPreviewProps: {previewModeId:"5749ed080bc6d9c7ebcecb7513493b79",previewModeSigningKey:"2266d3b39b76a0a30863e0a788a8d78fe2340f025f300dba6629a72fb4b2bcc3",previewModeEncryptionKey:"13591eaeda5d8bda3e73917bc1b739f4177e980b7a755916babce9e7144859db"}
      })
      
    

/***/ }),

/***/ 568384:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ CategoryHome),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: ./node_modules/react/jsx-runtime.js
var jsx_runtime = __webpack_require__(785893);
// EXTERNAL MODULE: ./src/Components/Index/MainHeader.js
var MainHeader = __webpack_require__(403279);
// EXTERNAL MODULE: ./src/Components/Header/Header.js + 1 modules
var Header = __webpack_require__(886248);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(425675);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(667294);
// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.modern.js
var reactstrap_modern = __webpack_require__(250450);
// EXTERNAL MODULE: ./src/Components/Index/SupplyChain.module.css
var SupplyChain_module = __webpack_require__(698489);
var SupplyChain_module_default = /*#__PURE__*/__webpack_require__.n(SupplyChain_module);
;// CONCATENATED MODULE: ./src/Components/Index/SupplyChain.js





const SupplyChain = ()=>{
    return(/*#__PURE__*/ jsx_runtime.jsx("div", {
        className: (SupplyChain_module_default()).supplyChain,
        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Container */.W2, {
            className: "text-center",
            children: [
                /*#__PURE__*/ jsx_runtime.jsx("img", {
                    src: "/Images/Surity.png",
                    style: {
                        width: "100px"
                    }
                }),
                /*#__PURE__*/ jsx_runtime.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime.jsx("span", {
                    className: (SupplyChain_module_default()).supplyChainHeader,
                    children: "Creating a 100% conscious supply chain"
                }),
                /*#__PURE__*/ jsx_runtime.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime.jsx("span", {
                    className: (SupplyChain_module_default()).supplyChainInfo,
                    children: "BUYAMIA is aiming to provide the many small and micro manufacturers in Indonesia a platform to sell to the rest of the world. From Rural to Main Street. With a WE-Commerce approach we highlight both the products you love and enjoy as well as the artisans behind them."
                }),
                /*#__PURE__*/ jsx_runtime.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime.jsx("br", {
                }),
                /*#__PURE__*/ jsx_runtime.jsx("span", {
                    className: (SupplyChain_module_default()).supplyChainInfo,
                    children: "We differ from the traditional E-Commerce platforms as we offer extra services and dedicate ourselves to curated quality products and full service to take your headaches away by streamlining entire process."
                })
            ]
        })
    }));
};
/* harmony default export */ const Index_SupplyChain = (SupplyChain);

// EXTERNAL MODULE: ./src/Components/Index/Footer.js
var Footer = __webpack_require__(744332);
;// CONCATENATED MODULE: ./src/Components/CategoryProducts/Categories.js


const Categories = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        style: {
            padding: "20px"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("h4", {
                children: "All Categories"
            }),
            /*#__PURE__*/ jsx_runtime.jsx("br", {
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("ul", {
                style: {
                    listStyleType: "none"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Apparel"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Beauty"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Food & Drinks"
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("li", {
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx("h5", {
                                children: "Home & Lifestyle"
                            }),
                            /*#__PURE__*/ (0,jsx_runtime.jsxs)("ul", {
                                style: {
                                    listStyleType: "none"
                                },
                                children: [
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Bathroom"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Bedroom"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Canldes & Holders"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Cleaning & Storage"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Garden & Outdoor"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Home Decor"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Home Fragrance"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Kitchen & Dining"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime.jsx("h6", {
                                            children: "Crafts & Hobbies"
                                        })
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Jewelry"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Kids"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Stationery"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("li", {
                        children: /*#__PURE__*/ jsx_runtime.jsx("h5", {
                            children: "Art & Vintage"
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const CategoryProducts_Categories = (Categories);

// EXTERNAL MODULE: ./node_modules/@material-ui/core/index.js
var core = __webpack_require__(958189);
;// CONCATENATED MODULE: ./src/Components/CategoryProducts/MinOrder.js



const MinOrder = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        style: {
            padding: "20px"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("h5", {
                children: "Minimum Order"
            }),
            /*#__PURE__*/ jsx_runtime.jsx("br", {
            }),
            /*#__PURE__*/ jsx_runtime.jsx("ul", {
                children: /*#__PURE__*/ jsx_runtime.jsx(core.FormControl, {
                    component: "fieldset",
                    children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(core.RadioGroup, {
                        "aria-label": "gender",
                        defaultValue: "female",
                        name: "radio-buttons-group",
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(core.FormControlLabel, {
                                value: "all",
                                control: /*#__PURE__*/ jsx_runtime.jsx(core.Radio, {
                                    color: "dark"
                                }),
                                label: "Show All"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(core.FormControlLabel, {
                                value: "nm",
                                control: /*#__PURE__*/ jsx_runtime.jsx(core.Radio, {
                                    color: "dark"
                                }),
                                label: "No Minimum"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(core.FormControlLabel, {
                                value: "100",
                                control: /*#__PURE__*/ jsx_runtime.jsx(core.Radio, {
                                    color: "dark"
                                }),
                                label: "Up to $100"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(core.FormControlLabel, {
                                value: "250",
                                control: /*#__PURE__*/ jsx_runtime.jsx(core.Radio, {
                                    color: "dark"
                                }),
                                label: "Up to $250"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(core.FormControlLabel, {
                                value: "500",
                                control: /*#__PURE__*/ jsx_runtime.jsx(core.Radio, {
                                    color: "dark"
                                }),
                                label: "Up to $500"
                            })
                        ]
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const CategoryProducts_MinOrder = (MinOrder);

// EXTERNAL MODULE: ./node_modules/react-icons/fa/index.esm.js
var index_esm = __webpack_require__(889583);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/index.js
var node = __webpack_require__(707772);
;// CONCATENATED MODULE: ./src/Components/CategoryProducts/WholesalePrice.js





const WholesalePrice = ()=>{
    const { 0: value1 , 1: setValue  } = (0,react.useState)(50);
    const handleChange = (e, newval)=>{
        setValue(newval);
    // console.log(value);
    };
    function valuetext(value) {
        const val = value * 100;
        return `${val}$`;
    }
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        style: {
            padding: "20px"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("h5", {
                children: "Minimum Order"
            }),
            /*#__PURE__*/ jsx_runtime.jsx("br", {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(node.Stack, {
                    spacing: 2,
                    direction: "row",
                    sx: {
                        mb: 1
                    },
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx(index_esm/* FaDollarSign */.RcD, {
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("h6", {
                            children: "100$"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(node.Slider, {
                            "aria-label": "Volume",
                            getAriaValueText: valuetext,
                            value: value1,
                            valueLabelDisplay: "auto",
                            onChange: handleChange,
                            style: {
                                color: "black"
                            }
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("h6", {
                            children: "1000$"
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const CategoryProducts_WholesalePrice = (WholesalePrice);

// EXTERNAL MODULE: ./src/Components/Index/CategoryWiseProduct.js
var CategoryWiseProduct = __webpack_require__(617824);
;// CONCATENATED MODULE: ./src/Components/CategoryProducts/ProductPagination.js



function ProductPagination() {
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Pagination */.tl, {
        "aria-label": "Page navigation example",
        className: "text-center",
        children: [
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    previous: true,
                    href: "#"
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    href: "#",
                    children: "1"
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    href: "#",
                    children: "2"
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    href: "#",
                    children: "3"
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    href: "#",
                    children: "4"
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    href: "#",
                    children: "5"
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationItem */.nt, {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* PaginationLink */.kN, {
                    next: true,
                    href: "#"
                })
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./src/Components/CategoryProducts/Products.js








const Products = ({ ProductData , ProductCategoryData , CategoryName  })=>{
    return(/*#__PURE__*/ jsx_runtime.jsx(jsx_runtime.Fragment, {
        children: /*#__PURE__*/ jsx_runtime.jsx("div", {
            style: {
                margin: "30px 0",
                padding: "0"
            },
            children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                style: {
                    margin: "0",
                    padding: "0"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                        lg: "3",
                        md: "3",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Container */.W2, {
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx(CategoryProducts_Categories, {
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx("hr", {
                                    style: {
                                        margin: "20px",
                                        color: "black"
                                    }
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(CategoryProducts_MinOrder, {
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx("hr", {
                                    style: {
                                        margin: "20px",
                                        color: "black"
                                    }
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(CategoryProducts_WholesalePrice, {
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Button */.zx, {
                                    color: "dark",
                                    block: true,
                                    children: "Apply Now"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Col */.JX, {
                        lg: "9",
                        md: "9",
                        sm: "12",
                        xs: "12",
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(CategoryWiseProduct/* default */.Z, {
                                CategoryName: CategoryName,
                                ProductCategoryData: ProductCategoryData === null || ProductCategoryData === void 0 ? void 0 : ProductCategoryData.input[0],
                                Size: 15
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                    lg: {
                                        size: "4",
                                        offset: "4"
                                    },
                                    md: {
                                        size: "6",
                                        offset: "3"
                                    },
                                    sm: "12",
                                    xs: "12",
                                    children: /*#__PURE__*/ jsx_runtime.jsx(ProductPagination, {
                                    })
                                })
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const CategoryProducts_Products = (Products);

// EXTERNAL MODULE: ./src/Components/CategoryProducts/imageshome.module.css
var imageshome_module = __webpack_require__(59630);
var imageshome_module_default = /*#__PURE__*/__webpack_require__.n(imageshome_module);
;// CONCATENATED MODULE: ./src/Components/CategoryProducts/imageshome.js



const Imageshome = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("meta", {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                style: {
                    display: "flex",
                    flexWrap: "wrap",
                    justifyContent: "space-evenly",
                    marginTop: 40,
                    width: "100%",
                    backgroundColor: "rgb(245 243 241)"
                },
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                    style: {
                        position: "relative"
                    },
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx("img", {
                            style: {
                                width: "100%",
                                height: "100%"
                            },
                            src: "https://buyamiaimages.s3.ap-south-1.amazonaws.com/image+37.png"
                        }),
                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                            className: (imageshome_module_default()).category,
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx("label", {
                                    className: (imageshome_module_default()).uppertext,
                                    children: "THE LATEST PRODUCTS IN"
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx("label", {
                                    className: (imageshome_module_default()).categorytitle,
                                    children: "HOME & LIVING"
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx("label", {
                                    className: (imageshome_module_default()).smallfont,
                                    children: "A success story about what makes us great, the makers and their products, backgrounds and how they are there for you."
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx("button", {
                                    className: (imageshome_module_default()).shopnowbtn,
                                    children: "SHOP NOW"
                                })
                            ]
                        })
                    ]
                })
            })
        ]
    }));
};
//2800
/* harmony default export */ const imageshome = (Imageshome);

;// CONCATENATED MODULE: ./src/pages/CategoryProducts.js







function CategoryHome({ ProductData , ProductCategoryData , CategoryName  }) {
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime.jsx(MainHeader/* default */.Z, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Header/* default */.Z, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(imageshome, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(CategoryProducts_Products, {
                ProductData: ProductData,
                ProductCategoryData: ProductCategoryData,
                CategoryName: CategoryName
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Index_SupplyChain, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Footer/* default */.Z, {
            })
        ]
    }));
};
async function getServerSideProps({ req , res , params  }) {
    let CategoryName = "HOME_LIVING";
    const url = "https://hzlvonfjd2.execute-api.ap-south-1.amazonaws.com/dev/";
    const productAllData = await fetch(url + "getproduct");
    const ProductData = await productAllData.json();
    const productCategoryData = await fetch(url + "getproductbycategory", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            category: CategoryName
        })
    });
    const ProductCategoryData = await productCategoryData.json();
    return {
        props: {
            ProductData,
            ProductCategoryData,
            CategoryName
        }
    };
}


/***/ }),

/***/ 501014:
/***/ ((module) => {

"use strict";
module.exports = require("critters");

/***/ }),

/***/ 702186:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/@ampproject/toolbox-optimizer");

/***/ }),

/***/ 714300:
/***/ ((module) => {

"use strict";
module.exports = require("buffer");

/***/ }),

/***/ 706113:
/***/ ((module) => {

"use strict";
module.exports = require("crypto");

/***/ }),

/***/ 582361:
/***/ ((module) => {

"use strict";
module.exports = require("events");

/***/ }),

/***/ 657147:
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),

/***/ 113685:
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),

/***/ 795687:
/***/ ((module) => {

"use strict";
module.exports = require("https");

/***/ }),

/***/ 822037:
/***/ ((module) => {

"use strict";
module.exports = require("os");

/***/ }),

/***/ 371017:
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ }),

/***/ 863477:
/***/ ((module) => {

"use strict";
module.exports = require("querystring");

/***/ }),

/***/ 12781:
/***/ ((module) => {

"use strict";
module.exports = require("stream");

/***/ }),

/***/ 371576:
/***/ ((module) => {

"use strict";
module.exports = require("string_decoder");

/***/ }),

/***/ 257310:
/***/ ((module) => {

"use strict";
module.exports = require("url");

/***/ }),

/***/ 473837:
/***/ ((module) => {

"use strict";
module.exports = require("util");

/***/ }),

/***/ 959796:
/***/ ((module) => {

"use strict";
module.exports = require("zlib");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [244,765,189,772,447,638], () => (__webpack_exec__(390819)));
module.exports = __webpack_exports__;

})();