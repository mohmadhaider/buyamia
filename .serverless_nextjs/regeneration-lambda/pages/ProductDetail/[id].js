(() => {
var exports = {};
exports.id = 552;
exports.ids = [552];
exports.modules = {

/***/ 70923:
/***/ ((module) => {

// Exports
module.exports = {
	"Header": "Index_Header__fF0iw",
	"Content": "Index_Content__edu5h",
	"ParagraphEffect": "Index_ParagraphEffect__w1oyt",
	"RemoveParagraphEffect": "Index_RemoveParagraphEffect__k9JN4"
};


/***/ }),

/***/ 936605:
/***/ ((module) => {

// Exports
module.exports = {
	"InfoContainer": "Meet_InfoContainer__P0wPp",
	"image1Heading": "Meet_image1Heading__PXaAT",
	"image1Title": "Meet_image1Title__EHOV1",
	"image1Desc": "Meet_image1Desc__kIouR"
};


/***/ }),

/***/ 861535:
/***/ ((module) => {

// Exports
module.exports = {
	"container": "SliderGalary_container__2v48D",
	"mySlides": "SliderGalary_mySlides__4oi1u",
	"cursor": "SliderGalary_cursor__pt6GR",
	"prev": "SliderGalary_prev__KXEAt",
	"next": "SliderGalary_next__PYFSy",
	"numbertext": "SliderGalary_numbertext__NJlDT",
	"caption_container": "SliderGalary_caption_container__5GQY8",
	"row": "SliderGalary_row__WqmzM",
	"column": "SliderGalary_column__DaH5Y",
	"demo": "SliderGalary_demo__jnW1q",
	"active": "SliderGalary_active__hX13D",
	"ProductImgContainer": "SliderGalary_ProductImgContainer__YCaVH"
};


/***/ }),

/***/ 886769:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps),
/* harmony export */   "getStaticPaths": () => (/* binding */ getStaticPaths),
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "unstable_getStaticParams": () => (/* binding */ unstable_getStaticParams),
/* harmony export */   "unstable_getStaticProps": () => (/* binding */ unstable_getStaticProps),
/* harmony export */   "unstable_getStaticPaths": () => (/* binding */ unstable_getStaticPaths),
/* harmony export */   "unstable_getServerProps": () => (/* binding */ unstable_getServerProps),
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "_app": () => (/* binding */ _app),
/* harmony export */   "renderReqToHTML": () => (/* binding */ renderReqToHTML),
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(170607);
/* harmony import */ var next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(659450);
/* harmony import */ var private_dot_next_build_manifest_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(297020);
/* harmony import */ var private_dot_next_react_loadable_manifest_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(973978);
/* harmony import */ var next_dist_build_webpack_loaders_next_serverless_loader_page_handler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(999436);

      
      
      
      

      
      const { processEnv } = __webpack_require__(972333)
      processEnv([])
    
      
      const runtimeConfig = {}
      ;

      const documentModule = __webpack_require__(423105)

      const appMod = __webpack_require__(952654)
      let App = appMod.default || appMod.then && appMod.then(mod => mod.default);

      const compMod = __webpack_require__(536411)

      const Component = compMod.default || compMod.then && compMod.then(mod => mod.default)
      /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Component);
      const getStaticProps = compMod['getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['getStaticProp' + 's'])
      const getStaticPaths = compMod['getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['getStaticPath' + 's'])
      const getServerSideProps = compMod['getServerSideProp' + 's'] || compMod.then && compMod.then(mod => mod['getServerSideProp' + 's'])

      // kept for detecting legacy exports
      const unstable_getStaticParams = compMod['unstable_getStaticParam' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticParam' + 's'])
      const unstable_getStaticProps = compMod['unstable_getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticProp' + 's'])
      const unstable_getStaticPaths = compMod['unstable_getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticPath' + 's'])
      const unstable_getServerProps = compMod['unstable_getServerProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getServerProp' + 's'])

      let config = compMod['confi' + 'g'] || (compMod.then && compMod.then(mod => mod['confi' + 'g'])) || {}
      const _app = App

      const combinedRewrites = Array.isArray(private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg)
        ? private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg
        : []

      if (!Array.isArray(private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg)) {
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.beforeFiles */ .Dg.beforeFiles)
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.afterFiles */ .Dg.afterFiles)
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.fallback */ .Dg.fallback)
      }

      const { renderReqToHTML, render } = (0,next_dist_build_webpack_loaders_next_serverless_loader_page_handler__WEBPACK_IMPORTED_MODULE_4__/* .getPageHandler */ .u)({
        pageModule: compMod,
        pageComponent: Component,
        pageConfig: config,
        appModule: App,
        documentModule: documentModule,
        errorModule: __webpack_require__(89185),
        notFoundModule: undefined,
        pageGetStaticProps: getStaticProps,
        pageGetStaticPaths: getStaticPaths,
        pageGetServerSideProps: getServerSideProps,

        assetPrefix: "",
        canonicalBase: "",
        generateEtags: true,
        poweredByHeader: true,

        runtimeConfig,
        buildManifest: private_dot_next_build_manifest_json__WEBPACK_IMPORTED_MODULE_2__,
        reactLoadableManifest: private_dot_next_react_loadable_manifest_json__WEBPACK_IMPORTED_MODULE_3__,

        rewrites: combinedRewrites,
        i18n: undefined,
        page: "/ProductDetail/[id]",
        buildId: "-8BASkNRMpFB4EzFQPodJ",
        escapedBuildId: "\-8BASkNRMpFB4EzFQPodJ",
        basePath: "",
        pageIsDynamic: true,
        encodedPreviewProps: {previewModeId:"5749ed080bc6d9c7ebcecb7513493b79",previewModeSigningKey:"2266d3b39b76a0a30863e0a788a8d78fe2340f025f300dba6629a72fb4b2bcc3",previewModeEncryptionKey:"13591eaeda5d8bda3e73917bc1b739f4177e980b7a755916babce9e7144859db"}
      })
      
    

/***/ }),

/***/ 536411:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _id_),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: ./node_modules/react/jsx-runtime.js
var jsx_runtime = __webpack_require__(785893);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(667294);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/Rating/index.js
var Rating = __webpack_require__(724326);
var Rating_default = /*#__PURE__*/__webpack_require__.n(Rating);
// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.modern.js
var reactstrap_modern = __webpack_require__(250450);
;// CONCATENATED MODULE: ./src/Components/ProductDetails/InquiryForm.js


function InquiryForm() {
    return(/*#__PURE__*/ _jsxs("div", {
        style: {
            display: "flex",
            flexDirection: "column",
            flexWrap: "wrap",
            padding: "50px 0px",
            justifyContent: "center",
            alignItems: "center"
        },
        children: [
            /*#__PURE__*/ _jsx("b", {
                style: {
                    fontSize: 30
                },
                children: "Send Your Message To Supplier"
            }),
            /*#__PURE__*/ _jsxs("div", {
                style: {
                    display: "flex",
                    flexDirection: "column",
                    flexWrap: "wrap",
                    padding: "50px 0px",
                    justifyContent: "center"
                },
                children: [
                    /*#__PURE__*/ _jsx("label", {
                        children: "To: Seller"
                    }),
                    /*#__PURE__*/ _jsxs("div", {
                        style: {
                            display: "flex",
                            marginTop: "10px"
                        },
                        children: [
                            /*#__PURE__*/ _jsx("label", {
                                children: "*Message:\xa0\xa0"
                            }),
                            /*#__PURE__*/ _jsx("textarea", {
                                rows: 8,
                                style: {
                                    width: "70%"
                                },
                                placeholder: "Enter your inquiry details such as product name, color, size, moq etx"
                            })
                        ]
                    }),
                    /*#__PURE__*/ _jsx("label", {
                        style: {
                            marginLeft: "60px"
                        },
                        children: "Your message must be between 20-8000 characters"
                    }),
                    /*#__PURE__*/ _jsxs("div", {
                        style: {
                            display: "flex"
                        },
                        children: [
                            /*#__PURE__*/ _jsx("label", {
                                children: "Quantity: "
                            }),
                            /*#__PURE__*/ _jsx("input", {
                                type: "number",
                                style: {
                                    marginLeft: 20
                                },
                                placeholder: "Enter quantity"
                            }),
                            /*#__PURE__*/ _jsxs("select", {
                                style: {
                                    marginLeft: 20
                                },
                                children: [
                                    /*#__PURE__*/ _jsx("option", {
                                        children: "Acre/Acres"
                                    }),
                                    /*#__PURE__*/ _jsx("option", {
                                        children: "Acre/Acres"
                                    })
                                ]
                            })
                        ]
                    }),
                    " ",
                    /*#__PURE__*/ _jsxs("div", {
                        style: {
                            display: "flex"
                        },
                        children: [
                            /*#__PURE__*/ _jsx("input", {
                                type: "checkbox"
                            }),
                            "\xa0\xa0",
                            /*#__PURE__*/ _jsxs("label", {
                                children: [
                                    "Recommend matching suppliers if this supplier doesnt contact me on Message Center within 24 hours  ",
                                    /*#__PURE__*/ _jsx("b", {
                                        children: "Request for quotation"
                                    })
                                ]
                            })
                        ]
                    }),
                    " ",
                    /*#__PURE__*/ _jsxs("div", {
                        style: {
                            display: "flex"
                        },
                        children: [
                            /*#__PURE__*/ _jsx("input", {
                                type: "checkbox"
                            }),
                            "\xa0\xa0",
                            /*#__PURE__*/ _jsxs("label", {
                                children: [
                                    "I agree to share my ",
                                    /*#__PURE__*/ _jsx("b", {
                                        children: "Business Card"
                                    }),
                                    " to the supplier"
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ _jsx("div", {
                        children: /*#__PURE__*/ _jsx("button", {
                            style: {
                                marginTop: 20,
                                padding: "10px 20px",
                                background: "orange",
                                border: 0,
                                borderRadius: 6,
                                color: "#fff"
                            },
                            children: "Send"
                        })
                    })
                ]
            })
        ]
    }));
}
/* harmony default export */ const ProductDetails_InquiryForm = ((/* unused pure expression or super */ null && (InquiryForm)));

;// CONCATENATED MODULE: ./src/data/dummyproduct.json
const dummyproduct_namespaceObject = JSON.parse('{"name":"Wooden Table","category":"Home & LifeStyle","price":90,"pricesale":65,"stars":4,"reviewscount":25,"img":{"category":[{"img":"/img/product/0987188250_1_1_1.jpg","alt":"product"},{"img":"/img/product/0987188250_2_1_1.jpg","alt":"product"}],"masonry":{"img":"/img/product/serrah-galos-494312-unsplash-cropped.jpg","alt":"product"},"detail":[{"img":"/img/product/detail-3-gray.jpg","caption":"Push-up Jeans 1 - Caption text","alt":"Push-up Jeans 1"},{"img":"/img/product/detail-1-gray.jpg","caption":"Push-up Jeans 2 - Caption text","alt":"Push-up Jeans 2"},{"img":"/img/product/detail-2-gray.jpg","caption":"Push-up Jeans 3 - Caption text","alt":"Push-up Jeans 3"},{"img":"/img/product/detail-4-gray.jpg","caption":"Push-up Jeans 4 - Caption text","alt":"Push-up Jeans 4"},{"img":"/img/product/detail-5-gray.jpg","caption":"Push-up Jeans 5 - Caption text","alt":"Push-up Jeans 5"},{"img":"/img/product/detail-6.jpg","caption":"Push-up Jeans 6 - Caption text","alt":"Push-up Jeans 6"}]},"description":{"short":"Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.","long":"<h5>About</h5><p class=\'text-muted\'>Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.</p><p class=\'text-muted\'>He must have tried it a hundred times, shut his eyes so that he wouldn\'t have to look at the floundering legs, and only stopped when he began to feel a mild, dull pain there that he had never felt before.</p><h5>You will love</h5><ul class=\'text-muted\'><li>He must have tried it a hundred times</li><li>shut his eyes so that he wouldn\'t have to look</li><li>at the floundering legs, and only stopped</li></ul>","image":"Images/ProductDetails/6.png"},"additionalinfo":[{"name":"Product #","text":"Lorem ipsum dolor sit amet"},{"name":"Available packaging","text":"LOLDuis aute irure dolor in reprehenderit"},{"name":"Weight","text":"dolor sit amet"},{"name":"Sunt in culpa qui","text":"Lorem ipsum dolor sit amet"},{"name":"Weight","text":"dolor sit amet"},{"name":"Sunt in culpa qui","text":"Lorem ipsum dolor sit amet"},{"name":"Product #","text":"Lorem ipsum dolor sit amet"},{"name":"Available packaging","text":"LOLDuis aute irure dolor in reprehenderit"}],"reviews":[{"author":"Han Solo","date":"Dec 2018","avatar":"/img/avatar/person-1.jpg","stars":5,"text":"One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections"},{"author":"Luke Skywalker","date":"Dec 2018","avatar":"/img/avatar/person-2.jpg","stars":4,"text":"The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. &quot;What\'s happened to me?&quot; he thought. It wasn\'t a dream."},{"author":"Princess Leia","date":"Dec 2018","avatar":"/img/avatar/person-3.jpg","stars":3,"text":"His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table."},{"author":"Jabba Hut","date":"Dec 2018","avatar":"/img/avatar/person-4.jpg","stars":5,"text":"Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame."}],"attributes":[{"name":"Size","value":"Large"},{"name":"Colour","value":"Green"}],"sizes":[{"value":"value_0","label":"12-48 $45.55 p.p"},{"value":"value_1","label":"Medium"},{"value":"value_2","label":"Large"}],"types":[{"value":"value_0","id":"material_0","label":"Beech Wood Venner"},{"value":"value_1","id":"material_0","label":"Steamed Beech Veneer"}],"quantities":[{"value":"0","id":"0","label":"12-48     $45.55 p.p"},{"value":"1","id":"1","label":"48-120     $40.65 p.p"},{"value":"2","id":"2","label":"120+      $36.00 p.p"}],"tags":[{"name":"Homemade","link":"#"},{"name":"Wooden","link":"#"}]}');
;// CONCATENATED MODULE: ./src/Components/ProductDetails/Stars.js



const Stars = (props)=>{
    const starsArray = [];
    for(let i = 1; i <= 5; i++){
        i <= props.stars ? starsArray.push(/*#__PURE__*/ _jsx(FaStar, {
            style: {
                color: "#c8a27c"
            },
            className: `fa fa-star ${props.starClass ? props.starClass : ""}`
        }, i)) : starsArray.push(/*#__PURE__*/ _jsx(FaStar, {
        }, i));
    }
    return(/*#__PURE__*/ _jsxs("div", {
        className: props.className + " d-flex align-items-center",
        children: [
            starsArray,
            "\xa0\xa0",
            /*#__PURE__*/ _jsx("div", {
                className: "text-sm",
                children: /*#__PURE__*/ _jsx("span", {
                    className: "text-muted text-uppercase",
                    children: "(25)"
                })
            })
        ]
    }));
};
/* harmony default export */ const ProductDetails_Stars = ((/* unused pure expression or super */ null && (Stars)));

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(741664);
;// CONCATENATED MODULE: ./src/Components/ProductDetails/ReviewForm.js



const ReviewForm = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        className: "py-5 px-3",
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("h5", {
                className: "mb-4",
                children: "Leave a review"
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Form */.l0, {
                className: "mb-4 form",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                sm: "6",
                                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* FormGroup */.cw, {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Label */.__, {
                                            for: "name",
                                            className: "form-label",
                                            children: "Your name *"
                                        }),
                                        /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Input */.II, {
                                            type: "text",
                                            name: "name",
                                            id: "name",
                                            placeholder: "Enter your name",
                                            required: true
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                sm: "6",
                                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* FormGroup */.cw, {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Label */.__, {
                                            for: "rating",
                                            className: "form-label",
                                            children: "Your name *"
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("select", {
                                            name: "rating",
                                            id: "rating",
                                            className: "custom-select focus-shadow-0",
                                            children: [
                                                "s",
                                                " ",
                                                /*#__PURE__*/ jsx_runtime.jsx("option", {
                                                    value: "2",
                                                    children: "★★☆☆☆ (2/5)"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("option", {
                                                    value: "1",
                                                    children: "★☆☆☆☆ (1/5)"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* FormGroup */.cw, {
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Label */.__, {
                                for: "email",
                                className: "form-label",
                                children: "Your name *"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Input */.II, {
                                type: "email",
                                name: "email",
                                id: "email",
                                placeholder: "Enter your email",
                                required: true
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* FormGroup */.cw, {
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Label */.__, {
                                for: "review",
                                className: "form-label",
                                children: "Review text *"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Input */.II, {
                                rows: "4",
                                type: "textarea",
                                name: "review",
                                id: "review",
                                placeholder: "Enter your review",
                                required: true
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Button */.zx, {
                        type: "submit",
                        color: "outline-dark",
                        children: "Post review"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("p", {
                className: "text-muted text-sm",
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Badge */.Ct, {
                        color: "info",
                        children: "Note"
                    }),
                    " This form shows usage of the classic Bootstrap form controls, not their underlined variants. You can choose whichever variant you want."
                ]
            })
        ]
    }));
};
/* harmony default export */ const ProductDetails_ReviewForm = (ReviewForm);

// EXTERNAL MODULE: ./node_modules/react-icons/bs/index.esm.js
var index_esm = __webpack_require__(463750);
// EXTERNAL MODULE: ./node_modules/react-icons/fa/index.esm.js
var fa_index_esm = __webpack_require__(889583);
;// CONCATENATED MODULE: ./src/Components/ProductDetails/ProductBottomTabs.js








const ProductBottomTabs = ({ product , ProductData  })=>{
    const [activeTab, setActiveTab] = react.useState(1);
    const [refundModal, setRefundModal] = react.useState(false);
    ;
    const toggleTab = (tab)=>{
        if (activeTab !== tab) setActiveTab(tab);
    };
    const groupByN = (n, data)=>{
        let result = [];
        for(let i = 0; i < data.length; i += n)result.push(data.slice(i, i + n));
        return result;
    };
    const groupedAdditionalInfo = groupByN(4, product.additionalinfo);
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("section", {
                className: "mt-5",
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Container */.W2, {
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Nav */.JL, {
                            tabs: true,
                            className: "flex-column flex-sm-row",
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavItem */.LY, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavLink */.OL, {
                                        className: `detail-nav-link ${activeTab === 1 ? "active" : ""}`,
                                        onClick: ()=>toggleTab(1)
                                        ,
                                        style: {
                                            cursor: "pointer"
                                        },
                                        children: "Specifications"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavItem */.LY, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavLink */.OL, {
                                        className: `detail-nav-link ${activeTab === 2 ? "active" : ""}`,
                                        onClick: ()=>toggleTab(2)
                                        ,
                                        style: {
                                            cursor: "pointer"
                                        },
                                        children: "Company Profile"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavItem */.LY, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavLink */.OL, {
                                        className: `detail-nav-link ${activeTab === 3 ? "active" : ""}`,
                                        onClick: ()=>toggleTab(3)
                                        ,
                                        style: {
                                            cursor: "pointer"
                                        },
                                        children: "Reviews"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavItem */.LY, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* NavLink */.OL, {
                                        className: `detail-nav-link ${activeTab === 4 ? "active" : ""}`,
                                        onClick: ()=>toggleTab(4)
                                        ,
                                        style: {
                                            cursor: "pointer"
                                        },
                                        children: "FAQs"
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* TabContent */.I5, {
                            className: "py-4",
                            activeTab: activeTab,
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* TabPane */.Jm, {
                                    tabId: 1,
                                    children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                                md: "12"
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                                style: {
                                                    marginTop: "30px",
                                                    padding: "10px 20px"
                                                },
                                                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                                    md: "12",
                                                    style: {
                                                        fontSize: "15px"
                                                    },
                                                    children: /*#__PURE__*/ jsx_runtime.jsx("table", {
                                                        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("tbody", {
                                                            children: [
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Brand:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "DAOQI"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Color:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "Light Brown"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Material:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "Teak Wood"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Style & Design:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "Moroccan"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Country of Origin:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "Indonesia"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime.jsx("br", {
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Packing:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "23cm x 24cm x 2.5cm, 50cm x 48cm x 4cm"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Shipping:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "Surabya, DHL, Aramex"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Protection:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: /*#__PURE__*/ jsx_runtime.jsx(next_link["default"], {
                                                                                href: "#",
                                                                                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("span", {
                                                                                    style: {
                                                                                        color: "darkgreen",
                                                                                        fontWeight: "450",
                                                                                        cursor: "pointer"
                                                                                    },
                                                                                    onClick: (e)=>setRefundModal(!refundModal)
                                                                                    ,
                                                                                    children: [
                                                                                        "Refund Policy ",
                                                                                        /*#__PURE__*/ jsx_runtime.jsx(index_esm/* BsCheckCircle */.nRB, {
                                                                                        })
                                                                                    ]
                                                                                })
                                                                            })
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            className: "font-weight-normal border-0 p-1",
                                                                            children: "Certification:"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            className: "text-muted border-0 p-1",
                                                                            children: "CE / EU, CIQ, Eec, LFGB, Sgs"
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                                style: {
                                                    marginTop: "20px",
                                                    padding: "10px 20px"
                                                },
                                                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                                    md: "6",
                                                    style: {
                                                        fontSize: "14px"
                                                    },
                                                    children: /*#__PURE__*/ jsx_runtime.jsx("table", {
                                                        className: "table table-bordered",
                                                        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("tbody", {
                                                            children: [
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            children: "Qty"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: "12-48"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: "49-120"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: "120+"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            children: "Price"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: " IDR 2,000.00/ Piece"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: " IDR 1,950.00/ Piece"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: " IDR 1,900.00/ Piece"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                                            children: "Est. Lead Time"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: "4-7 days"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: "8-12 days"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                                            children: "12-15 days"
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                })
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* TabPane */.Jm, {
                                    tabId: 3,
                                    children: /*#__PURE__*/ jsx_runtime.jsx(ProductDetails_ReviewForm, {
                                    })
                                })
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Modal */.u_, {
                isOpen: refundModal,
                toggle: ()=>setRefundModal(!refundModal)
                ,
                // backdrop="static"
                keyboard: false,
                size: "sm",
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        style: {
                            position: "relative"
                        },
                        children: /*#__PURE__*/ jsx_runtime.jsx(fa_index_esm/* FaTimes */.aHS, {
                            style: {
                                position: "absolute",
                                right: "0",
                                margin: "10px",
                                cursor: "pointer"
                            },
                            className: "text-danger",
                            onClick: (e)=>setRefundModal(!refundModal)
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("br", {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("br", {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* ModalBody */.fe, {
                        children: /*#__PURE__*/ jsx_runtime.jsx("div", {
                            className: "table-responsive",
                            children: /*#__PURE__*/ jsx_runtime.jsx("span", {
                                style: {
                                    fontWeight: "600",
                                    color: "#000",
                                    marginTop: "20px"
                                },
                                children: "* If either dispatch date or product quality differs from what are specified in the online order, you can claim for a full or partial refund. 2-hour cancellation: For orders ≤ $500, you can get refund within 2 hours after you make a payment."
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const ProductDetails_ProductBottomTabs = (ProductBottomTabs);

// EXTERNAL MODULE: ./src/Components/ProductDetails/SliderGalary.module.css
var SliderGalary_module = __webpack_require__(861535);
var SliderGalary_module_default = /*#__PURE__*/__webpack_require__.n(SliderGalary_module);
// EXTERNAL MODULE: ./node_modules/react-icons/io/index.esm.js
var io_index_esm = __webpack_require__(651649);
;// CONCATENATED MODULE: ./src/Components/ProductDetails/SliderGalary3.js







const SliderGalary = ({ ProductData  })=>{
    const { 0: currentImage , 1: setCurrentImage  } = (0,react.useState)(0);
    const { 0: spliceIndex , 1: setSpliceIndex  } = (0,react.useState)(0);
    return(/*#__PURE__*/ jsx_runtime.jsx(jsx_runtime.Fragment, {
        children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Container */.W2, {
            className: "p-0",
            children: /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (SliderGalary_module_default()).container,
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                    style: {
                        margin: "0",
                        padding: "0"
                    },
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Col */.JX, {
                            lg: "2",
                            md: "2",
                            sm: "2",
                            xs: "2",
                            style: {
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "space-between",
                                padding: "0"
                            },
                            className: (SliderGalary_module_default()).ProductImgContainer,
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx("div", {
                                    style: {
                                        backgroundColor: "lightgray",
                                        opacity: "0.5"
                                    },
                                    className: "text-center mb-1",
                                    children: /*#__PURE__*/ jsx_runtime.jsx(io_index_esm/* IoIosArrowUp */.Vmf, {
                                        style: {
                                            fontSize: "25px"
                                        }
                                    })
                                }),
                                ProductData === null || ProductData === void 0 ? void 0 : ProductData.split(",").splice(spliceIndex, spliceIndex + 4).map((image, index)=>/*#__PURE__*/ jsx_runtime.jsx("div", {
                                        className: "mb-1",
                                        children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                            src: image,
                                            style: {
                                                width: "100%",
                                                height: "auto",
                                                cursor: "pointer",
                                                transition: "transform .2s"
                                            },
                                            onClick: (e)=>setCurrentImage(index)
                                            ,
                                            onMouseOver: (e)=>setCurrentImage(index)
                                            ,
                                            className: currentImage == index && "opacity-50"
                                        }, index)
                                    }, index)
                                ),
                                /*#__PURE__*/ jsx_runtime.jsx("div", {
                                    style: {
                                        backgroundColor: "lightgray",
                                        opacity: "0.5"
                                    },
                                    className: "text-center",
                                    children: /*#__PURE__*/ jsx_runtime.jsx(io_index_esm/* IoIosArrowDown */.OId, {
                                        style: {
                                            fontSize: "25px"
                                        }
                                    })
                                }),
                                (ProductData === null || ProductData === void 0 ? void 0 : ProductData.split(",").length) == 1 && /*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                                            className: "mb-1",
                                            children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                                src: "/Images/Transparent.png",
                                                style: {
                                                    width: "100%",
                                                    height: "auto",
                                                    cursor: "pointer",
                                                    transition: "transform .2s"
                                                }
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                                            className: "mb-1",
                                            children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                                src: "/Images/Transparent.png",
                                                style: {
                                                    width: "100%",
                                                    height: "auto",
                                                    cursor: "pointer",
                                                    transition: "transform .2s"
                                                }
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                                            className: "mb-1",
                                            children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                                src: "/Images/Transparent.png",
                                                style: {
                                                    width: "100%",
                                                    height: "auto",
                                                    cursor: "pointer",
                                                    transition: "transform .2s"
                                                }
                                            })
                                        })
                                    ]
                                }),
                                (ProductData === null || ProductData === void 0 ? void 0 : ProductData.split(",").length) == 2 && /*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                                            className: "mb-1",
                                            children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                                src: "/Images/Transparent.png",
                                                style: {
                                                    width: "100%",
                                                    height: "auto",
                                                    cursor: "pointer",
                                                    transition: "transform .2s"
                                                }
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                                            className: "mb-1",
                                            children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                                src: "/Images/Transparent.png",
                                                style: {
                                                    width: "100%",
                                                    height: "auto",
                                                    cursor: "pointer",
                                                    transition: "transform .2s"
                                                }
                                            })
                                        })
                                    ]
                                }),
                                (ProductData === null || ProductData === void 0 ? void 0 : ProductData.split(",").length) == 3 && /*#__PURE__*/ jsx_runtime.jsx(jsx_runtime.Fragment, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx("div", {
                                        className: "mb-1",
                                        children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                            src: "/Images/Transparent.png",
                                            style: {
                                                width: "100%",
                                                height: "auto",
                                                cursor: "pointer",
                                                transition: "transform .2s"
                                            }
                                        })
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                            lg: "10",
                            md: "10",
                            sm: "10",
                            xs: "10",
                            children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                style: {
                                    width: "100%",
                                    height: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    position: "relative"
                                },
                                className: (SliderGalary_module_default()).ProductImage,
                                children: [
                                    /*#__PURE__*/ jsx_runtime.jsx("img", {
                                        src: ProductData === null || ProductData === void 0 ? void 0 : ProductData.split(",")[currentImage],
                                        style: {
                                            width: "100%",
                                            height: "100%",
                                            objectFit: "cover"
                                        }
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                                        style: {
                                            position: "absolute",
                                            height: "70px",
                                            width: "70px",
                                            top: "0",
                                            right: "0",
                                            fontSize: "30px",
                                            backgroundColor: "#54622B",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            display: "flex"
                                        },
                                        children: /*#__PURE__*/ jsx_runtime.jsx(fa_index_esm/* FaHeart */.$0H, {
                                            style: {
                                                color: "#ffffff"
                                            }
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                                        style: {
                                            position: "absolute",
                                            height: "70px",
                                            width: "70px",
                                            bottom: "0",
                                            right: "0",
                                            fontSize: "30px",
                                            backgroundColor: "lightgray",
                                            opacity: "0.7",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            display: "flex"
                                        },
                                        children: /*#__PURE__*/ jsx_runtime.jsx(fa_index_esm/* FaSearch */.U41, {
                                            style: {
                                                color: "#ffffff"
                                            }
                                        })
                                    })
                                ]
                            })
                        })
                    ]
                })
            })
        })
    }));
};
/* harmony default export */ const SliderGalary3 = (SliderGalary);

// EXTERNAL MODULE: ./src/Components/Index/CategoryWiseProduct.js
var CategoryWiseProduct = __webpack_require__(617824);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(425675);
// EXTERNAL MODULE: ./src/Components/ProductDetails/Meet.module.css
var Meet_module = __webpack_require__(936605);
var Meet_module_default = /*#__PURE__*/__webpack_require__.n(Meet_module);
;// CONCATENATED MODULE: ./src/Components/ProductDetails/MeetTheMakers.js





const MeetTheMakers = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
        style: {
            padding: "0",
            margin: "0"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                lg: "7",
                md: "7",
                sm: "12",
                xs: "12",
                style: {
                    padding: "0"
                },
                children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                    src: "/Images/4.png",
                    style: {
                        width: "100%"
                    }
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                lg: "5",
                md: "5",
                sm: "12",
                xs: "12",
                style: {
                    backgroundColor: "rgb(200 162 124);",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                },
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                    className: (Meet_module_default()).InfoContainer,
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Meet_module_default()).image1Heading,
                            children: "MEET THE MAKERS"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("br", {
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Meet_module_default()).image1Title,
                            children: "THE BALI CURATOR"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("br", {
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Meet_module_default()).image1Desc,
                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut PLAY aliquip ex ea commodo consequat."
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const ProductDetails_MeetTheMakers = (MeetTheMakers);

// EXTERNAL MODULE: ./src/Components/ProductDetails/Index.module.css
var Index_module = __webpack_require__(70923);
var Index_module_default = /*#__PURE__*/__webpack_require__.n(Index_module);
;// CONCATENATED MODULE: ./src/Components/ProductDetails/Index.js









// import ProductBottomProducts from "../components/ProductBottomProducts";
// import SwiperGallery from "../components/SwiperGallery";





const Index = ({ ProductData , ProductAllData , ProductCategoryData  })=>{
    ProductData = ProductData[0];
    const { 0: open , 1: setOpen  } = (0,react.useState)(false);
    const { 0: showMore , 1: setShowMore  } = (0,react.useState)(false);
    const DvScrollbar = (0,react.useRef)(null);
    (0,react.useEffect)(()=>{
        if (showMore == true) {
            DvScrollbar.current.scrollTo(0, 0);
        }
    }, [
        showMore
    ]);
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("section", {
                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Container */.W2, {
                    className: "pt-4",
                    children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                lg: "6",
                                className: "order-1 order-lg-1",
                                children: /*#__PURE__*/ jsx_runtime.jsx(SliderGalary3, {
                                    ProductData: ProductData.mainimagepath
                                })
                            }),
                            /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Col */.JX, {
                                lg: "6",
                                className: "pl-lg-4 order-2 order-lg-2 ",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                        clsssName: (Index_module_default()).Header,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                className: "d-flex",
                                                style: {
                                                    justifyContent: "space-between"
                                                },
                                                children: /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                    style: {
                                                        color: "#666666",
                                                        fontSize: "15px"
                                                    },
                                                    children: ProductData.creator ? ProductData.creator : "The Bali Curator"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                className: "mb-0",
                                                style: {
                                                    textTransform: "uppercase",
                                                    fontWeight: "600",
                                                    fontSize: "20px"
                                                },
                                                children: ProductData.productname
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                                className: "d-flex mb-3",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime.jsx((Rating_default()), {
                                                        name: "half-rating",
                                                        contentEditable: false,
                                                        defaultValue: 2.5,
                                                        precision: 4,
                                                        style: {
                                                            color: "#e88f48"
                                                        }
                                                    }),
                                                    "\xa0\xa0(25)"
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                className: "d-flex flex-row flex-sm-row align-items-sm-center justify-content-sm-between",
                                                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("ul", {
                                                    className: "list-inline mb-2 mb-sm-0",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime.jsx("li", {
                                                            className: "list-inline-item h4 font-weight-light mb-0",
                                                            style: {
                                                                color: "#000"
                                                            },
                                                            children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("span", {
                                                                style: {
                                                                    color: "#000",
                                                                    fontSize: "18px",
                                                                    fontWeight: "400"
                                                                },
                                                                children: [
                                                                    "IDR",
                                                                    " ",
                                                                    ProductData.rate.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                                                                    " ",
                                                                    /*#__PURE__*/ jsx_runtime.jsx(next_link["default"], {
                                                                        href: "#",
                                                                        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("span", {
                                                                            onClick: (e)=>setOpen(true)
                                                                            ,
                                                                            style: {
                                                                                color: "#000",
                                                                                fontSize: "18px",
                                                                                fontWeight: "400",
                                                                                cursor: "pointer",
                                                                                textDecoration: "underline"
                                                                            },
                                                                            children: [
                                                                                "(Min.Order ",
                                                                                ProductData.moq,
                                                                                ")"
                                                                            ]
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime.jsx("li", {
                                                            className: "list-inline-item h4 font-weight-light mb-0",
                                                            style: {
                                                                color: "#000"
                                                            }
                                                        })
                                                    ]
                                                })
                                            }),
                                            ProductData.msrp && /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                style: {
                                                    marginBottom: "40px"
                                                },
                                                children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                                    children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("span", {
                                                        style: {
                                                            color: "#000",
                                                            fontSize: "14px",
                                                            fontWeight: "400",
                                                            opacity: "0.8"
                                                        },
                                                        children: [
                                                            "Retail Price : IDR",
                                                            " ",
                                                            ProductData.msrp.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                                        ]
                                                    })
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                                        className: (Index_module_default()).Content + " " + (showMore && (Index_module_default()).ScrollBar),
                                        ref: DvScrollbar,
                                        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Form */.l0, {
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                                                    children: [
                                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Col */.JX, {
                                                            sm: "3",
                                                            lg: "3",
                                                            className: "detail-option mb-2",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                                    className: "detail-option-heading",
                                                                    style: {
                                                                        color: "#000",
                                                                        fontSize: "15px",
                                                                        fontWeight: "200"
                                                                    },
                                                                    children: "Order Qty"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                                    style: {
                                                                        display: "flex",
                                                                        maxHeight: "min-content",
                                                                        border: "1px solid #000",
                                                                        alignItems: "center",
                                                                        height: "33px"
                                                                    },
                                                                    children: /*#__PURE__*/ jsx_runtime.jsx("input", {
                                                                        type: "number",
                                                                        placeholder: "5",
                                                                        style: {
                                                                            width: "100%",
                                                                            border: "none !important",
                                                                            textDecoration: "none",
                                                                            outline: "none !important",
                                                                            padding: "2px 5px",
                                                                            color: "#000",
                                                                            height: "100%"
                                                                        }
                                                                    })
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Col */.JX, {
                                                            sm: "6",
                                                            lg: "5",
                                                            className: "detail-option mb-2",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                                    className: "detail-option-heading",
                                                                    style: {
                                                                        color: "#000",
                                                                        fontSize: "15px",
                                                                        fontWeight: "200"
                                                                    },
                                                                    children: "Style"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                                    style: {
                                                                        display: "flex",
                                                                        maxHeight: "min-content",
                                                                        border: "1px solid #000",
                                                                        alignItems: "center",
                                                                        height: "33px"
                                                                    },
                                                                    children: /*#__PURE__*/ jsx_runtime.jsx("select", {
                                                                        style: {
                                                                            width: "100%",
                                                                            border: "none",
                                                                            textDecoration: "none",
                                                                            outline: "none",
                                                                            padding: "2px 5px",
                                                                            color: "#666666",
                                                                            height: "100%"
                                                                        },
                                                                        children: dummyproduct_namespaceObject.types.map((item, index)=>/*#__PURE__*/ jsx_runtime.jsx("option", {
                                                                                children: item.label
                                                                            }, index)
                                                                        )
                                                                    })
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                                    children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                                        lg: "12",
                                                        sm: "12",
                                                        children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("span", {
                                                            style: {
                                                                fontSize: "15px"
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime.jsx("b", {
                                                                    children: "Samples: "
                                                                }),
                                                                "IDR 40,000.00/Piece | 1 Piece (Min Order)",
                                                                " "
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("br", {
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                                    children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Col */.JX, {
                                                        sm: "12",
                                                        lg: "8",
                                                        className: "d-flex mb-2",
                                                        style: {
                                                            justifyContent: "space-between"
                                                        },
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                                style: {
                                                                    backgroundColor: "#c8a27c",
                                                                    borderRadius: "10px",
                                                                    color: "#ffffff",
                                                                    display: "flex",
                                                                    alignItems: "center",
                                                                    justifyContent: "center",
                                                                    height: "35px",
                                                                    cursor: "pointer"
                                                                },
                                                                children: /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                                    style: {
                                                                        fontSize: "14px",
                                                                        fontWeight: "500"
                                                                    },
                                                                    className: "px-5",
                                                                    children: "Add to Cart"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime.jsx("div", {
                                                                style: {
                                                                    backgroundColor: "#000",
                                                                    borderRadius: "10px",
                                                                    color: "#ffffff",
                                                                    display: "flex",
                                                                    alignItems: "center",
                                                                    justifyContent: "center",
                                                                    height: "35px",
                                                                    cursor: "pointer"
                                                                },
                                                                children: /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                                    style: {
                                                                        fontSize: "14px",
                                                                        fontWeight: "500"
                                                                    },
                                                                    className: "px-4",
                                                                    children: "Get Custom Quote"
                                                                })
                                                            })
                                                        ]
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Row */.X2, {
                                                    children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                                                        sm: "12",
                                                        lg: "10",
                                                        children: !showMore ? /*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime.jsx("p", {
                                                                    className: "mb-4 " + (Index_module_default()).ParagraphEffect,
                                                                    style: {
                                                                        fontWeight: "450",
                                                                        fontSize: "15px"
                                                                    },
                                                                    children: ProductData.productdesc
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                                    children: /*#__PURE__*/ jsx_runtime.jsx("span", {
                                                                        style: {
                                                                            cursor: "pointer",
                                                                            color: "rgb(83 83 83)",
                                                                            fontWeight: "550"
                                                                        },
                                                                        onClick: (e)=>setShowMore(!showMore)
                                                                        ,
                                                                        children: "Show More"
                                                                    })
                                                                })
                                                            ]
                                                        }) : /*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime.jsx("p", {
                                                                    className: "mb-4",
                                                                    style: {
                                                                        color: "#000",
                                                                        fontWeight: "450",
                                                                        fontSize: "15px"
                                                                    },
                                                                    children: ProductData.productdesc
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime.jsx("br", {
                                                                })
                                                            ]
                                                        })
                                                    })
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                })
            }),
            showMore && /*#__PURE__*/ jsx_runtime.jsx("section", {
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Container */.W2, {
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx(ProductDetails_ProductBottomTabs, {
                            product: dummyproduct_namespaceObject,
                            ProductData: ProductData
                        }),
                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                            className: "d-flex",
                            style: {
                                justifyContent: "space-between"
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx("div", {
                                    children: "\xa0"
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx("span", {
                                    children: /*#__PURE__*/ jsx_runtime.jsx("span", {
                                        style: {
                                            cursor: "pointer",
                                            color: "rgb(83 83 83)",
                                            fontWeight: "550"
                                        },
                                        onClick: (e)=>setShowMore(!showMore)
                                        ,
                                        children: "Show Less"
                                    })
                                })
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: "mt-5",
                children: /*#__PURE__*/ jsx_runtime.jsx(ProductDetails_MeetTheMakers, {
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(CategoryWiseProduct/* default */.Z, {
                CategoryName: "More from this maker",
                Size: 15,
                ProductCategoryData: ProductCategoryData
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Modal */.u_, {
                isOpen: open,
                toggle: ()=>setOpen(false)
                ,
                // backdrop="static"
                keyboard: false,
                size: "md",
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        style: {
                            position: "relative"
                        },
                        children: /*#__PURE__*/ jsx_runtime.jsx(fa_index_esm/* FaTimes */.aHS, {
                            style: {
                                position: "absolute",
                                right: "0",
                                margin: "10px",
                                cursor: "pointer",
                                color: "#e88f48",
                                fontSize: "20px"
                            },
                            onClick: (e)=>setOpen(false)
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("br", {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* ModalBody */.fe, {
                        children: /*#__PURE__*/ jsx_runtime.jsx("div", {
                            className: "table-responsive",
                            style: {
                                fontSize: "16px",
                                Color: "#666666",
                                opacity: "0.8"
                            },
                            children: /*#__PURE__*/ jsx_runtime.jsx("table", {
                                className: "table",
                                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("tbody", {
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                    style: {
                                                        border: "none"
                                                    },
                                                    children: "Qty"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                    style: {
                                                        border: "none"
                                                    },
                                                    children: "Price p.p"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("th", {
                                                    style: {
                                                        border: "none"
                                                    },
                                                    children: "Est. Lead Time"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: "12-48"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: " IDR 1,950.00"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: "4-7 days"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: "49-120"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: " IDR 2,000.00"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: "8-12 days"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: "120+"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: " IDR 1,900.00"
                                                }),
                                                /*#__PURE__*/ jsx_runtime.jsx("td", {
                                                    children: "12-15 days"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const ProductDetails_Index = (Index);

// EXTERNAL MODULE: ./src/Components/Index/MainHeader.js
var MainHeader = __webpack_require__(403279);
// EXTERNAL MODULE: ./src/Components/Header/Header.js + 1 modules
var Header = __webpack_require__(886248);
// EXTERNAL MODULE: ./src/Components/Index/BehindTheScenes.js
var BehindTheScenes = __webpack_require__(124870);
// EXTERNAL MODULE: ./src/Components/Index/Footer.js
var Footer = __webpack_require__(744332);
;// CONCATENATED MODULE: ./src/pages/ProductDetail/[id].js








const ProductDetails = ({ ProductData , ProductAllData , ProductCategoryData ,  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("meta", {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            }),
            /*#__PURE__*/ jsx_runtime.jsx(MainHeader/* default */.Z, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Header/* default */.Z, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(ProductDetails_Index, {
                ProductData: ProductData.input[0],
                ProductAllData: ProductAllData,
                ProductCategoryData: ProductCategoryData === null || ProductCategoryData === void 0 ? void 0 : ProductCategoryData.input[0]
            }),
            /*#__PURE__*/ jsx_runtime.jsx(CategoryWiseProduct/* default */.Z, {
                ProductCategoryData: ProductCategoryData === null || ProductCategoryData === void 0 ? void 0 : ProductCategoryData.input[0]
            }),
            /*#__PURE__*/ jsx_runtime.jsx(BehindTheScenes/* default */.Z, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Footer/* default */.Z, {
            })
        ]
    }));
};
async function getServerSideProps({ req , res , params  }) {
    let productid = params.id;
    // console.log(params)
    const url = "https://hzlvonfjd2.execute-api.ap-south-1.amazonaws.com/dev/";
    const productData = await fetch(url + "getsingleproduct", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            productid: productid
        })
    });
    const ProductData = await productData.json();
    const productAllRes = await fetch(url + "getproduct");
    const ProductAllData = await productAllRes.json();
    let CategoryName = 7;
    const productCategoryData = await fetch(url + "getproductbycategory", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            categoryid: CategoryName
        })
    });
    const ProductCategoryData = await productCategoryData.json();
    return {
        props: {
            ProductData,
            ProductAllData,
            ProductCategoryData
        }
    };
}
/* harmony default export */ const _id_ = (ProductDetails);


/***/ }),

/***/ 501014:
/***/ ((module) => {

"use strict";
module.exports = require("critters");

/***/ }),

/***/ 702186:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/@ampproject/toolbox-optimizer");

/***/ }),

/***/ 714300:
/***/ ((module) => {

"use strict";
module.exports = require("buffer");

/***/ }),

/***/ 706113:
/***/ ((module) => {

"use strict";
module.exports = require("crypto");

/***/ }),

/***/ 582361:
/***/ ((module) => {

"use strict";
module.exports = require("events");

/***/ }),

/***/ 657147:
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),

/***/ 113685:
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),

/***/ 795687:
/***/ ((module) => {

"use strict";
module.exports = require("https");

/***/ }),

/***/ 822037:
/***/ ((module) => {

"use strict";
module.exports = require("os");

/***/ }),

/***/ 371017:
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ }),

/***/ 863477:
/***/ ((module) => {

"use strict";
module.exports = require("querystring");

/***/ }),

/***/ 12781:
/***/ ((module) => {

"use strict";
module.exports = require("stream");

/***/ }),

/***/ 371576:
/***/ ((module) => {

"use strict";
module.exports = require("string_decoder");

/***/ }),

/***/ 257310:
/***/ ((module) => {

"use strict";
module.exports = require("url");

/***/ }),

/***/ 473837:
/***/ ((module) => {

"use strict";
module.exports = require("util");

/***/ }),

/***/ 959796:
/***/ ((module) => {

"use strict";
module.exports = require("zlib");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [244,765,649,447,638,870], () => (__webpack_exec__(886769)));
module.exports = __webpack_exports__;

})();