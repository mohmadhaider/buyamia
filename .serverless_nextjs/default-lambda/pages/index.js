(() => {
var exports = {};
exports.id = 405;
exports.ids = [405];
exports.modules = {

/***/ 693197:
/***/ ((module) => {

// Exports
module.exports = {
	"box": "Category_box__WCMSD",
	"title": "Category_title__OdZOJ",
	"iconWrap": "Category_iconWrap__GrNLi",
	"CategoryContainer": "Category_CategoryContainer___bdNv",
	"prev": "Category_prev__cpPFj",
	"next": "Category_next__qrljG"
};


/***/ }),

/***/ 752379:
/***/ ((module) => {

// Exports
module.exports = {
	"title": "CategoryContainer_title__Hynfz",
	"mainTitle": "CategoryContainer_mainTitle__FpK1P"
};


/***/ }),

/***/ 708930:
/***/ ((module) => {

// Exports
module.exports = {
	"container": "DetailsComponent_container__gEt7p",
	"iconWrap": "DetailsComponent_iconWrap__Ok9YA",
	"titleWrap": "DetailsComponent_titleWrap__vH1Sb",
	"summeryWrap": "DetailsComponent_summeryWrap__IbdDy"
};


/***/ }),

/***/ 350051:
/***/ ((module) => {

// Exports
module.exports = {
	"box": "DropshipHotlist_box__HUgKp",
	"title": "DropshipHotlist_title__3d1E1",
	"iconWrap": "DropshipHotlist_iconWrap__xLpwj",
	"productContainer": "DropshipHotlist_productContainer__2J0Kb",
	"prev": "DropshipHotlist_prev__RjVjD",
	"next": "DropshipHotlist_next__Up3Ht"
};


/***/ }),

/***/ 579810:
/***/ ((module) => {

// Exports
module.exports = {
	"slideshow_container": "MainSlider_slideshow_container__NOu0T",
	"InfoContainer": "MainSlider_InfoContainer__bvBMi",
	"InfoTitle": "MainSlider_InfoTitle__TQ1xc",
	"InfoHeading": "MainSlider_InfoHeading__ZpjiK",
	"fade": "MainSlider_fade__BD0f9",
	"InfoDescription": "MainSlider_InfoDescription__292jG",
	"prev": "MainSlider_prev__TJ8uA",
	"next": "MainSlider_next__n3_zu",
	"text": "MainSlider_text__fTRg8",
	"numbertext": "MainSlider_numbertext__XzUbg",
	"dot": "MainSlider_dot__Hmpy8",
	"active": "MainSlider_active__JfdYU"
};


/***/ }),

/***/ 869473:
/***/ ((module) => {

// Exports
module.exports = {
	"InfoContainer": "Meet_InfoContainer__lRVHp",
	"image1Heading": "Meet_image1Heading__nVicp",
	"image1Title": "Meet_image1Title__C7tla",
	"image1Desc": "Meet_image1Desc__QYO53",
	"shopnowbtn": "Meet_shopnowbtn__DxeCf"
};


/***/ }),

/***/ 722776:
/***/ ((module) => {

// Exports
module.exports = {
	"shopnowbtn": "imageshome_shopnowbtn__g_QMx",
	"shopnowbtn2": "imageshome_shopnowbtn2__AFHGv",
	"category": "imageshome_category__94Nza",
	"uppertext": "imageshome_uppertext__Q_NiR",
	"categorytitle": "imageshome_categorytitle__AH87V",
	"smallfont": "imageshome_smallfont__hvWBn",
	"img2Title": "imageshome_img2Title__8HwIY",
	"img3Title": "imageshome_img3Title__9vjWd",
	"img2Desc": "imageshome_img2Desc__N6qsE",
	"img2Container": "imageshome_img2Container__5X_F4",
	"mobileWrap": "imageshome_mobileWrap__oqM0j",
	"imageWidth": "imageshome_imageWidth___zOH9"
};


/***/ }),

/***/ 227970:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps),
/* harmony export */   "getStaticPaths": () => (/* binding */ getStaticPaths),
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "unstable_getStaticParams": () => (/* binding */ unstable_getStaticParams),
/* harmony export */   "unstable_getStaticProps": () => (/* binding */ unstable_getStaticProps),
/* harmony export */   "unstable_getStaticPaths": () => (/* binding */ unstable_getStaticPaths),
/* harmony export */   "unstable_getServerProps": () => (/* binding */ unstable_getServerProps),
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "_app": () => (/* binding */ _app),
/* harmony export */   "renderReqToHTML": () => (/* binding */ renderReqToHTML),
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(170607);
/* harmony import */ var next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_node_polyfill_fetch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(659450);
/* harmony import */ var private_dot_next_build_manifest_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(297020);
/* harmony import */ var private_dot_next_react_loadable_manifest_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(973978);
/* harmony import */ var next_dist_build_webpack_loaders_next_serverless_loader_page_handler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(999436);

      
      
      
      

      
      const { processEnv } = __webpack_require__(972333)
      processEnv([])
    
      
      const runtimeConfig = {}
      ;

      const documentModule = __webpack_require__(423105)

      const appMod = __webpack_require__(952654)
      let App = appMod.default || appMod.then && appMod.then(mod => mod.default);

      const compMod = __webpack_require__(66881)

      const Component = compMod.default || compMod.then && compMod.then(mod => mod.default)
      /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Component);
      const getStaticProps = compMod['getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['getStaticProp' + 's'])
      const getStaticPaths = compMod['getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['getStaticPath' + 's'])
      const getServerSideProps = compMod['getServerSideProp' + 's'] || compMod.then && compMod.then(mod => mod['getServerSideProp' + 's'])

      // kept for detecting legacy exports
      const unstable_getStaticParams = compMod['unstable_getStaticParam' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticParam' + 's'])
      const unstable_getStaticProps = compMod['unstable_getStaticProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticProp' + 's'])
      const unstable_getStaticPaths = compMod['unstable_getStaticPath' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getStaticPath' + 's'])
      const unstable_getServerProps = compMod['unstable_getServerProp' + 's'] || compMod.then && compMod.then(mod => mod['unstable_getServerProp' + 's'])

      let config = compMod['confi' + 'g'] || (compMod.then && compMod.then(mod => mod['confi' + 'g'])) || {}
      const _app = App

      const combinedRewrites = Array.isArray(private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg)
        ? private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg
        : []

      if (!Array.isArray(private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites */ .Dg)) {
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.beforeFiles */ .Dg.beforeFiles)
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.afterFiles */ .Dg.afterFiles)
        combinedRewrites.push(...private_dot_next_routes_manifest_json__WEBPACK_IMPORTED_MODULE_1__/* .rewrites.fallback */ .Dg.fallback)
      }

      const { renderReqToHTML, render } = (0,next_dist_build_webpack_loaders_next_serverless_loader_page_handler__WEBPACK_IMPORTED_MODULE_4__/* .getPageHandler */ .u)({
        pageModule: compMod,
        pageComponent: Component,
        pageConfig: config,
        appModule: App,
        documentModule: documentModule,
        errorModule: __webpack_require__(89185),
        notFoundModule: undefined,
        pageGetStaticProps: getStaticProps,
        pageGetStaticPaths: getStaticPaths,
        pageGetServerSideProps: getServerSideProps,

        assetPrefix: "",
        canonicalBase: "",
        generateEtags: true,
        poweredByHeader: true,

        runtimeConfig,
        buildManifest: private_dot_next_build_manifest_json__WEBPACK_IMPORTED_MODULE_2__,
        reactLoadableManifest: private_dot_next_react_loadable_manifest_json__WEBPACK_IMPORTED_MODULE_3__,

        rewrites: combinedRewrites,
        i18n: undefined,
        page: "/",
        buildId: "-8BASkNRMpFB4EzFQPodJ",
        escapedBuildId: "\-8BASkNRMpFB4EzFQPodJ",
        basePath: "",
        pageIsDynamic: false,
        encodedPreviewProps: {previewModeId:"5749ed080bc6d9c7ebcecb7513493b79",previewModeSigningKey:"2266d3b39b76a0a30863e0a788a8d78fe2340f025f300dba6629a72fb4b2bcc3",previewModeEncryptionKey:"13591eaeda5d8bda3e73917bc1b739f4177e980b7a755916babce9e7144859db"}
      })
      
    

/***/ }),

/***/ 66881:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ Home),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: ./node_modules/react/jsx-runtime.js
var jsx_runtime = __webpack_require__(785893);
// EXTERNAL MODULE: ./src/Components/Index/MainHeader.js
var MainHeader = __webpack_require__(403279);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(667294);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(425675);
// EXTERNAL MODULE: ./src/Components/Index/Meet.module.css
var Meet_module = __webpack_require__(869473);
var Meet_module_default = /*#__PURE__*/__webpack_require__.n(Meet_module);
// EXTERNAL MODULE: ./node_modules/reactstrap/dist/reactstrap.modern.js
var reactstrap_modern = __webpack_require__(250450);
;// CONCATENATED MODULE: ./src/Components/Index/MeetTheMakers.js





const MeetTheMakers = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
        style: {
            padding: "0",
            margin: "0"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                lg: "7",
                md: "7",
                sm: "12",
                xs: "12",
                style: {
                    padding: "0"
                },
                children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                    src: "/Images/4.png",
                    style: {
                        width: "100%"
                    }
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                lg: "5",
                md: "5",
                sm: "12",
                xs: "12",
                style: {
                    backgroundColor: "#FEF6EC",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                },
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                    className: (Meet_module_default()).InfoContainer,
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Meet_module_default()).image1Heading,
                            children: "THE BALI CURATOR"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("br", {
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Meet_module_default()).image1Title,
                            children: "Meet The Makers"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("br", {
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("br", {
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                            className: (Meet_module_default()).image1Desc,
                            children: "A success story about what makes us great, the makers and their products, backgrounds and how they are there for you."
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                            className: (Meet_module_default()).image1Desc,
                            children: "A success story about what makes us great, the makers and their products, backgrounds and how they are there for you."
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                            className: (Meet_module_default()).image1Desc,
                            children: "A success story about what makes us great, the makers and their products, backgrounds and how they are there for you."
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("button", {
                            className: (Meet_module_default()).shopnowbtn,
                            children: "Learn More"
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const Index_MeetTheMakers = (MeetTheMakers);

// EXTERNAL MODULE: ./node_modules/prop-types/index.js
var prop_types = __webpack_require__(45697);
var prop_types_default = /*#__PURE__*/__webpack_require__.n(prop_types);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/Box/index.js
var node_Box = __webpack_require__(609285);
// EXTERNAL MODULE: ./src/Components/Index/DetailsComponent.module.css
var DetailsComponent_module = __webpack_require__(708930);
var DetailsComponent_module_default = /*#__PURE__*/__webpack_require__.n(DetailsComponent_module);
;// CONCATENATED MODULE: ./src/Components/Index/DetailsComponent.js




function DetailsComponent({ icon , title , summery  }) {
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        className: (DetailsComponent_module_default()).container + " text-center",
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                    src: icon,
                    className: (DetailsComponent_module_default()).iconWrap
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (DetailsComponent_module_default()).titleWrap,
                children: title
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (DetailsComponent_module_default()).summeryWrap,
                children: summery
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./src/Components/Index/Details.js






function Item(props) {
    const { sx , ...other } = props;
    return(/*#__PURE__*/ jsx_runtime.jsx(node_Box["default"], {
        sx: {
            // bgcolor: "primary.main",
            color: "white",
            p: 1,
            m: 1,
            borderRadius: 1,
            textAlign: "center",
            fontSize: "1rem",
            // height: 220,
            // width: 250,
            // marginRight: 10,
            fontWeight: "700",
            ...sx
        },
        ...other
    }));
}
Item.propTypes = {
    sx: prop_types_default().oneOfType([
        prop_types_default().arrayOf(prop_types_default().oneOfType([
            (prop_types_default()).func,
            (prop_types_default()).object,
            (prop_types_default()).bool
        ])),
        (prop_types_default()).func,
        (prop_types_default()).object, 
    ])
};
function Details() {
    return(/*#__PURE__*/ jsx_runtime.jsx("div", {
        style: {
            padding: "60px 0px"
        },
        children: /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Container */.W2, {
            children: /*#__PURE__*/ (0,jsx_runtime.jsxs)(reactstrap_modern/* Row */.X2, {
                style: {
                    margin: "0",
                    padding: "0"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                        lg: "3",
                        md: "3",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ jsx_runtime.jsx(DetailsComponent, {
                            icon: "/Images/Details/wecommerce.svg",
                            title: "WE-COMMERCE",
                            summery: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh."
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                        lg: "3",
                        md: "3",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ jsx_runtime.jsx(DetailsComponent, {
                            icon: "/Images/Details/quality.svg",
                            title: "CURATED QUALITY",
                            summery: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet "
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                        lg: "3",
                        md: "3",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ jsx_runtime.jsx(DetailsComponent, {
                            icon: "/Images/Details/shopping.svg",
                            title: "SHIPPING AND QC",
                            summery: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam "
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(reactstrap_modern/* Col */.JX, {
                        lg: "3",
                        md: "3",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ jsx_runtime.jsx(DetailsComponent, {
                            icon: "/Images/Details/support.svg",
                            title: "24H SUPPORT",
                            summery: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet "
                        })
                    })
                ]
            })
        })
    }));
};

// EXTERNAL MODULE: ./src/Components/Index/DropshipHotlist.module.css
var DropshipHotlist_module = __webpack_require__(350051);
var DropshipHotlist_module_default = /*#__PURE__*/__webpack_require__.n(DropshipHotlist_module);
// EXTERNAL MODULE: ./src/Components/Index/ProductContainer.js
var Index_ProductContainer = __webpack_require__(242494);
// EXTERNAL MODULE: ./node_modules/react-multi-carousel/index.js
var react_multi_carousel = __webpack_require__(386529);
;// CONCATENATED MODULE: ./src/Components/Index/DropshipHotlist.js










const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: {
            max: 4000,
            min: 3000
        },
        items: 5
    },
    desktop: {
        breakpoint: {
            max: 3000,
            min: 1024
        },
        items: 3
    },
    tablet: {
        breakpoint: {
            max: 1024,
            min: 464
        },
        items: 2
    },
    mobile: {
        breakpoint: {
            max: 464,
            min: 0
        },
        items: 1
    }
};
function DropshipHotlist_Item(props) {
    const { sx , ...other } = props;
    return(/*#__PURE__*/ jsx_runtime.jsx(node_Box["default"], {
        sx: {
            // bgcolor: "primary.main",
            color: "#666666",
            p: 1,
            m: 1,
            borderRadius: 1,
            fontSize: "1rem",
            // height: 465,
            // width: 300,
            // marginRight: 10,
            fontWeight: "700",
            ...sx
        },
        ...other
    }));
}
DropshipHotlist_Item.propTypes = {
    sx: prop_types_default().oneOfType([
        prop_types_default().arrayOf(prop_types_default().oneOfType([
            (prop_types_default()).func,
            (prop_types_default()).object,
            (prop_types_default()).bool
        ])),
        (prop_types_default()).func,
        (prop_types_default()).object, 
    ])
};
function DropshipHotlist({ products =[]  }) {
    var ref;
    let DivRef = react.useRef(null);
    const [currentScrollPosition, setCurrentScrollPosition] = react.useState(0);
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        className: (DropshipHotlist_module_default()).box,
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (DropshipHotlist_module_default()).title,
                children: "Best Sellers"
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (DropshipHotlist_module_default()).iconWrap,
                children: /*#__PURE__*/ jsx_runtime.jsx("div", {
                    // src={"/Images/DropshipHotlist/Rectangle 115.png"}
                    style: {
                        width: 110,
                        height: 6,
                        backgroundColor: "#e88f48"
                    }
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (DropshipHotlist_module_default()).productContainer,
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                    style: {
                        display: "flex",
                        maxWidth: "98%",
                        flexDirection: "row",
                        // padding: "22px 0px 27px",
                        justifyContent: "space-between",
                        alignItems: "center",
                        justifyItems: "center",
                        position: "relative"
                    },
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (DropshipHotlist_module_default()).prev,
                            onClick: ()=>{
                                if (currentScrollPosition - 350 <= 0) {
                                    setCurrentScrollPosition(0);
                                } else {
                                    setCurrentScrollPosition(currentScrollPosition - 350);
                                }
                                DivRef.current.scrollTo(currentScrollPosition, currentScrollPosition - 350);
                            },
                            children: "❮"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("div", {
                            ref: DivRef,
                            style: {
                                display: "flex",
                                scrollBehavior: "smooth",
                                justifyContent: "space-between",
                                flexWrap: "nowrap",
                                padding: "5px 0px",
                                scrollbarWidth: "none",
                                scrollbarColor: "#fff",
                                overflow: "hidden"
                            },
                            children: (ref = products.slice(0, 10)) === null || ref === void 0 ? void 0 : ref.map((product, index)=>/*#__PURE__*/ jsx_runtime.jsx(DropshipHotlist_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(Index_ProductContainer/* default */.Z, {
                                        productid: product.productid,
                                        productImage: product.mainimagepath,
                                        title: product.productname,
                                        price: product.rate,
                                        summery: product.productdesc,
                                        rating: 4,
                                        ratingColor: "true",
                                        imageHeight: 230,
                                        hotseller: index === 2 ? true : false,
                                        minWidth: 232,
                                        minHeight: 400
                                    })
                                }, index)
                            )
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (DropshipHotlist_module_default()).next,
                            onClick: ()=>{
                                DivRef.current.scrollTo({
                                    left: currentScrollPosition
                                });
                                if (DivRef.current.scrollWidth - DivRef.current.offsetWidth - (currentScrollPosition + 350) > 350) {
                                    setCurrentScrollPosition(currentScrollPosition + 350);
                                } else {
                                    setCurrentScrollPosition(DivRef.current.scrollWidth - DivRef.current.offsetWidth);
                                }
                            },
                            children: "❯"
                        })
                    ]
                })
            })
        ]
    }));
};

// EXTERNAL MODULE: ./src/Components/Index/Category.module.css
var Category_module = __webpack_require__(693197);
var Category_module_default = /*#__PURE__*/__webpack_require__.n(Category_module);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/CardMedia/index.js
var CardMedia = __webpack_require__(205991);
var CardMedia_default = /*#__PURE__*/__webpack_require__.n(CardMedia);
// EXTERNAL MODULE: ./src/Components/Index/CategoryContainer.module.css
var CategoryContainer_module = __webpack_require__(752379);
var CategoryContainer_module_default = /*#__PURE__*/__webpack_require__.n(CategoryContainer_module);
;// CONCATENATED MODULE: ./src/Components/Index/CategoryContainer.js







function CategoryContainer({ categoryImage , title , colorCode , heightSize , widthSize ,  }) {
    // console.log(JSON.stringify(productImage) + "PRODUCT IAMEF");
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        style: {
            minWidth: 275,
            zIndex: 10
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                style: {
                    backgroundColor: colorCode
                },
                children: /*#__PURE__*/ jsx_runtime.jsx((CardMedia_default()), {
                    component: "img",
                    height: 270,
                    width: 187,
                    sx: {
                        objectFit: "contain",
                        padding: "15px",
                        cursor: "pointer",
                        transition: "all 0.3s ease-in-out",
                        zIndex: "-1",
                        overflow: "hidden",
                        "&:hover": {
                            "-ms-transform": "scale(1.1)",
                            "-webkit-transform": "scale(1.1)",
                            transform: "scale(1.1)"
                        }
                    },
                    src: categoryImage,
                    alt: "Category Image Not Found"
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                className: (CategoryContainer_module_default()).title,
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("lable", {
                        gutterBottom: true,
                        variant: "h5",
                        component: "div",
                        children: /*#__PURE__*/ jsx_runtime.jsx("center", {
                            children: title
                        })
                    }),
                    " "
                ]
            })
        ]
    }));
};

// EXTERNAL MODULE: ./node_modules/@material-ui/core/index.js
var core = __webpack_require__(958189);
;// CONCATENATED MODULE: ./src/Components/Index/Category.js








function Category_Item(props) {
    const { sx , ...other } = props;
    return(/*#__PURE__*/ jsx_runtime.jsx(core.Box, {
        sx: {
            // bgcolor: "primary.main",
            color: "#666666",
            // p: 1,
            // m: 1,
            border: "none",
            // margin: "0px 10px",
            fontSize: "1rem",
            // height: 465,
            width: 300,
            // margin: "0px 10px",
            fontWeight: "700",
            ...sx
        },
        ...other
    }));
}
Category_Item.propTypes = {
    sx: prop_types_default().oneOfType([
        prop_types_default().arrayOf(prop_types_default().oneOfType([
            (prop_types_default()).func,
            (prop_types_default()).object,
            (prop_types_default()).bool
        ])),
        (prop_types_default()).func,
        (prop_types_default()).object, 
    ])
};
function Category() {
    let DivRef = react.useRef(null);
    const [currentScrollPosition, setCurrentScrollPosition] = react.useState(0);
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        className: (Category_module_default()).box,
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (Category_module_default()).title,
                children: "Categories"
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (Category_module_default()).iconWrap,
                children: /*#__PURE__*/ jsx_runtime.jsx("div", {
                    // src={"/Images/DropshipHotlist/Rectangle 115.png"}
                    style: {
                        width: 110,
                        height: 6,
                        backgroundColor: "#e88f48"
                    }
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (Category_module_default()).productContainer,
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                    style: {
                        display: "flex",
                        flexDirection: "row",
                        // padding: "22px 0px 27px",
                        position: "relative",
                        // justifyContent: "stretch",
                        alignItems: "center",
                        justifyItems: "center",
                        minHeight: "max-content"
                    },
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Category_module_default()).prev,
                            onClick: ()=>{
                                if (currentScrollPosition - 300 <= 0) {
                                    setCurrentScrollPosition(0);
                                } else {
                                    setCurrentScrollPosition(currentScrollPosition - 300);
                                }
                                DivRef.current.scrollTo(currentScrollPosition, currentScrollPosition - 300);
                            },
                            children: "❮"
                        }),
                        /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                            ref: DivRef,
                            style: {
                                display: "flex",
                                scrollBehavior: "smooth",
                                alignItems: "center",
                                flexWrap: "nowrap",
                                padding: "20px 0px 5px",
                                scrollbarWidth: "none",
                                scrollbarColor: "#fff",
                                overflow: "hidden"
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime.jsx(Category_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(CategoryContainer, {
                                        categoryImage: "/Images/Category/Home.png",
                                        title: "Home & Lifestyle",
                                        // heightSize={270}
                                        // widthSize={187}
                                        // style={{}}
                                        colorCode: "#e2e6e3"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(Category_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(CategoryContainer, {
                                        categoryImage: "/Images/Category/Asset 1.png",
                                        title: "Beauty",
                                        heightSize: 162,
                                        colorCode: "#fef6ec",
                                        widthSize: 236
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(Category_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(CategoryContainer, {
                                        categoryImage: "/Images/Category/Asset 2.png",
                                        title: "Fashion",
                                        heightSize: 270,
                                        widthSize: 187,
                                        colorCode: "#e2e6e3"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(Category_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(CategoryContainer, {
                                        categoryImage: "/Images/Category/Food.png",
                                        title: "Food & Drinks",
                                        heightSize: 162,
                                        widthSize: 236,
                                        colorCode: "#f4f2f0"
                                    })
                                }),
                                " ",
                                /*#__PURE__*/ jsx_runtime.jsx(Category_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(CategoryContainer, {
                                        categoryImage: "/Images/Category/Raw.png",
                                        title: "Raw Materials",
                                        heightSize: 270,
                                        widthSize: 187,
                                        colorCode: "#e2e6e3"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime.jsx(Category_Item, {
                                    children: /*#__PURE__*/ jsx_runtime.jsx(CategoryContainer, {
                                        categoryImage: "/Images/Category/Art.png",
                                        title: "Art & Antique",
                                        heightSize: 162,
                                        widthSize: 236,
                                        colorCode: "#f4f2f0"
                                    })
                                }),
                                " "
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (Category_module_default()).next,
                            onClick: ()=>{
                                DivRef.current.scrollTo({
                                    left: currentScrollPosition
                                });
                                if (DivRef.current.scrollWidth - DivRef.current.offsetWidth - (currentScrollPosition + 300) > 300) {
                                    setCurrentScrollPosition(currentScrollPosition + 300);
                                } else {
                                    setCurrentScrollPosition(DivRef.current.scrollWidth - DivRef.current.offsetWidth);
                                }
                            },
                            children: "❯"
                        })
                    ]
                })
            })
        ]
    }));
};

// EXTERNAL MODULE: ./src/Components/Index/CategoryWiseProduct.js
var CategoryWiseProduct = __webpack_require__(617824);
;// CONCATENATED MODULE: ./src/Components/Index/CategoryWiseProduct1.js






function CategoryWiseProduct1_Item(props) {
    const { sx , ...other } = props;
    return(/*#__PURE__*/ jsx_runtime.jsx(node_Box["default"], {
        sx: {
            // bgcolor: "primary.main",
            color: "white",
            p: 1,
            m: 1,
            borderRadius: 1,
            textAlign: "center",
            fontSize: "1rem",
            // height: 150,
            // width: 100,
            // marginRight: 20,
            fontWeight: "700",
            ...sx
        },
        ...other
    }));
}
CategoryWiseProduct1_Item.propTypes = {
    sx: prop_types_default().oneOfType([
        prop_types_default().arrayOf(prop_types_default().oneOfType([
            (prop_types_default()).func,
            (prop_types_default()).object,
            (prop_types_default()).bool
        ])),
        (prop_types_default()).func,
        (prop_types_default()).object, 
    ])
};
function CategoryWiseProduct1({ ProductCategoryData2 =[] , ProductCategoryData3 =[] , CategoryName2 ="Art and Antique" ,  }) {
    var ref, ref1;
    return(/*#__PURE__*/ _jsx("div", {
        style: {
            width: "100%",
            marginTop: 10
        },
        children: /*#__PURE__*/ _jsxs(Box, {
            sx: {
                display: "flex",
                flexWrap: "wrap",
                // p: 1,
                // m: 1,
                bgcolor: "background.paper",
                //   maxWidth: 300,
                // alignItems: "center",
                justifyContent: "center"
            },
            children: [
                /*#__PURE__*/ _jsx("div", {
                    className: s.productContainer,
                    children: /*#__PURE__*/ _jsxs("div", {
                        className: s.box,
                        children: [
                            /*#__PURE__*/ _jsxs("div", {
                                className: s.title,
                                children: [
                                    CategoryName2,
                                    " "
                                ]
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: s.iconWrap,
                                children: /*#__PURE__*/ _jsx("img", {
                                    src: "/Images/DropshipHotlist/Rectangle 115.png",
                                    style: {
                                        width: 100
                                    },
                                    height: 8
                                })
                            }),
                            /*#__PURE__*/ _jsx(Box, {
                                sx: {
                                    display: "flex",
                                    //flexWrap: "wrap",
                                    overflow: "hidden",
                                    p: 1,
                                    m: 1,
                                    // width: "100%",
                                    bgcolor: "background.paper",
                                    //   maxWidth: 300,
                                    alignItems: "center",
                                    justifyContent: "center"
                                },
                                children: (ref = ProductCategoryData2.slice(0, 2)) === null || ref === void 0 ? void 0 : ref.map((product, index)=>/*#__PURE__*/ _jsx(CategoryWiseProduct1_Item, {
                                        children: /*#__PURE__*/ _jsx(ProductContainer, {
                                            productid: product.productid,
                                            productImage: product.mainimagepath,
                                            title: product.productname,
                                            price: product.rate,
                                            summery: product.productdesc,
                                            rating: 4,
                                            minWidth: 255
                                        })
                                    }, index)
                                )
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ _jsx("div", {
                    className: s.productContainer,
                    children: /*#__PURE__*/ _jsxs("div", {
                        className: s.box,
                        children: [
                            /*#__PURE__*/ _jsxs("div", {
                                className: s.title,
                                children: [
                                    CategoryName3,
                                    " "
                                ]
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: s.iconWrap,
                                children: /*#__PURE__*/ _jsx("img", {
                                    src: "/Images/DropshipHotlist/Rectangle 115.png",
                                    style: {
                                        width: 110,
                                        backgroundColor: "#e88f48"
                                    },
                                    height: 6
                                })
                            }),
                            /*#__PURE__*/ _jsx(Box, {
                                sx: {
                                    display: "flex",
                                    flexWrap: "wrap",
                                    p: 1,
                                    m: 1,
                                    width: "100%",
                                    bgcolor: "background.paper",
                                    //   maxWidth: 300,
                                    // alignItems: "center",
                                    justifyContent: "center"
                                },
                                children: (ref1 = ProductCategoryData3.slice(0, 2)) === null || ref1 === void 0 ? void 0 : ref1.map((product, index)=>/*#__PURE__*/ _jsx(CategoryWiseProduct1_Item, {
                                        children: /*#__PURE__*/ _jsx(ProductContainer, {
                                            productImage: product.mainimagepath,
                                            title: product.productname,
                                            productid: product.productid,
                                            price: product.rate,
                                            summery: product.productdesc,
                                            rating: 4,
                                            minWidth: 255
                                        })
                                    }, index)
                                )
                            })
                        ]
                    })
                })
            ]
        })
    }));
};

// EXTERNAL MODULE: ./src/Components/Index/BehindTheScenes.js
var BehindTheScenes = __webpack_require__(124870);
// EXTERNAL MODULE: ./src/Components/Index/imageshome.module.css
var imageshome_module = __webpack_require__(722776);
var imageshome_module_default = /*#__PURE__*/__webpack_require__.n(imageshome_module);
;// CONCATENATED MODULE: ./src/Components/Index/imageshome.js



const Imageshome = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        style: {
            width: "100%"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("meta", {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                className: (imageshome_module_default()).mobileWrap,
                style: {
                    display: "flex",
                    justifyContent: "space-evenly",
                    width: "100%",
                    marginTop: 47
                },
                children: [
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                        style: {
                            position: "relative",
                            width: "100%"
                        },
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx("img", {
                                style: {
                                    width: "100%",
                                    height: "100%"
                                },
                                src: "https://buyamiaimages.s3.ap-south-1.amazonaws.com/image+37.png"
                            }),
                            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                className: (imageshome_module_default()).category,
                                children: [
                                    /*#__PURE__*/ jsx_runtime.jsx("label", {
                                        className: (imageshome_module_default()).uppertext,
                                        children: "THE LATEST PRODUCTS IN"
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("label", {
                                        style: {
                                            lineHeight: 0.8
                                        },
                                        className: (imageshome_module_default()).categorytitle,
                                        children: "HOME & LIVING"
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("label", {
                                        className: (imageshome_module_default()).smallfont + " mt-3",
                                        style: {
                                            color: "#7a7070"
                                        },
                                        children: "A success story about what makes us great, the makers and their products, backgrounds and how they are there for you."
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("button", {
                                        className: (imageshome_module_default()).shopnowbtn,
                                        children: "SHOP NOW"
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        style: {
                            width: "80px",
                            height: "100%"
                        }
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                        className: (imageshome_module_default()).imageWidth,
                        style: {
                            display: "flex",
                            flexWrap: "wrap",
                            justifyContent: "space-between",
                            flexDirection: "column"
                        },
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                className: (imageshome_module_default()).img2Container,
                                style: {
                                    position: "relative"
                                },
                                children: [
                                    /*#__PURE__*/ jsx_runtime.jsx("img", {
                                        src: "/Images/Asset3.png"
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("label", {
                                        style: {
                                            position: "absolute",
                                            top: 0,
                                            left: 0,
                                            padding: "7px 14px",
                                            letterSpacing: 1,
                                            color: "#fff",
                                            fontSize: 12,
                                            backgroundColor: "#546122"
                                        },
                                        children: "DESIGNER"
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                        style: {
                                            display: "flex",
                                            position: "absolute",
                                            bottom: 0,
                                            left: 30,
                                            flexDirection: "column",
                                            justifyContent: "flex-end"
                                        },
                                        children: [
                                            /*#__PURE__*/ jsx_runtime.jsx("label", {
                                                style: {
                                                    bottom: 38,
                                                    position: "absolute"
                                                },
                                                className: (imageshome_module_default()).img2Title,
                                                children: "EXCLUSIVE"
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx("p", {
                                                className: (imageshome_module_default()).img2Title,
                                                children: " DESIGNER LABELS"
                                            })
                                        ]
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                className: (imageshome_module_default()).img2Container,
                                style: {
                                    backgroundColor: "#efefef"
                                },
                                children: [
                                    /*#__PURE__*/ jsx_runtime.jsx("img", {
                                        src: "https://buyamiaimages.s3.ap-south-1.amazonaws.com/2+5.png"
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                        style: {
                                            display: "flex",
                                            flexDirection: "column",
                                            padding: "30px 0px 30px 10px",
                                            overflow: "auto"
                                        },
                                        children: [
                                            /*#__PURE__*/ jsx_runtime.jsx("label", {
                                                style: {
                                                    lineHeight: 1
                                                },
                                                className: (imageshome_module_default()).img3Title,
                                                children: "New Product 2021"
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx("label", {
                                                className: (imageshome_module_default()).img2Desc,
                                                children: "Lorem Ipsum is simply dummy text of the printing and types sate industry Lorem Ipsum has been the industry."
                                            }),
                                            /*#__PURE__*/ jsx_runtime.jsx("button", {
                                                className: (imageshome_module_default()).shopnowbtn2,
                                                children: "Buy Now"
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        ]
    }));
};
//2800
/* harmony default export */ const imageshome = (Imageshome);

// EXTERNAL MODULE: ./src/Components/Header/Header.js + 1 modules
var Header = __webpack_require__(886248);
// EXTERNAL MODULE: ./src/Components/Index/Footer.js
var Footer = __webpack_require__(744332);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/FormatListBulleted.js
var FormatListBulleted = __webpack_require__(885938);
// EXTERNAL MODULE: ./src/Components/Index/MainContent.module.css
var MainContent_module = __webpack_require__(326123);
var MainContent_module_default = /*#__PURE__*/__webpack_require__.n(MainContent_module);
// EXTERNAL MODULE: ./node_modules/react-dom/index.js
var react_dom = __webpack_require__(973935);
// EXTERNAL MODULE: ./node_modules/react-responsive-carousel/lib/js/index.js
var js = __webpack_require__(710615);
// EXTERNAL MODULE: ./src/Components/Index/Category/CategoryComponent.js
var CategoryComponent = __webpack_require__(662863);
// EXTERNAL MODULE: ./src/Components/Index/MainSlider.module.css
var MainSlider_module = __webpack_require__(579810);
var MainSlider_module_default = /*#__PURE__*/__webpack_require__.n(MainSlider_module);
;// CONCATENATED MODULE: ./src/Components/Index/MainSlider.js



const MainSlider = ()=>{
    const images = [
        "/Images/MainContent/homepagemain.png",
        "/Images/MainContent/homepagemain.png",
        "/Images/MainContent/homepagemain.png",
        "/Images/MainContent/homepagemain.png", 
    ];
    const { 0: currentImageIndex , 1: setCurrentImage  } = (0,react.useState)(0);
    function plusSlides(incrIndex) {
        let nextIndex = currentImageIndex + incrIndex;
        // console.log(nextIndex);
        if (nextIndex < 0) {
            setCurrentImage(images.length - 1);
        } else if (nextIndex > images.length - 1) {
            setCurrentImage(0);
        } else {
            setCurrentImage(nextIndex);
        }
    }
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)(jsx_runtime.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                className: (MainSlider_module_default()).slideshow_container,
                children: [
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                        className: ((MainSlider_module_default()).mySlides, (MainSlider_module_default()).fade),
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                                className: (MainSlider_module_default()).InfoContainer,
                                children: [
                                    /*#__PURE__*/ jsx_runtime.jsx("span", {
                                        className: (MainSlider_module_default()).InfoTitle,
                                        children: "MEET THE MAKERS"
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("br", {
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("span", {
                                        className: (MainSlider_module_default()).InfoHeading,
                                        children: [
                                            "FROM RURAL",
                                            /*#__PURE__*/ jsx_runtime.jsx("br", {
                                            }),
                                            " TO MAIN STREET"
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("br", {
                                    }),
                                    /*#__PURE__*/ jsx_runtime.jsx("span", {
                                        className: (MainSlider_module_default()).InfoDescription,
                                        children: "A success story about what makes us great, the makers and their products, backgrounds and how they are there for you"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: images[currentImageIndex],
                                style: {
                                    objectFit: "scale-down",
                                    height: "80%",
                                    width: "auto",
                                    margin: "0 30px 0px 0px"
                                }
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("span", {
                        className: (MainSlider_module_default()).prev,
                        onClick: (e)=>plusSlides(-1)
                        ,
                        children: "❮"
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("span", {
                        className: (MainSlider_module_default()).next,
                        onClick: (e)=>plusSlides(1)
                        ,
                        children: "❯"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                style: {
                    textAlign: "center",
                    position: "absolute",
                    zIndex: "100",
                    bottom: "20px",
                    right: "46%"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("img", {
                        src: "/Images/Youtube.svg",
                        style: {
                            cursor: "pointer"
                        }
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("br", {
                    }),
                    images && images.map((item, index)=>/*#__PURE__*/ jsx_runtime.jsx("span", {
                            className: (MainSlider_module_default()).dot,
                            style: {
                                opacity: index === currentImageIndex ? "0.5" : "1"
                            },
                            onClick: (e)=>setCurrentImage(index)
                        }, index)
                    )
                ]
            })
        ]
    }));
};
/* harmony default export */ const Index_MainSlider = (MainSlider);

;// CONCATENATED MODULE: ./src/Components/Index/MainContent.js





 // requires a loader




function MainContent() {
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        className: (MainContent_module_default()).containter,
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (MainContent_module_default()).child1 + " d-none d-xl-block",
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "CATEGORIES",
                            color: "#062A30",
                            icon: /*#__PURE__*/ jsx_runtime.jsx(FormatListBulleted/* default */.Z, {
                                style: {
                                    margin: "0px 15px 0px 7px",
                                    height: 30,
                                    color: "#062A30"
                                }
                            }),
                            subCategory: [
                                "Bathroom",
                                "Bedroom",
                                "Home Decor",
                                "Tabletop",
                                "Kitchen & Dining",
                                "Lighting",
                                "Textiles & Rugs",
                                "Furniture",
                                "Office", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Home & Lifestyle",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/home.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "Bathroom",
                                "Bedroom",
                                "Home Decor",
                                "Tabletop",
                                "Kitchen & Dining",
                                "Lighting",
                                "Textiles & Rugs",
                                "Furniture",
                                "Office", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Beauty",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/beauty.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "Bath & Body\t",
                                "Fragrences & Oils",
                                "Hair",
                                "Maek-up",
                                "Shaving & Grooming",
                                "Skincare",
                                "Spa & Spiritual\t",
                                "Nail Care", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Fashion",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/fashion.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "ActiveWear",
                                "Swimwear",
                                "Coats & Jackets",
                                "Dresses",
                                "Knitwear & Sweaters",
                                "Pants & Shorts",
                                "Skirts",
                                "Sleepwear",
                                "Socks",
                                "Tops",
                                "T-shirts",
                                "Underwear",
                                "Footwear",
                                "Bags & Purses", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Food & Drinks",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/food.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            // icon={<BrunchDiningIcon style={{ margin: "0px 15px 0px 0px" }} />}
                            subCategory: [
                                "Drinks & Syrups",
                                "Snacks",
                                "Sweet",
                                "Cooking & Baking",
                                "Cereals & Grains",
                                "Jams",
                                "Sauces",
                                "Pure Indonesian",
                                "Spices & Seasoning", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Jewelry",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/jewelry.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "Rings",
                                "Necklaces",
                                "Earrings",
                                "Bracelets",
                                "Body Jewelry", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Kids",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/kids.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "Apparel",
                                "Footwear",
                                "Nursery",
                                "Toys & Learning"
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Stationery",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/stationery.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "Books",
                                "Cards",
                                "Office Supplies",
                                "Storage/Filing", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Art & Antique",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/art.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            subCategory: [
                                "Drawing/Painting",
                                "Sculpture",
                                "Functional Art",
                                "Indonesian",
                                "Prints",
                                "Photography", 
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Raw Materials",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/raw.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            // icon={
                            //   <FormatColorFillIcon style={{ margin: "0px 15px 0px 0px" }} />
                            // }
                            subCategory: [
                                "Sub-Category Not Available"
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                            name: "Designer Labels",
                            color: "#cfa684",
                            icon: /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/CategoryIcon/star.svg",
                                height: 30,
                                style: {
                                    margin: "0px 15px 0px 0px"
                                }
                            }),
                            // icon={
                            //   <FormatColorFillIcon style={{ margin: "0px 15px 0px 0px" }} />
                            // }
                            subCategory: [
                                "Sub-Category Not Available"
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (MainContent_module_default()).child2,
                children: /*#__PURE__*/ jsx_runtime.jsx(Index_MainSlider, {
                })
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./src/pages/index.js













function Home({ ProductData , ProductCategoryData , ProductCategoryData2 , ProductCategoryData3 , CategoryName , CategoryName2 , CategoryName3 ,  }) {
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("meta", {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            }),
            /*#__PURE__*/ jsx_runtime.jsx(MainHeader/* default */.Z, {
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Header/* default */.Z, {
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                style: {
                    // transform: "scale(0.9)",
                    maxWidth: "1520px",
                    margin: "0 auto"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx(MainContent, {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(imageshome, {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(Details, {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(DropshipHotlist, {
                        products: ProductData === null || ProductData === void 0 ? void 0 : ProductData.input[0]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(Category, {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(Index_MeetTheMakers, {
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryWiseProduct/* default */.Z, {
                        CategoryName: "Home And Living",
                        ProductCategoryData: ProductCategoryData === null || ProductCategoryData === void 0 ? void 0 : ProductCategoryData.input[0]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryWiseProduct/* default */.Z, {
                        CategoryName: "Art And Antique",
                        Size: 5,
                        ProductCategoryData: ProductCategoryData2 === null || ProductCategoryData2 === void 0 ? void 0 : ProductCategoryData2.input[0].concat(ProductCategoryData3 === null || ProductCategoryData3 === void 0 ? void 0 : ProductCategoryData3.input[0])
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(BehindTheScenes/* default */.Z, {
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime.jsx(Footer/* default */.Z, {
            })
        ]
    }));
};
async function getServerSideProps({ req , res , params  }) {
    let CategoryName = 7;
    const url = "https://hzlvonfjd2.execute-api.ap-south-1.amazonaws.com/dev/";
    const productAllData = await fetch(url + "getproduct");
    const ProductData = await productAllData.json();
    const productCategoryData = await fetch(url + "getproductbycategory", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            categoryid: CategoryName
        })
    });
    const ProductCategoryData = await productCategoryData.json();
    let CategoryName2 = 1;
    const productCategoryData2 = await fetch(url + "getproductbycategory", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            categoryid: CategoryName2
        })
    });
    const ProductCategoryData2 = await productCategoryData2.json();
    let CategoryName3 = 3;
    const productCategoryData3 = await fetch(url + "getproductbycategory", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            categoryid: CategoryName3
        })
    });
    const ProductCategoryData3 = await productCategoryData3.json();
    // console.log(ProductCategoryData3);
    return {
        props: {
            ProductData,
            ProductCategoryData,
            ProductCategoryData2,
            ProductCategoryData3,
            CategoryName: "Home And Living",
            CategoryName2: "Art And Antique",
            CategoryName3: "Beauty"
        }
    };
}


/***/ }),

/***/ 501014:
/***/ ((module) => {

"use strict";
module.exports = require("critters");

/***/ }),

/***/ 702186:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/@ampproject/toolbox-optimizer");

/***/ }),

/***/ 714300:
/***/ ((module) => {

"use strict";
module.exports = require("buffer");

/***/ }),

/***/ 706113:
/***/ ((module) => {

"use strict";
module.exports = require("crypto");

/***/ }),

/***/ 582361:
/***/ ((module) => {

"use strict";
module.exports = require("events");

/***/ }),

/***/ 657147:
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),

/***/ 113685:
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),

/***/ 795687:
/***/ ((module) => {

"use strict";
module.exports = require("https");

/***/ }),

/***/ 822037:
/***/ ((module) => {

"use strict";
module.exports = require("os");

/***/ }),

/***/ 371017:
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ }),

/***/ 863477:
/***/ ((module) => {

"use strict";
module.exports = require("querystring");

/***/ }),

/***/ 12781:
/***/ ((module) => {

"use strict";
module.exports = require("stream");

/***/ }),

/***/ 371576:
/***/ ((module) => {

"use strict";
module.exports = require("string_decoder");

/***/ }),

/***/ 257310:
/***/ ((module) => {

"use strict";
module.exports = require("url");

/***/ }),

/***/ 473837:
/***/ ((module) => {

"use strict";
module.exports = require("util");

/***/ }),

/***/ 959796:
/***/ ((module) => {

"use strict";
module.exports = require("zlib");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [244,765,189,615,447,638,870], () => (__webpack_exec__(227970)));
module.exports = __webpack_exports__;

})();