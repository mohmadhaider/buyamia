exports.id = 638;
exports.ids = [638];
exports.modules = {

/***/ 637742:
/***/ ((module) => {

// Exports
module.exports = {
	"mobileicon": "Header_mobileicon__QH5cn"
};


/***/ }),

/***/ 84789:
/***/ ((module) => {

// Exports
module.exports = {
	"main": "SearchBar_main___UNH_",
	"container": "SearchBar_container__70I_R",
	"right": "SearchBar_right__G1D_l",
	"logoimage": "SearchBar_logoimage__DUY0T",
	"center": "SearchBar_center__Xf_8Y",
	"centerInner": "SearchBar_centerInner__2ZOzu",
	"centerInnerselect": "SearchBar_centerInnerselect__B7kq_",
	"containerInnerPrepend": "SearchBar_containerInnerPrepend__bXv2e",
	"selectSearchProduct": "SearchBar_selectSearchProduct__UreoQ",
	"containerInnerPostpend": "SearchBar_containerInnerPostpend__9KJnW",
	"left": "SearchBar_left__KE_Rz",
	"containerInnerLeft": "SearchBar_containerInnerLeft__kTa9_",
	"innerLeftIcon": "SearchBar_innerLeftIcon__APj1y",
	"userDropdown": "SearchBar_userDropdown__aerxu",
	"searchText": "SearchBar_searchText__u4jf8"
};


/***/ }),

/***/ 165194:
/***/ ((module) => {

// Exports
module.exports = {
	"box": "CategoryWiseProduct_box__fRfmZ",
	"carouselitem": "CategoryWiseProduct_carouselitem__Y5JBV",
	"title": "CategoryWiseProduct_title__igmkt",
	"iconWrap": "CategoryWiseProduct_iconWrap__MQb_q",
	"productContainer": "CategoryWiseProduct_productContainer__1e8PA"
};


/***/ }),

/***/ 679168:
/***/ ((module) => {

// Exports
module.exports = {
	"footer": "Footer_footer__UVidG",
	"linkContainer": "Footer_linkContainer__td8BZ",
	"linkHeader": "Footer_linkHeader___h8Xm",
	"linkItem": "Footer_linkItem__rb69a",
	"infoNewsLetter": "Footer_infoNewsLetter__7PzkS",
	"newsLetterInfoHeader": "Footer_newsLetterInfoHeader__Qzw7A",
	"newsLetterInfoItem": "Footer_newsLetterInfoItem__nlIHM",
	"inputNewsLetter": "Footer_inputNewsLetter__yU3Lt",
	"inputNewsLetterInput": "Footer_inputNewsLetterInput__PUT9g",
	"newsletterFlag": "Footer_newsletterFlag__JgWPd",
	"divIconContainer": "Footer_divIconContainer__l0BlQ",
	"divIconCircle": "Footer_divIconCircle__UH_5S"
};


/***/ }),

/***/ 326123:
/***/ ((module) => {

// Exports
module.exports = {
	"containter": "MainContent_containter__b1K_5",
	"child1": "MainContent_child1__S06xd",
	"mainCategory": "MainContent_mainCategory__Pk8Xw",
	"category": "MainContent_category__fV2uk",
	"normalCategory": "MainContent_normalCategory__gAhsv",
	"child2": "MainContent_child2__hjGP3"
};


/***/ }),

/***/ 561892:
/***/ ((module) => {

// Exports
module.exports = {
	"container": "MainHeader_container__TvHD5",
	"wrapper": "MainHeader_wrapper__74Xzf",
	"wrapperlinks": "MainHeader_wrapperlinks__tCWph"
};


/***/ }),

/***/ 656963:
/***/ ((module) => {

// Exports
module.exports = {
	"imageContainer": "ProductContainer_imageContainer__AIm_A",
	"title": "ProductContainer_title__Fnitw",
	"price": "ProductContainer_price__Lbwi2",
	"summery": "ProductContainer_summery__89f_A",
	"vendor": "ProductContainer_vendor__OQat1",
	"cardContainer": "ProductContainer_cardContainer__2bAjR"
};


/***/ }),

/***/ 886248:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Header_Header)
});

// EXTERNAL MODULE: ./node_modules/react/jsx-runtime.js
var jsx_runtime = __webpack_require__(785893);
// EXTERNAL MODULE: ./node_modules/@material-ui/icons/index.js
var icons = __webpack_require__(617066);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/FormatListBulleted.js
var FormatListBulleted = __webpack_require__(885938);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Home.js
var Home = __webpack_require__(976638);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/AutoFixHigh.js
var AutoFixHigh = __webpack_require__(440135);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Toys.js
var Toys = __webpack_require__(815604);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/MenuBook.js
var MenuBook = __webpack_require__(367600);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Article.js
var Article = __webpack_require__(131523);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/BrunchDining.js
var BrunchDining = __webpack_require__(385875);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/FormatColorFill.js
var FormatColorFill = __webpack_require__(439513);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Flare.js
var Flare = __webpack_require__(291458);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Woman.js
var Woman = __webpack_require__(916665);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/Typography/index.js
var Typography = __webpack_require__(34904);
var Typography_default = /*#__PURE__*/__webpack_require__.n(Typography);
// EXTERNAL MODULE: ./src/Components/Index/MainContent.module.css
var MainContent_module = __webpack_require__(326123);
var MainContent_module_default = /*#__PURE__*/__webpack_require__.n(MainContent_module);
// EXTERNAL MODULE: ./src/Components/Header/Header.module.css
var Header_module = __webpack_require__(637742);
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(667294);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/InputBase/index.js
var InputBase = __webpack_require__(673176);
var InputBase_default = /*#__PURE__*/__webpack_require__.n(InputBase);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/IconButton/index.js
var IconButton = __webpack_require__(86024);
var IconButton_default = /*#__PURE__*/__webpack_require__.n(IconButton);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Search.js
var Search = __webpack_require__(942761);
// EXTERNAL MODULE: ./src/Components/Header/SearchBar.module.css
var SearchBar_module = __webpack_require__(84789);
var SearchBar_module_default = /*#__PURE__*/__webpack_require__.n(SearchBar_module);
// EXTERNAL MODULE: ./node_modules/@material-ui/core/esm/styles/makeStyles.js
var makeStyles = __webpack_require__(241120);
;// CONCATENATED MODULE: ./src/Components/Header/SearchBar.js











const useStyles = (0,makeStyles/* default */.Z)({
    root: {
        padding: "2px 4px",
        backgroundColor: "#c8a27b",
        // borderRadius: "50px",
        borderTopLeftRadius: "50px",
        borderBottomLeftRadius: "50px",
        "& .MuiSelect-select.MuiSelect-select": {
            padding: 0,
            color: "#fff"
        }
    }
});
function SearchBar(props) {
    const [age, setAge] = react.useState("Product");
    const classes = useStyles();
    const handleChange = (event)=>{
        setAge(event.target.value);
    };
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        component: "form",
        // elevation={0}
        style: {
            // p: "2px 4px",
            display: "flex",
            borderRadius: "40px",
            alignItems: "center",
            justifySelf: "center",
            width: props.highwidth ? "600px" : "auto",
            maxWidth: 600,
            margin: "auto"
        },
        className: (SearchBar_module_default()).centerInner,
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                style: {
                    padding: "0px 7px",
                    fontWeight: 100
                },
                className: (SearchBar_module_default()).centerInnerselect,
                children: /*#__PURE__*/ (0,jsx_runtime.jsxs)("select", {
                    name: "search",
                    id: "search",
                    className: (SearchBar_module_default()).centerInnerselect,
                    children: [
                        /*#__PURE__*/ jsx_runtime.jsx("option", {
                            value: "product",
                            style: {
                                border: 0
                            },
                            children: "\xa0Products"
                        }),
                        /*#__PURE__*/ jsx_runtime.jsx("option", {
                            value: "seller",
                            style: {
                                border: 0
                            },
                            children: "\xa0Sellers"
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime.jsx((InputBase_default()), {
                className: (SearchBar_module_default()).searchText + " text-truncate",
                sx: {
                    mx: 2,
                    flex: 1
                },
                placeholder: "Search for products & find verified sellers near you",
                inputProps: {
                    "aria-label": "Search for products & find verified sellers near you"
                }
            }),
            /*#__PURE__*/ jsx_runtime.jsx((IconButton_default()), {
                type: "submit",
                sx: {
                    p: "10px 10px 10px 0px"
                },
                "aria-label": "search",
                children: /*#__PURE__*/ jsx_runtime.jsx(Search/* default */.Z, {
                })
            })
        ]
    }));
};

// EXTERNAL MODULE: ./node_modules/@mui/material/node/Box/index.js
var Box = __webpack_require__(609285);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/Drawer/index.js
var Drawer = __webpack_require__(698369);
var Drawer_default = /*#__PURE__*/__webpack_require__.n(Drawer);
// EXTERNAL MODULE: ./node_modules/@mui/material/node/List/index.js
var List = __webpack_require__(915419);
var List_default = /*#__PURE__*/__webpack_require__.n(List);
// EXTERNAL MODULE: ./src/Components/Index/Category/CategoryComponent.js
var CategoryComponent = __webpack_require__(662863);
// EXTERNAL MODULE: ./node_modules/@mui/icons-material/Cancel.js
var Cancel = __webpack_require__(44510);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(741664);
;// CONCATENATED MODULE: ./src/Components/Header/Header.js























const Header = ()=>{
    const [state, setState] = react.useState({
        top: false,
        left: false,
        bottom: false,
        right: false
    });
    const toggleDrawer = (anchor, open)=>(event)=>{
            if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
                return;
            }
            setState({
                ...state,
                [anchor]: open
            });
        }
    ;
    const list = (anchor)=>/*#__PURE__*/ jsx_runtime.jsx(Box["default"], {
            sx: {
                width: anchor === "top" || anchor === "bottom" ? "auto" : 250
            },
            role: "presentation",
            //   onClick={toggleDrawer(anchor, false)}
            onKeyDown: toggleDrawer(anchor, false),
            children: /*#__PURE__*/ (0,jsx_runtime.jsxs)((List_default()), {
                children: [
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                        className: (MainContent_module_default()).category,
                        //   expandIcon={<ExpandMoreIcon />}
                        "aria-controls": "panel1a-content",
                        id: "panel1a-header",
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(FormatListBulleted/* default */.Z, {
                                style: {
                                    margin: "0px 20px 0px 0px"
                                }
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx((Typography_default()), {
                                style: {
                                    fontWeight: 750,
                                    fontFamily: "Roboto"
                                },
                                children: "CATEGORIES"
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(Cancel/* default */.Z, {
                                style: {
                                    fontSize: 30,
                                    cursor: "pointer"
                                },
                                onClick: toggleDrawer(anchor, false)
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "Home & Living",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(Home/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Bathroom",
                            "Bedroom",
                            "Home Decor",
                            "Tabletop",
                            "Kitchen & Dining",
                            "Lighting",
                            "Textiles & Rugs",
                            "Furniture",
                            "Office", 
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "BEAUTY",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(AutoFixHigh/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Bath & Body\t",
                            "Fragrences & Oils",
                            "Hair",
                            "Maek-up",
                            "Shaving & Grooming",
                            "Skincare",
                            "Spa & Spiritual\t",
                            "Nail Care", 
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "FASHION",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(Woman/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "ActiveWear",
                            "Swimwear",
                            "Coats & Jackets",
                            "Dresses",
                            "Knitwear & Sweaters",
                            "Pants & Shorts",
                            "Skirts",
                            "Sleepwear",
                            "Socks",
                            "Tops",
                            "T-shirts",
                            "Underwear",
                            "Footwear",
                            "Bags & Purses", 
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "FOOD & DRINKS",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(BrunchDining/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Drinks & Syrups",
                            "Snacks",
                            "Sweet",
                            "Cooking & Baking",
                            "Cereals & Grains",
                            "Jams",
                            "Sauces",
                            "Pure Indonesian",
                            "Spices & Seasoning", 
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "JEWELRY",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(Flare/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Rings",
                            "Necklaces",
                            "Earrings",
                            "Bracelets",
                            "Body Jewelry", 
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "KIDS",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(Toys/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Apparel",
                            "Footwear",
                            "Nursery",
                            "Toys & Learning"
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "STATIONERY",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(MenuBook/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Books",
                            "Cards",
                            "Office Supplies",
                            "Storage/Filing"
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "ART & ANTIQUE",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(Article/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Drawing/Painting",
                            "Sculpture",
                            "Functional Art",
                            "Indonesian",
                            "Prints",
                            "Photography", 
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx(CategoryComponent/* default */.Z, {
                        name: "RAW MATERIALS",
                        icon: /*#__PURE__*/ jsx_runtime.jsx(FormatColorFill/* default */.Z, {
                            style: {
                                margin: "0px 20px 0px 0px"
                            }
                        }),
                        subCategory: [
                            "Sub-Category Not Available"
                        ]
                    })
                ]
            })
        })
    ;
    return(/*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        children: [
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                style: {
                    display: "flex",
                    justifyContent: "space-around",
                    alignItems: "center",
                    padding: "10px 10px"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx(next_link["default"], {
                        href: "/",
                        children: /*#__PURE__*/ jsx_runtime.jsx("img", {
                            style: {
                                maxWidth: 170,
                                cursor: " pointer"
                            },
                            src: "/Images/logocropped.svg"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        style: {
                            margin: "20px 10px"
                        },
                        className: "d-none d-lg-block d-xl-block",
                        children: /*#__PURE__*/ jsx_runtime.jsx(SearchBar, {
                            highwidth: true
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                        style: {
                            display: "flex"
                        },
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/HeaderIcon/bucket.svg",
                                height: 35,
                                className: (Header_module_default()).mobileicon
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/HeaderIcon/notification.svg",
                                height: 35,
                                className: (Header_module_default()).mobileicon
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx("img", {
                                src: "/Images/HeaderIcon/user.svg",
                                height: 35,
                                className: (Header_module_default()).mobileicon
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx(icons/* AppsRounded */.caC, {
                                className: "d-lg-none d-xl-none",
                                style: {
                                    fontSize: "35px",
                                    color: "#C8A27B",
                                    cursor: "pointer"
                                },
                                onClick: toggleDrawer("bottom", true)
                            }),
                            /*#__PURE__*/ jsx_runtime.jsx("div", {
                                children: /*#__PURE__*/ jsx_runtime.jsx((Drawer_default()), {
                                    anchor: "bottom",
                                    open: state["bottom"],
                                    children: list("bottom")
                                })
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                style: {
                    margin: "20px 20px"
                },
                className: "d-lg-none d-xl-none",
                children: /*#__PURE__*/ jsx_runtime.jsx(SearchBar, {
                })
            })
        ]
    }));
};
/* harmony default export */ const Header_Header = (Header);


/***/ }),

/***/ 617824:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ CategoryWiseProduct)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(785893);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(667294);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(45697);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(609285);
/* harmony import */ var _ProductContainer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(242494);
/* harmony import */ var _CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(165194);
/* harmony import */ var _CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(425675);
/* harmony import */ var react_multi_carousel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(386529);










const responsive = {
    desktop: {
        breakpoint: {
            max: 3000,
            min: 1024
        },
        items: 3,
        slidesToSlide: 3
    },
    tablet: {
        breakpoint: {
            max: 1024,
            min: 464
        },
        items: 2,
        slidesToSlide: 2
    },
    mobile: {
        breakpoint: {
            max: 464,
            min: 0
        },
        items: 1,
        slidesToSlide: 1
    }
};
function Item(props) {
    const { sx , ...other } = props;
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_mui_material_Box__WEBPACK_IMPORTED_MODULE_6__["default"], {
        sx: {
            // bgcolor: "primary.main",
            color: "white",
            p: 1,
            m: 1,
            margin: "0px 10px",
            borderRadius: 1,
            textAlign: "center",
            fontSize: "1rem",
            // height: 416,
            width: 254,
            // margin: "0px 20px",
            fontWeight: "700",
            ...sx
        },
        ...other
    }));
}
Item.propTypes = {
    sx: prop_types__WEBPACK_IMPORTED_MODULE_2___default().oneOfType([
        prop_types__WEBPACK_IMPORTED_MODULE_2___default().arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_2___default().oneOfType([
            (prop_types__WEBPACK_IMPORTED_MODULE_2___default().func),
            (prop_types__WEBPACK_IMPORTED_MODULE_2___default().object),
            (prop_types__WEBPACK_IMPORTED_MODULE_2___default().bool)
        ])),
        (prop_types__WEBPACK_IMPORTED_MODULE_2___default().func),
        (prop_types__WEBPACK_IMPORTED_MODULE_2___default().object), 
    ])
};
function CategoryWiseProduct({ ProductCategoryData =[] , CategoryName ="Home & Living" , Size =8 ,  }) {
    var ref;
    const [width, setWidth] = react__WEBPACK_IMPORTED_MODULE_1__.useState();
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setWidth(window.innerWidth);
        console.log(window.innerWidth);
        window.addEventListener("resize", ()=>{
            setWidth(window.innerWidth);
            console.log(window.innerWidth);
        });
    }, []);
    let DivRef = react__WEBPACK_IMPORTED_MODULE_1__.useRef(null);
    const [currentScrollPosition, setCurrentScrollPosition] = react__WEBPACK_IMPORTED_MODULE_1__.useState(0);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        style: {
            width: "100%"
        },
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: (_CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7___default().box),
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7___default().title),
                    children: CategoryName.replace("_", " & ")
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7___default().iconWrap),
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        // src={"/Images/DropshipHotlist/Rectangle 115.png"}
                        style: {
                            width: 110,
                            height: 6,
                            backgroundColor: "#C8A27B"
                        }
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    style: {
                        padding: "20px 20px 0px"
                    },
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_multi_carousel__WEBPACK_IMPORTED_MODULE_5__["default"], {
                        responsive: responsive,
                        containerClass: `${width >= '1420' ? " justify-content-center" : ""}`,
                        removeArrowOnDeviceType: [
                            "desktop",
                            "tablet",
                            "mobile"
                        ],
                        itemClass: (_CategoryWiseProduct_module_css__WEBPACK_IMPORTED_MODULE_7___default().carouselitem),
                        children: (ref = ProductCategoryData.slice(0, 5)) === null || ref === void 0 ? void 0 : ref.map((product, index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Item, {
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_ProductContainer__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                                    productImage: product.mainimagepath,
                                    title: product.productname,
                                    price: product.rate,
                                    productid: product.productid,
                                    summery: product.productdesc,
                                    rating: 4,
                                    minWidth: 250,
                                    minHeight: 420
                                })
                            }, index)
                        )
                    })
                })
            ]
        })
    }));
};


/***/ }),

/***/ 662863:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(785893);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(667294);
/* harmony import */ var _MainContent_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(326123);
/* harmony import */ var _MainContent_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_MainContent_module_css__WEBPACK_IMPORTED_MODULE_2__);



function CategoryComponent({ name , icon , subCategory , color =""  }) {
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: !color ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: (_MainContent_module_css__WEBPACK_IMPORTED_MODULE_2___default().category),
            onClick: ()=>{
            },
            style: {
                cursor: "pointer"
            },
            color: "primary",
            id: "panel2a-header",
            children: [
                " ",
                icon,
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    style: {
                        textAlign: "center"
                    },
                    children: name
                })
            ]
        }) : /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: (_MainContent_module_css__WEBPACK_IMPORTED_MODULE_2___default().category),
            onClick: ()=>{
            },
            style: {
                cursor: "pointer",
                color: color
            },
            color: "primary",
            id: "panel2a-header",
            children: [
                " ",
                icon,
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    style: {
                        textAlign: "center",
                        color: color
                    },
                    children: name
                })
            ]
        })
    }));
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CategoryComponent);


/***/ }),

/***/ 744332:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(785893);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(667294);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(250450);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(425675);
/* harmony import */ var _Footer_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(679168);
/* harmony import */ var _Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Footer_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_icons_bs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(463750);
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(889583);







const Footer = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().footer),
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Container */ .W2, {
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Row */ .X2, {
                className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkContainer),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Col */ .JX, {
                        lg: "7",
                        md: "7",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Row */ .X2, {
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Col */ .JX, {
                                lg: {
                                    size: "8",
                                    offset: "2"
                                },
                                md: {
                                    size: "8",
                                    offset: "2"
                                },
                                sm: "12",
                                xs: "12",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Row */ .X2, {
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Col */ .JX, {
                                            lg: "4",
                                            md: "4",
                                            sm: "4",
                                            xs: "4",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkHeader),
                                                    children: "About"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "About Us"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Timeline"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Careers"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Become a Seller"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Buying from us"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Col */ .JX, {
                                            lg: "4",
                                            md: "4",
                                            sm: "4",
                                            xs: "4",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkHeader),
                                                    children: "Support"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Contact US"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Shipping"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Return/Warranty"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Support"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Col */ .JX, {
                                            lg: "4",
                                            md: "4",
                                            sm: "4",
                                            xs: "4",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkHeader),
                                                    children: "Legal"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Privacy"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().linkItem),
                                                    children: "Terms"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Col */ .JX, {
                        lg: "4",
                        md: "4",
                        sm: "12",
                        xs: "12",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Container */ .W2, {
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().infoNewsLetter),
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            src: "https://img.icons8.com/color/48/000000/indonesia-circular.png",
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().newsletterFlag)
                                        }),
                                        "\xa0\xa0\xa0",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().newsLetterInfoHeader),
                                            children: "Region"
                                        }),
                                        "\xa0\xa0",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().newsLetterInfoItem),
                                            children: "Indonesia"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().infoNewsLetter),
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().newsLetterInfoHeader),
                                        children: "Wan’t be receive discounts, latest and greatest, sign up for our newsletter:"
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .FormGroup */ .cw, {
                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().inputNewsLetter),
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(reactstrap__WEBPACK_IMPORTED_MODULE_4__/* .Input */ .II, {
                                        placeholder: "Sign Up",
                                        className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().inputNewsLetterInput)
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconContainer),
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsInstagram */ .Vs6, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsLinkedin */ .NQh, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsPinterest */ .I30, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsFacebook */ .k1O, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_fa__WEBPACK_IMPORTED_MODULE_6__/* .FaTiktok */ .nTm, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsWhatsapp */ .RGt, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsTwitter */ .meP, {
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_Footer_module_css__WEBPACK_IMPORTED_MODULE_3___default().divIconCircle),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_bs__WEBPACK_IMPORTED_MODULE_5__/* .BsYoutube */ .bUO, {
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Footer);


/***/ }),

/***/ 403279:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(785893);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(667294);
/* harmony import */ var _MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(561892);
/* harmony import */ var _MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2__);



const MainHeader = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: (_MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2___default().container),
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2___default().wrapper),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2___default().wrapperlinks),
                        children: "How We Work"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2___default().wrapperlinks),
                        children: "Meet Our Makers"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_MainHeader_module_css__WEBPACK_IMPORTED_MODULE_2___default().wrapperlinks),
                        children: "About Us"
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MainHeader);


/***/ }),

/***/ 242494:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ ProductContainer)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(785893);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(667294);
/* harmony import */ var _mui_material_CardMedia__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(205991);
/* harmony import */ var _mui_material_CardMedia__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material_CardMedia__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material_Rating__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(724326);
/* harmony import */ var _mui_material_Rating__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Rating__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(656963);
/* harmony import */ var _ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3__);









const imagesPath = (/* unused pure expression or super */ null && ([
    "https://buyamiaimages.s3.ap-south-1.amazonaws.com/p1.png",
    "https://buyamiaimages.s3.ap-south-1.amazonaws.com/p2.png",
    "https://buyamiaimages.s3.ap-south-1.amazonaws.com/p3.png",
    "https://buyamiaimages.s3.ap-south-1.amazonaws.com/p4.png", 
]));
function ProductContainer({ productid , productImage ="" , title ="" , imageHeight ="250px" , price ="" , summery ="" , rating ="" , minWidth ="" , minHeight ="" , hotseller , ratingColor ="" ,  }) {
    //   console.log(JSON.stringify(productImage) + "PRODUCT IAMEF");
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        onClick: ()=>{
            window.location.href = "/ProductDetail/" + productid;
        },
        style: {
            minWidth: minWidth,
            minHeight: minHeight,
            backgroundColor: "#fff",
            maxWidth: 315,
            maxHeight: 440,
            cursor: "pointer",
            position: "relative",
            boxShadow: "5px 0px 10px  rgba(32, 33, 36, 0.28)",
            borderRadius: "0px"
        },
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_CardMedia__WEBPACK_IMPORTED_MODULE_2___default()), {
                component: "img",
                // width="254px"
                sx: {
                    minHeight: imageHeight
                },
                src: productImage === null || productImage === void 0 ? void 0 : productImage.split(",")[0],
                alt: "Product Image Not Found"
            }),
            hotseller && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                style: {
                    position: "absolute",
                    top: "0",
                    left: "0",
                    height: "42px",
                    width: "55px",
                    backgroundColor: "#e88f48",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "10px",
                    fontSize: "12px",
                    color: "#ffffff",
                    lineHeight: 1,
                    fontWeight: "400"
                },
                children: "HOT SELLER"
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default().cardContainer),
                children: [
                    (title === null || title === void 0 ? void 0 : title.length) > 21 ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                        className: (_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default().title),
                        children: [
                            " ",
                            title
                        ]
                    }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("lable", {
                        className: (_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default().title),
                        children: title
                    }),
                    " ",
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("b", {
                        className: (_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default().price),
                        children: [
                            "From IDR ",
                            price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                        color: "text.secondary",
                        className: (_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default().summery),
                        children: "50 Pieces (Min. Order)"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                        color: "text.secondary",
                        className: (_ProductContainer_module_css__WEBPACK_IMPORTED_MODULE_3___default().vendor),
                        children: "The Bali Curator"
                    }),
                    ratingColor === "" ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Rating__WEBPACK_IMPORTED_MODULE_4___default()), {
                        name: "half-rating",
                        contentEditable: false,
                        defaultValue: 2.5,
                        precision: rating,
                        style: {
                            color: "#c8a27b",
                            bottom: 10,
                            backgroundColor: "white",
                            position: "absolute"
                        }
                    }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Rating__WEBPACK_IMPORTED_MODULE_4___default()), {
                        name: "half-rating",
                        contentEditable: false,
                        defaultValue: 2.5,
                        precision: rating,
                        style: {
                            color: "#e88f48",
                            bottom: 10,
                            backgroundColor: "white",
                            position: "absolute"
                        }
                    })
                ]
            })
        ]
    }));
};


/***/ })

};
;